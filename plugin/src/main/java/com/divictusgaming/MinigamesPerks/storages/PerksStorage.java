package com.divictusgaming.MinigamesPerks.storages;

import com.divictusgaming.MinigamesPerks.dto.Perk;

import java.util.Map;

public class PerksStorage {
    private Map<String, Perk> perksObject;
    private Map<String, Object> perksSettings;

    public PerksStorage( Map<String, Perk> perksObject, Map<String, Object> perksSettings) {
        this.perksObject = perksObject;
        this.perksSettings = perksSettings;
    }

    public Map<String, Object> getPerksSettings() {
        return perksSettings;
    }

    public Map<String, Perk> getPerksObject() {
        return perksObject;
    }
}
