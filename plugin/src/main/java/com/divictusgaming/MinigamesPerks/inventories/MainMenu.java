package com.divictusgaming.MinigamesPerks.inventories;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.Perk;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;


public class MainMenu extends Menu {
    private static ItemStack fillItem = null;

    public static void openMainMenu(Player player) {
        Main plugin = Main.getPlugin(Main.class);

        Inventory inventory = Bukkit.getServer().createInventory(player, 9 * plugin.getConfigObject().getInvSize(),
                plugin.getConfigObject().getInvTitle());
        HashMap<String, Integer> levels = plugin.getDb().getLevels(player.getUniqueId());
        for (Map.Entry<String, Perk> perk : plugin.getPerks().getPerksObject().entrySet()) {
            ItemStack item = createItem(perk.getKey(), levels, player);
            if (item == null) continue;

            inventory.setItem(perk.getValue().getPosition(), item);
            ItemStack i = inventory.getItem(perk.getValue().getPosition());
            if (i == null || i.getType() == Material.AIR) {
                item.setAmount(1);
                inventory.setItem(perk.getValue().getPosition(), item);
            }
        }

        formatCustomItems(player, inventory, plugin.getConfigObject().getCustomItems(), null);

        ItemStack[] content = inventory.getContents();

        if (fillItem == null) fillItem = formatFillingItem(plugin.getConfigObject().getInvFill());
        fillContent(content, fillItem);

        inventory.setContents(content);
        player.openInventory(inventory);
    }

}
