package com.divictusgaming.MinigamesPerks.inventories;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.Config;
import com.divictusgaming.MinigamesPerks.dto.CustomItem;
import com.divictusgaming.MinigamesPerks.dto.Perk;
import com.divictusgaming.MinigamesPerks.utils.StringFormatter;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

abstract class Menu {
    static ItemFlag[] itemFlags = ItemFlag.values();


    static void formatCustomItems(Player player, Inventory inventory, Map<String, CustomItem> items, Perk perk) {
        for (Map.Entry<String, CustomItem> entry : items.entrySet()) {
            ItemStack showItem = perk == null ?
                    new ItemStack(entry.getValue().getItem()) : perk.getDisplayItem();
            if (!entry.getValue().isPerkItem())
                showItem = new ItemStack(entry.getValue().getItem());

            ItemMeta showItemMeta = showItem.getItemMeta();
            if (showItemMeta == null)
                showItem.setItemMeta(entry.getValue().getItem().getItemMeta());

            if (showItemMeta == null || !showItemMeta.hasDisplayName()) {
                if (perk == null)
                    showItemMeta.setDisplayName(perk.getDisplayItem().getItemMeta().getDisplayName());
                else
                    showItemMeta.setDisplayName(entry.getValue().getItem().getItemMeta().getDisplayName());
            }

            Main plugin = Main.getPlugin(Main.class);

            if (perk != null) {
                showItem.setItemMeta(showItemMeta);
                showItem = plugin.getINMSMain().createPerkItem(":" + perk.getId() + ":" + entry.getKey(), showItem);
                showItemMeta = showItem.getItemMeta();
            }
            int level = perk == null ?
                    0 : plugin.getDb().getLevels(player.getUniqueId()).get(perk.getId());
            if (entry.getValue().getItem().getItemMeta().getLore() != null) {
                String lore = String.join("", entry.getValue().getItem().getItemMeta().getLore());
                lore = StringFormatter.replaceVars(lore, level, perk, player);
                ArrayList<String> l = new ArrayList<>(Arrays.asList(lore.split("\\|\\|")));
                showItemMeta.setLore(l);
            }
            showItemMeta.setDisplayName(StringFormatter.replaceVars(showItemMeta.getDisplayName(), level, perk, player));

            if (showItemMeta.getDisplayName().contains("{perkName}"))
                showItemMeta.setDisplayName(showItemMeta.getDisplayName().replace("{perkName}", perk.getName()));


            showItem.setItemMeta(showItemMeta);
            for (Integer position : entry.getValue().getPositions())
                inventory.setItem(position, showItem);


        }
    }

    static ItemStack createItem(String perk, HashMap<String, Integer> levels, Player player) {
        Main plugin = Main.getPlugin(Main.class);
        Perk perkObj = plugin.getPerks().getPerksObject().get(perk);
        Config config = plugin.getConfigObject();
        ItemStack item = perkObj.getDisplayItem();
        item = plugin.getINMSMain().createPerkItem(perk, item);
        try {
            Integer currentLevel = levels.get(perk);
            if (currentLevel == null) currentLevel = 0;

            if (config.getShowLevelNumber() &&
                    (currentLevel > 0 || config.getZeroWhenNotUpgraded()))
                item.setAmount(currentLevel);

            ItemMeta meta = item.getItemMeta();
            String name = perkObj.getName();
            name = StringFormatter.replaceVars(name, currentLevel, perkObj, player);
            meta.setDisplayName(name);

            String lore = perkObj.getLore();

            lore = StringFormatter.replaceVars(lore, currentLevel, perkObj, player);

            List<String> fullLore = new ArrayList<>(Arrays.asList(lore.split("\\|\\|")));

            String loreLast;
            if (currentLevel < perkObj.getMaxLevel()) loreLast = perkObj.getLastLoreUpgrade();
            else loreLast = perkObj.getLastLoreMaxed();

            loreLast = StringFormatter.replaceVars(loreLast, currentLevel, perkObj, player);
            fullLore.addAll(Arrays.asList(loreLast.split("\\|\\|")));

            meta.setLore(fullLore);
            meta.addItemFlags(itemFlags);

            if (currentLevel > 0 && config.getEnchantWhenUpgraded())
                meta.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);

            item.setItemMeta(meta);

            return item;
        } catch (NullPointerException e) {
            player.sendMessage("Failed loading perks, please rejoin...");
            return null;
        }
    }

    static void fillContent(ItemStack[] content, ItemStack fillItem) {
        if (fillItem.getType() != Material.AIR)
            for (int i = 0; i < content.length; i++)
                if (content[i] == null || content[i].getType() == Material.AIR)
                    content[i] = fillItem;
    }

    static ItemStack formatFillingItem(ItemStack material) {
        if (material.getType() == Material.AIR)
            return new ItemStack(Material.AIR);

        ItemStack item = new ItemStack(material);

        item.setAmount(1);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(new ArrayList<>());
        meta.setDisplayName(ChatColor.RED + "");
        meta.addItemFlags(itemFlags);
        item.setItemMeta(meta);
        return item;
    }
}
