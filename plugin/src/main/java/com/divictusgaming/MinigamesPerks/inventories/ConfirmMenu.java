package com.divictusgaming.MinigamesPerks.inventories;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.Perk;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


public class ConfirmMenu extends Menu {
    private static ItemStack fillItem = null;

    public static void openConfirmMenu(Player player, Perk perk) {
        Main plugin = Main.getPlugin(Main.class);
        Inventory inventory = Bukkit.getServer().createInventory(player, 9 * plugin.getConfigObject().getConfirmInventorySize(),
                plugin.getConfigObject().getConfirmInventoryTitle());

        formatCustomItems(player, inventory, plugin.getConfigObject().getConfirmItems(), perk);

        ItemStack[] content = inventory.getContents();

        if (fillItem == null) fillItem = formatFillingItem(plugin.getConfigObject().getConfirmFillItem());
        fillContent(content, fillItem);

        inventory.setContents(content);
        player.closeInventory();
        player.openInventory(inventory);
    }
}
