package com.divictusgaming.MinigamesPerks.dto;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

public class ConditionResource {
    private Entity damager;
    private Entity victim;
    private ItemStack item;
    private Event event;
    private Block block;

    public ConditionResource(Entity damager, Entity victim) {
        this.damager = damager;
        this.victim = victim;
    }

    public ConditionResource(Entity damager, Entity victim, Block block) {
        this.damager = damager;
        this.victim = victim;
        this.block = block;
    }

    public ConditionResource(Entity damager, Entity victim, ItemStack item) {
        this.damager = damager;
        this.victim = victim;
        this.item = item;
    }

    public ConditionResource(Entity damager, Entity victim, Event event) {
        this.damager = damager;
        this.victim = victim;
        this.event = event;
    }

    public ConditionResource(Entity damager) {
        this.damager = damager;
    }

    public Entity getVictim() {
        return victim;
    }

    public Entity getDamager() {
        return damager;
    }

    public ItemStack getItem() {
        return item;
    }

    public Block getBlock() {
        return block;
    }

    public Event getEvent() {
        return event;
    }
}
