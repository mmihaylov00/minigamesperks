package com.divictusgaming.MinigamesPerks.dto;

public class LastDamagerStorage {
    private String attacker;
    private long time;

    public LastDamagerStorage(String attacker) {
        this.attacker = attacker;
        this.time = System.currentTimeMillis() / 1000L;
    }

    public String getAttacker() {
        return attacker;
    }

    public long getTime() {
        return time;
    }
}
