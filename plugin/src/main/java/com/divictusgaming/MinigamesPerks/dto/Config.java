package com.divictusgaming.MinigamesPerks.dto;

import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map;

public class Config {
    private final String invTitle;
    private final Integer invSize;
    private final ItemStack fillItem;
    private final String ecoCommand;
    private final String ecoPlaceholder;
    private final Integer lastDamagerTimeSec;
    private final Map<String, CustomItem> customItems;
    private final Boolean confirmEnabled;
    private final String confirmInventoryTitle;
    private final Integer confirmInventorySize;
    private final List<String> disabledWorlds;
    private final ItemStack confirmFillItem;
    private final Map<String, CustomItem> confirmItems;
    private final Boolean needPermission;
    private final Boolean zeroWhenNotUpgraded;
    private final Boolean showLevelNumber;
    private final Boolean enchantWhenUpgraded;

    public Config(String invTitle, Integer invSize, ItemStack fillItem, String ecoCommand, String ecoPlaceholder, Integer lastDamagerTimeSec,
                  Map<String, CustomItem> customItems, Boolean confirmEnabled, String confirmInventoryTitle, Integer confirmInventorySize,
                  ItemStack confirmFillItem, Map<String, CustomItem> confirmItems, List<String> disabledWorlds, Boolean needPermission,
                  Boolean zeroWhenNotUpgraded, Boolean showLevelNumber, Boolean enchantWhenUpgraded) {
        this.invTitle = invTitle;
        this.invSize = invSize;
        this.fillItem = fillItem;
        this.ecoCommand = ecoCommand;
        this.ecoPlaceholder = ecoPlaceholder;
        this.lastDamagerTimeSec = lastDamagerTimeSec;
        this.customItems = customItems;
        this.confirmEnabled = confirmEnabled;
        this.confirmInventoryTitle = confirmInventoryTitle;
        this.confirmInventorySize = confirmInventorySize;
        this.confirmFillItem = confirmFillItem;
        this.confirmItems = confirmItems;
        this.disabledWorlds = disabledWorlds;
        this.needPermission = needPermission;
        this.zeroWhenNotUpgraded = zeroWhenNotUpgraded;
        this.showLevelNumber = showLevelNumber;
        this.enchantWhenUpgraded = enchantWhenUpgraded;
    }

    public String getInvTitle() {
        return invTitle;
    }

    public Integer getInvSize() {
        return invSize;
    }

    public ItemStack getInvFill() {
        return fillItem;
    }

    public String getEcoCommand() {
        return ecoCommand;
    }

    public String getEcoPlaceholder() {
        return ecoPlaceholder;
    }

    public Integer getLastDamagerTimeSec() {
        return lastDamagerTimeSec;
    }

    public Map<String, CustomItem> getCustomItems() {
        return customItems;
    }

    public ItemStack getFillItem() {
        return fillItem;
    }

    public Boolean getConfirmEnabled() {
        return confirmEnabled;
    }

    public String getConfirmInventoryTitle() {
        return confirmInventoryTitle;
    }

    public Integer getConfirmInventorySize() {
        return confirmInventorySize;
    }

    public ItemStack getConfirmFillItem() {
        return confirmFillItem;
    }

    public Map<String, CustomItem> getConfirmItems() {
        return confirmItems;
    }

    public List<String> getDisabledWorlds() {
        return disabledWorlds;
    }

    public Boolean getNeedPermission() {
        return needPermission;
    }

    public Boolean getZeroWhenNotUpgraded() {
        return zeroWhenNotUpgraded;
    }

    public Boolean getShowLevelNumber() {
        return showLevelNumber;
    }

    public Boolean getEnchantWhenUpgraded() {
        return enchantWhenUpgraded;
    }
}
