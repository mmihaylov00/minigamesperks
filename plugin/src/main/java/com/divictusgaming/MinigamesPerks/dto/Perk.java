package com.divictusgaming.MinigamesPerks.dto;

import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Perk {
    private final String id;
    private final String name;
    private final String lore;
    private final String permission;
    private final String permissionMessage;
    private final String lastLoreUpgrade;
    private final String lastLoreMaxed;
    private final Double cost;
    private final Double costIncrease;
    private final Double percentIncrease;
    private final Integer maxLevel;
    private final Integer position;
    private final Boolean sendMessage;
    private final String message;
    private final ItemStack displayItem;
    private final Integer startingLevel;
    private final List<String> upgradeCommands;

    public Perk(String id, String name, String permission, String permissionMessage, String lore, String lastLoreUpgrade, String lastLoreMaxed, Double cost,
                Double costIncrease, Double percentIncrease, Integer maxLevel, Integer position, Boolean sendMessage,
                String message, ItemStack displayItem, Integer startingLevel, List<String> upgradeCommands) {
        this.id = id;
        this.name = name;
        this.permission = permission;
        this.permissionMessage = permissionMessage;
        this.lore = lore;
        this.lastLoreUpgrade = lastLoreUpgrade;
        this.lastLoreMaxed = lastLoreMaxed;
        this.cost = cost;
        this.costIncrease = costIncrease;
        this.percentIncrease = percentIncrease;
        this.maxLevel = maxLevel;
        this.position = position;
        this.sendMessage = sendMessage;
        this.message = message;
        this.displayItem = displayItem;
        this.startingLevel = startingLevel;
        this.upgradeCommands = upgradeCommands;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLore() {
        return lore;
    }

    public String getLastLoreUpgrade() {
        return lastLoreUpgrade;
    }

    public String getLastLoreMaxed() {
        return lastLoreMaxed;
    }

    public Double getCost() {
        return cost;
    }

    public Double getCostIncrease() {
        return costIncrease;
    }

    public Double getPercentIncrease() {
        return percentIncrease;
    }

    public Integer getMaxLevel() {
        return maxLevel;
    }

    public Integer getPosition() {
        return position;
    }

    public Boolean getSendMessage() {
        return sendMessage;
    }

    public String getMessage() {
        return message;
    }

    public ItemStack getDisplayItem() {
        return displayItem;
    }

    public String getPermission() {
        return permission;
    }

    public String getPermissionMessage() {
        return permissionMessage;
    }

    public Integer getStartingLevel() {
        return startingLevel;
    }

    public List<String> getUpgradeCommands() {
        return upgradeCommands;
    }
}
