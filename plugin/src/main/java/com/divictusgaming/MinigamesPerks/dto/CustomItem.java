package com.divictusgaming.MinigamesPerks.dto;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class CustomItem {
    private ItemStack item;
    private List<String> commands;
    private boolean close;
    private boolean cancel;
    private boolean confirm;
    private boolean isPerkItem;
    private ArrayList<Integer> positions;

    public CustomItem() {
        commands = new ArrayList<>();
        positions = new ArrayList<>();
        close = false;
        cancel = false;
        confirm = false;
        isPerkItem = false;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public List<String> getCommands() {
        return commands;
    }

    public void addCommands(String command) {
        this.commands.add(command);
    }

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean close) {
        this.close = close;
    }

    public boolean isCancel() {
        return cancel;
    }

    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    public void setCommands(List<String> commands) {
        this.commands = commands;
    }

    public ArrayList<Integer> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<Integer> positions) {
        this.positions = positions;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    public boolean isPerkItem() {
        return isPerkItem;
    }

    public void setPerkItem(boolean perkItem) {
        isPerkItem = perkItem;
    }
}
