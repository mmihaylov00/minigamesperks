package com.divictusgaming.MinigamesPerks.database;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.entities.PerksEntity;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FileDatabase implements IDatabase {
    private final HashMap<String, PerksEntity> users;
    private final FileConfiguration database;
    private final File file;

    public FileDatabase() {
        users = new HashMap<>();
        File path = Main.getPlugin(Main.class).getPluginsFolder();
        file = new File(path + File.separator + "database.yml");
        database = YamlConfiguration.loadConfiguration(file);
    }

    public void saveAllPlayers() {
        for (Map.Entry<String, PerksEntity> users : users.entrySet()) {
            updateData(users.getKey(), false);
        }
    }

    public void loadData(UUID uuid) {
        loadData(uuid.toString());
    }

    public void loadData(String uuid) {
        if (database.isSet("users." + uuid)) {
            ConfigurationSection section = database.getConfigurationSection("users." + uuid);
            users.put(uuid, new PerksEntity(section));
        } else {
            users.put(uuid, new PerksEntity());
            createUser(uuid);
        }
    }

    @Override
    public void loadAllPlayers() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            loadData(player.getUniqueId());
        }
    }

    private void createUser(String uuid) {
        for (String perk : Main.getPlugin(Main.class).getPerks().getPerksObject().keySet()) {
            database.set("users." + uuid + "." + perk, 0);
        }
    }

    public void updateData(UUID uuid, boolean remove) {
        updateData(uuid.toString(), remove);
    }

    public void updateData(String uuid, boolean remove) {
        PerksEntity perks = users.get(uuid);
        if (perks.getUpdated()) {
            for (Map.Entry<String, Integer> entry : perks.getPerkLevels().entrySet())
                database.set("users." + uuid + "." + entry.getKey(), entry.getValue());

            try {
                database.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (remove) users.remove(uuid);
    }

    public void setLevel(UUID uuid, String perk, int level) {
        PerksEntity perksEntity = users.get(uuid.toString());

        perksEntity.getPerkLevels().replace(perk, level);
        perksEntity.update();
    }

    public HashMap<String, Integer> getLevels(UUID uuid) {
        PerksEntity p = users.get(uuid.toString());

        return p == null ? null : p.getPerkLevels();
    }
}
