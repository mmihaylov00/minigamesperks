package com.divictusgaming.MinigamesPerks.database;


import java.util.HashMap;
import java.util.UUID;

public interface IDatabase {
    void saveAllPlayers();
    void loadData(UUID uuid);
    void loadData(String uuid);
    void loadAllPlayers();
    void updateData(UUID uuid, boolean remove);
    void updateData(String uuid, boolean remove);
    void setLevel(UUID uuid, String perk, int level);
    HashMap<String, Integer> getLevels(UUID uuid);
}
