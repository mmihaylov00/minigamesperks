package com.divictusgaming.MinigamesPerks.database;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.entities.PerksEntity;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SQLDatabase implements IDatabase {
    private Connection connection;
    private final HashMap<String, PerksEntity> users;
    private Boolean isWorking = true;

    public SQLDatabase(String url, String username, String password) {
        users = new HashMap<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
        connect(url, username, password);
    }

    private void connect(String url, String username, String password) {
        try {
            connection = DriverManager.getConnection(url, username, password);
            generateTable();
        } catch (SQLException e) {
            if (!url.endsWith("&characterEncoding=latin1")) {
                connect(url + "&characterEncoding=latin1", username, password);
            } else {
                isWorking = false;
                e.printStackTrace();
            }
        }
    }

    public void closeConnection() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveAllPlayers() {
        for (Map.Entry<String, PerksEntity> users : users.entrySet()) {
            updateData(users.getKey(), false);
        }
    }

    private void generateTable() {
        String sql = "CREATE TABLE IF NOT EXISTS minigames_perks(uuid varchar(64) PRIMARY KEY NOT NULL);";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();
            generateColumns();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void generateColumns() {
        for (String value : Main.getPlugin(Main.class).getPerks().getPerksObject().keySet()) {
            String sql = "ALTER TABLE minigames_perks ADD " + value.replaceAll("-", "_") + "_LEVEL INT DEFAULT 0 NOT NULL;";
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.executeUpdate();
            } catch (SQLException ignored) {
            }
        }
    }

    public void loadData(UUID uuid) {
        loadData(uuid.toString());
    }

    public void loadData(String uuid) {
        String sql = "SELECT * FROM minigames_perks WHERE uuid = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                users.put(uuid, new PerksEntity(result));
            } else {
                users.put(uuid, new PerksEntity());
                createUser(uuid);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void loadAllPlayers() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            loadData(player.getUniqueId());
        }
    }

    public void createUser(UUID uuid) {
        createUser(uuid.toString());
    }

    public void createUser(String uuid) {
        String sql = "INSERT INTO minigames_perks (uuid) values(?);";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void updateData(UUID uuid, boolean remove) {
        updateData(uuid.toString(), remove);
    }

    public void updateData(String uuid, boolean remove) {
        PerksEntity perks = users.get(uuid);
        if (perks == null){
            return;
        }
        if (perks.getUpdated()) {
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE minigames_perks SET ");
            ArrayList<String> values = new ArrayList<>(Main.getPlugin(Main.class).getPerks().getPerksObject().keySet());
            for (String value : values) {
                String perk = value.replaceAll("-", "_") + "_LEVEL";
                sb.append(perk).append(" = ?, ");
            }
            sb.delete(sb.length() - 2, sb.length());
            sb.append(" WHERE uuid = ?;");

            try {
                PreparedStatement preparedStatement = connection.prepareStatement(sb.toString());
                for (int i = 1; i <= values.size(); i++) {
                    preparedStatement.setInt(i, perks.getPerkLevels().get(values.get(i - 1)));
                }
                preparedStatement.setString(values.size() + 1, uuid);
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        if (remove) users.remove(uuid);

    }

    public void setLevel(UUID uuid, String perk, int level) {
        PerksEntity perksEntity = users.get(uuid.toString());

        perksEntity.getPerkLevels().replace(perk, level);
        perksEntity.update();
    }

    public HashMap<String, Integer> getLevels(UUID uuid) {
        return users.get(uuid.toString()).getPerkLevels();
    }

    public Boolean getWorking() {
        return isWorking;
    }
}
