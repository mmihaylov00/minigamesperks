package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import com.divictusgaming.MinigamesPerks.utils.RomanConventer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.potion.PotionEffect;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EffectPrize implements IPrize {
    private Integer giveTo;
    private List<PotionEffect> effects;
    private Integer giveSize;

    public EffectPrize(Integer giveTo, List<PotionEffect> effects, Integer giveSize) {
        this.giveTo = giveTo;
        this.effects = effects;
        if (giveSize == 0) {
            this.giveSize = effects.size();
        } else {
            this.giveSize = giveSize;
        }

    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) {
        LivingEntity endEntity;
        if (entity.length == 1) endEntity = entity[0];
         else endEntity = entity[giveTo];

        StringBuilder sb = new StringBuilder();
        HashMap<String, String> placeholders = new HashMap<>();

        Main plugin = Main.getPlugin(Main.class);

        for (int i = 0; i < giveSize; i++) {
            PotionEffect potion = effects.get(i);
            if (!endEntity.hasPotionEffect(potion.getType())) {
                endEntity.addPotionEffect(new PotionEffect(potion.getType(), potion.getDuration(), potion.getAmplifier() - 1));
            } else {
                for (PotionEffect activePotionEffect : endEntity.getActivePotionEffects()) {
                    if (activePotionEffect.getType() == potion.getType()) {
                        if (activePotionEffect.getAmplifier() < potion.getAmplifier() - 1) {
                            endEntity.removePotionEffect(potion.getType());
                            endEntity.addPotionEffect(new PotionEffect(potion.getType(), potion.getDuration(),
                                    potion.getAmplifier() - 1));
                        } else if (activePotionEffect.getAmplifier() == potion.getAmplifier() - 1) {
                            endEntity.removePotionEffect(potion.getType());
                            endEntity.addPotionEffect(new PotionEffect(potion.getType(), potion.getDuration() + activePotionEffect.getDuration(),
                                    potion.getAmplifier() - 1));
                        }
                        break;
                    }
                }
            }
            if (giveSize == 1) {
                placeholders.put("\\{effect}", plugin.getINMSMain().getEffectName(potion.getType().getName()));
                placeholders.put("\\{amplifier}", RomanConventer.toRoman(potion.getAmplifier()));
                placeholders.put("\\{duration}", potion.getDuration() / 20 + " seconds");
            } else
                sb.append(plugin.getINMSMain().getEffectName(potion.getType().getName()));

            if (i + 1 < giveSize) sb.append(", ");

        }
        if (giveSize != 1)
            placeholders.put("\\{effect}", plugin.getINMSMain().getEffectName(sb.toString()));


        return placeholders;
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }
}
