package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.Event;

import java.util.Map;

public class SpawnTNTPrize implements IPrize {
    private Integer spawnAt;

    public SpawnTNTPrize(Integer spawnAt) {
        this.spawnAt = spawnAt;
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) {
        LivingEntity player;
        if (entity.length == 1) {
            player = entity[0];
        } else {
            player = entity[spawnAt];
        }

        TNTPrimed tnt = player.getWorld().spawn(player.getLocation().add(0, 1, 0), TNTPrimed.class);
        tnt.setFuseTicks(20 * 3);
        return null;
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }
}
