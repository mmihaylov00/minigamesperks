package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.Map;

public class SmitePrize implements IPrize {
    private final Double damage;

    public SmitePrize(Double damage) {
        this.damage = damage;
    }
    @Override
    public Map<String, String> givePrize(LivingEntity... entity) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }

    @Override
    public Map<String, String> givePrize(Event event) {
        if (!(event instanceof EntityDamageByEntityEvent)) return null;
        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        e.getEntity().getWorld().strikeLightningEffect(e.getEntity().getLocation());
        e.getEntity().setFireTicks(60);
        e.setDamage(e.getDamage() + this.damage);
        return null;
    }
}
