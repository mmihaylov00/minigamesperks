package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.Map;

public class IncreaseDamagePrize implements IPrize {
    private Double bonus;

    public IncreaseDamagePrize(Double bonus) {
        this.bonus = bonus;
    }
    @Override
    public Map<String, String> givePrize(LivingEntity... entity) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }

    @Override
    public Map<String, String> givePrize(Event event) {
        double damage = ((EntityDamageByEntityEvent) event).getDamage() * bonus/100;
        ((EntityDamageByEntityEvent) event).setDamage(((EntityDamageByEntityEvent) event).getDamage() + damage);
        return null;
    }
}
