package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import java.util.Map;

public class ExpLevelPrize implements IPrize {
    private Integer bonus;

    public ExpLevelPrize(Integer bonus) {
        this.bonus = bonus;
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) {
        ((Player) entity[0]).giveExpLevels(bonus);
        return null;
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }
}
