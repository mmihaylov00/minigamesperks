package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityShootBowEvent;

import java.util.Map;

public class FireArrowPrize implements IPrize {

    public FireArrowPrize() {
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        if (event instanceof EntityShootBowEvent){
            ((EntityShootBowEvent) event).getProjectile().setFireTicks(Integer.MAX_VALUE);
        }
        throw new EmptyPrizeException();
    }
}
