package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.Map;

public class ReduceDamagePrize implements IPrize {
    private Double bonus;

    public ReduceDamagePrize(Double bonus) {
        this.bonus = bonus;
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }

    @Override
    public Map<String, String> givePrize(Event event) {
        if (event instanceof EntityDamageEvent) {
            double damage = ((EntityDamageEvent) event).getDamage() * bonus / 100;
            ((EntityDamageEvent) event).setDamage(((EntityDamageEvent) event).getDamage() - damage);
        }
        return null;
    }
}
