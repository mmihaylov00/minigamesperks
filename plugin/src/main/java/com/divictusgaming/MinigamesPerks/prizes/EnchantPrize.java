package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnchantPrize implements IPrize {

    private final List<Enchantment> enchants;
    private final List<Material> materials;
    private final Integer giveSize;
    private final boolean isHand;

    public EnchantPrize(List<Enchantment> enchants, List<Material> materials, Integer giveSize, boolean isHand) {
        this.enchants = enchants;
        this.materials = materials;
        if (giveSize == 0) this.giveSize = enchants.size();
        else this.giveSize = giveSize;

        this.isHand = isHand;
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) {
        Player player = (Player) entity[0];
        HashMap<String, String> placeholders = new HashMap<>();

        Collections.shuffle(enchants);
        Main plugin = Main.getPlugin(Main.class);
        if (giveSize == 1) placeholders.put("\\{enchant}", plugin.getINMSMain().getEnchantRealName(enchants.get(0)));
        else {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < giveSize; i++) {
                sb.append(plugin.getINMSMain().getEnchantRealName(enchants.get(0)));
                if (i + 1 < giveSize) sb.append(", ");
            }
            placeholders.put("\\{enchant}", sb.toString());
        }
        if (isHand) {
            ItemStack itemStack = player.getInventory().getItemInMainHand();
            for (int i = 0; i < giveSize; i++) {
                Enchantment enchant = enchants.get(i);
                int ench = itemStack.getEnchantmentLevel(enchant);
                itemStack.addUnsafeEnchantment(enchant, ench + 1);
            }
        } else {
            for (ItemStack itemStack : player.getInventory()) {
                if (itemStack != null && checkMaterial(itemStack.getType())) {
                    for (int i = 0; i < giveSize; i++) {
                        Enchantment enchant = enchants.get(i);
                        int ench = itemStack.getEnchantmentLevel(enchant);
                        itemStack.addUnsafeEnchantment(enchant, ench + 1);
                    }
                }
            }
        }
        return placeholders;
    }

    private boolean checkMaterial(Material material) {
        for (Material m : materials) {
            if (m == material) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }
}
