package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import java.util.List;
import java.util.Map;

public class ConsoleCommandPrize implements IPrize {
    private List<String> commands;

    public ConsoleCommandPrize(List<String> commands) {
        this.commands = commands;
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) {
        Player player = (Player) entity[0];
        Player victim = null;
        if (entity.length > 1 && entity[1] instanceof Player) {
            victim = (Player) entity[1];
        }
        for (String command : commands) {
            String finalCmd = command.replaceAll("\\{player}", player.getName());
            if (victim != null) {
                finalCmd = finalCmd.replaceAll("\\{victim}", victim.getName());
            }
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), finalCmd);
        }
        return null;
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }
}
