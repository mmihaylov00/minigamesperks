package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import java.util.List;
import java.util.Map;

public class PlayerCommandPrize implements IPrize {
    private List<String> commands;

    public PlayerCommandPrize(List<String> commands) {
        this.commands = commands;
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) {
        Player player = (Player) entity[0];
        Player victim = null;
        if (entity.length > 1 && entity[1] instanceof Player) {
            victim = (Player) entity[1];
        }
        for (String command : commands) {
            String finalCmd = command.replaceAll("\\{player}", player.getName());
            if (victim != null) {
                finalCmd = finalCmd.replaceAll("\\{victim}", victim.getName());
            }
            player.performCommand(finalCmd);
        }
        return null;
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }
}
