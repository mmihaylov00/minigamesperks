package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;

import java.util.Map;

public class HealPrize implements IPrize {
    private Integer healthBonus;

    public HealPrize(Integer healthBonus) {
        this.healthBonus = healthBonus;
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) {
        double hp = entity[0].getHealth() + healthBonus;
        if (hp > entity[0].getMaxHealth()) {
            hp = entity[0].getMaxHealth();
        }
        entity[0].setHealth(hp);
        return null;
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }
}
