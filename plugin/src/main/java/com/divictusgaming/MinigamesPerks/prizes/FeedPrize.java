package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import java.util.Map;

public class FeedPrize implements IPrize {
    private Integer foodBonus;

    public FeedPrize(Integer foodBonus) {
        this.foodBonus = foodBonus;
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) {
        Player player = (Player) entity[0];
        player.setFoodLevel(player.getFoodLevel() + foodBonus);
        return null;
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }
}
