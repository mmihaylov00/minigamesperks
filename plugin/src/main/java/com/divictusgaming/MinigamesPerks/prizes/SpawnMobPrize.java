package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import com.divictusgaming.MinigamesPerks.utils.RandomGenerator;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Wolf;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerItemConsumeEvent;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SpawnMobPrize implements IPrize {
    private Integer spawnAt;
    private List<Class<? extends Entity>> mobs;
    private List<Integer> params;

    public SpawnMobPrize(Integer spawnAt, List<Class<? extends Entity>> mobs, List<Integer> params) {
        this.spawnAt = spawnAt;
        this.mobs = mobs;
        this.params = params;
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) throws EmptyPrizeException {
        LivingEntity player;
        try {
            player = entity[spawnAt];
        } catch (IndexOutOfBoundsException e) {
            throw new EmptyPrizeException();
        }

        Collections.shuffle(mobs);
        player.getWorld().spawn(player.getLocation(), mobs.get(0));
        HashMap<String, String> placeholders = new HashMap<>();
        placeholders.put("\\{mob}", mobs.get(0).getSimpleName());

        return placeholders;
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        Location loc = null;
        if (event instanceof EntityDamageByEntityEvent) {
            loc = ((EntityDamageByEntityEvent) event).getDamager().getLocation();
        } else if (event instanceof ProjectileHitEvent) {
            if (((ProjectileHitEvent) event).getHitEntity() != null &&
                    ((ProjectileHitEvent) event).getHitEntity().getLocation() != null) {
                loc = ((ProjectileHitEvent) event).getHitEntity().getLocation();
            } else {
                loc = ((ProjectileHitEvent) event).getHitBlock().getLocation();
            }
        } else if (event instanceof BlockPlaceEvent) {
            loc = ((BlockPlaceEvent) event).getBlockPlaced().getLocation();
        } else if (event instanceof BlockBreakEvent) {
            loc = ((BlockBreakEvent) event).getBlock().getLocation();
        } else if (event instanceof EntityDamageEvent) {
            loc = ((EntityDamageEvent) event).getEntity().getLocation();
        } else if (event instanceof EntityShootBowEvent) {
            loc = ((EntityShootBowEvent) event).getEntity().getLocation();
        } else if (event instanceof PlayerDeathEvent) {
            loc = ((PlayerDeathEvent) event).getEntity().getLocation();
        } else if (event instanceof PlayerItemConsumeEvent) {
            loc = ((PlayerItemConsumeEvent) event).getPlayer().getLocation();
        } else {
            return null;
        }
        int random = RandomGenerator.getRandom(mobs.size());
        Class<? extends Entity> mob = mobs.get(random);
        Integer param = params.get(random);
        Entity entity = loc.getWorld().spawn(loc, mob);
        try {
            if (entity instanceof Sheep) {
                ((Sheep) entity).setColor(DyeColor.getByWoolData(param.byteValue()));
            }
            if (entity instanceof Wolf) {
                ((Wolf) entity).setCollarColor(DyeColor.getByWoolData(param.byteValue()));
            }
        }catch (NullPointerException e){
            Bukkit.getLogger().warning(ChatColor.RED + "There is a spawn-mob prize with color param that's not existing. Defaulting, please fix!");
        }
        //todo horse types and whatever else there is
        HashMap<String, String> placeholders = new HashMap<>();
        placeholders.put("\\{mob}", mobs.get(random).getSimpleName());

        return placeholders;
    }
}
