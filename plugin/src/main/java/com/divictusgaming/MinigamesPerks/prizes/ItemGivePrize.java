package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ItemGivePrize implements IPrize {
    private List<ItemStack> items;
    private Boolean hasHandBlock;
    private Boolean hasBrokenBlock;
    private Integer giveItems;

    public ItemGivePrize(List<ItemStack> items, Integer giveItems) {
        this(items, false, false, giveItems);
    }

    public ItemGivePrize(List<ItemStack> items, Boolean hasHandBlock, Boolean hasBrokenBlock, Integer giveItems) {
        this.items = items;
        this.hasHandBlock = hasHandBlock;
        this.hasBrokenBlock = hasBrokenBlock;
        if (giveItems == 0) {
            this.giveItems = items.size();
        } else {
            this.giveItems = giveItems;
        }
    }

    public ItemGivePrize(Boolean hasHandBlock, Boolean hasBrokenBlock, Integer giveItems) {
        this(new ArrayList<>(), hasHandBlock, hasBrokenBlock, giveItems);
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) throws EmptyPrizeException {
        if (giveItems == 0 || giveItems >= items.size()) {
            for (ItemStack item : items) {
                ((Player) entity[0]).getInventory().addItem(item);
            }
        } else {
            Collections.shuffle(items);
            for (int i = 0; i < giveItems; i++) {
                ((Player) entity[0]).getInventory().addItem(items.get(i));
            }
        }
        if (hasHandBlock) {
            throw new EmptyPrizeException();
        }
        if (hasBrokenBlock) {
            throw new EmptyPrizeException();
        }
        return null;
    }

    @Override
    public Map<String, String> givePrize(Event event) throws EmptyPrizeException {
        if (hasBrokenBlock) {
            if (!(event instanceof BlockBreakEvent)) throw new EmptyPrizeException();
            Main plugin = Main.getPlugin(Main.class);
            for (ItemStack drop : ((BlockBreakEvent) event).getBlock()
                    .getDrops(plugin.getINMSMain()
                            .getItemInHand(((BlockBreakEvent) event).getPlayer().getInventory(), 0))) {
                ((BlockBreakEvent) event).getPlayer().getInventory().addItem(drop);
                break;
            }
        }
        if (hasHandBlock) {
            if (!(event instanceof BlockPlaceEvent)) {
                throw new EmptyPrizeException();
            }
            ItemStack item = new ItemStack(((BlockPlaceEvent) event).getItemInHand());
            item.setAmount(1);
            Player player = ((BlockPlaceEvent) event).getPlayer();
            if (((BlockPlaceEvent) event).getItemInHand().getAmount() > 1) {
                ((BlockPlaceEvent) event).getItemInHand().setAmount(((BlockPlaceEvent) event).getItemInHand().getAmount() - 1);
                player.getInventory().addItem(item);
            } else {
                int slot = player.getInventory().getHeldItemSlot();
                player.getInventory().setItem(slot, item);
            }
        }
        return null;
    }
}
