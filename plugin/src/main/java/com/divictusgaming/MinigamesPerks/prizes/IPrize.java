package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;

import java.util.Map;

public interface IPrize {
    Map<String, String> givePrize(LivingEntity... entity) throws EmptyPrizeException;
    Map<String, String> givePrize(Event event) throws EmptyPrizeException;
}
