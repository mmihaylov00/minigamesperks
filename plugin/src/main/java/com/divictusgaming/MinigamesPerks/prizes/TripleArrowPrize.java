package com.divictusgaming.MinigamesPerks.prizes;

import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.util.Vector;

import java.util.Map;

public class TripleArrowPrize implements IPrize {

    public TripleArrowPrize() {
    }

    @Override
    public Map<String, String> givePrize(LivingEntity... entity) throws EmptyPrizeException {
        throw new EmptyPrizeException();
    }

    @Override
    public Map<String, String> givePrize(Event event) {
        EntityShootBowEvent e = (EntityShootBowEvent) event;
        //left arrow
        Arrow a = e.getEntity().getWorld().spawnArrow(e.getEntity().getLocation().clone().add(0, 1.6, 0),
                rotateVector(e.getProjectile().getVelocity(), 0.1), e.getForce() * 4, 0f);
        a.setShooter(e.getEntity());
        //right arrow
        Arrow a2 = e.getEntity().getWorld().spawnArrow(e.getEntity().getLocation().clone().add(0, 1.6, 0),
                rotateVector(e.getProjectile().getVelocity(), -0.11), e.getForce() * 4, 0f);
        a2.setShooter(e.getEntity());
        return null;
    }

    private Vector rotateVector(Vector vector, double whatAngle) {
        double sin = Math.sin(whatAngle);
        double cos = Math.cos(whatAngle);
        double x = vector.getX() * cos + vector.getZ() * sin;
        double z = vector.getX() * -sin + vector.getZ() * cos;

        return vector.setX(x).setZ(z);
    }
}
