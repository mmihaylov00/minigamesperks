package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.List;

public class ProjectileHitEntityEvent extends CustomEvent {
    public ProjectileHitEntityEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onProjectileHit(EntityDamageByEntityEvent event) {
        ConditionResource conditionRes = new ConditionResource(event.getDamager(), event.getEntity());
        if (checkConditions(conditionRes)) return;

        Projectile projectile = (Projectile) event.getDamager();
        Player player = (Player) projectile.getShooter();
        giveReward(event, player);
    }
}
