package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.List;

public class DamageTakeEvent extends CustomEvent {
    public DamageTakeEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onHit(EntityDamageEvent event) {
        if (event instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent edee = (EntityDamageByEntityEvent) event;
            ConditionResource conditionRes = new ConditionResource(edee.getDamager(), event.getEntity(), event);
            if (checkConditions(conditionRes)) return;

        } else {
            ConditionResource conditionRes = new ConditionResource(event.getEntity(), event.getEntity(), event);
            if (checkConditions(conditionRes)) return;
        }

        giveReward(event, (LivingEntity) event.getEntity(), (LivingEntity) event.getEntity());
    }
}
