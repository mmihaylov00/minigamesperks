package com.divictusgaming.MinigamesPerks.events;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.CustomItem;
import com.divictusgaming.MinigamesPerks.enums.DefaultMessages;
import com.divictusgaming.MinigamesPerks.inventories.MainMenu;
import com.divictusgaming.MinigamesPerks.utils.PerkUpgrader;
import com.divictusgaming.MinigamesPerks.utils.StringFormatter;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class InventoryEvents implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        ItemStack clickedItem = event.getCurrentItem();
        if (clickedItem == null || clickedItem.getType() == Material.AIR)
            clickedItem = event.getCursor();

        Main plugin = Main.getPlugin(Main.class);

        if (event.getView().getTitle().equals(plugin.getConfigObject().getInvTitle())) {
            event.setResult(Event.Result.DENY);
            event.setCancelled(true);
            String perkName = plugin.getINMSMain().getPerkName("perk.type", clickedItem);
            if (perkName != null) {
                Player player = (Player) event.getWhoClicked();

                if (perkName.startsWith(":")) {
                    CustomItem i = plugin.getConfigObject().getCustomItems().get(perkName.split(":")[1]);
                    executeCommands(i, player, perkName);
                } else {
                    if (perkName.equals(".")) {
                        return;
                    }

                    upgradePerk(player, perkName, plugin.getConfigObject().getConfirmEnabled());
                }
            }
        } else if (event.getView().getTitle().equals(plugin.getConfigObject().getConfirmInventoryTitle())) {
            event.setResult(Event.Result.DENY);
            event.setCancelled(true);
            String perkName = plugin.getINMSMain().getPerkName("perk.type", clickedItem);
            if (perkName != null) {
                Player player = (Player) event.getWhoClicked();

                if (perkName.startsWith(":")) {
                    CustomItem i = plugin.getConfigObject().getConfirmItems().get(perkName.split(":")[2]);
                    executeCommands(i, player, perkName.split(":")[1]);
                }
            }

        } else if (clickedItem != null) {
            if (plugin.getINMSMain().getItemBoolean("is.from.menu", clickedItem)) {
                event.getWhoClicked().setItemOnCursor(null);
                event.setCurrentItem(null);
            }

        }
    }

    private void executeCommands(CustomItem i, Player player, String perkName) {
        if (i.isCancel()) {
            player.closeInventory();
            MainMenu.openMainMenu(player);
        }
        if (i.isClose())
            player.closeInventory();

        if (i.isConfirm())
            upgradePerk(player, perkName, false);


        final String regex = "%[A-Za-z_{}.0-9]+%";
        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        for (String cmd : i.getCommands()) {
            String command = cmd;
            final Matcher matcher = pattern.matcher(command);
            while (matcher.find()) {
                for (int j = 0; j <= matcher.groupCount(); j++) {
                    String val = PlaceholderAPI.setPlaceholders(player, matcher.group(j));
                    command = command.replace(matcher.group(j), val);
                }
            }
            if (command.startsWith("[message]")) {
                command = command.substring(9).trim()
                        .replaceFirst("\\{prefix}", (String) Main.getPlugin(Main.class)
                        .getMessages().get(DefaultMessages.PREFIX.getPath()));
                player.sendMessage(StringFormatter.format(command));
            } else {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
                        command.replaceAll("\\{player}", player.getName()));
            }
        }
    }

    private void upgradePerk(Player player, String perkName, boolean openConfirm) {
        boolean isUpgraded = PerkUpgrader.upgradePerk(player, perkName, openConfirm);
        if (isUpgraded) MainMenu.openMainMenu(player);
    }
}
