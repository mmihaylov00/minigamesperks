package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.entity.Player;

import java.util.List;

public class CommandEvent extends CustomEvent {
    public CommandEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    public void executeCommand(Player player) {

        ConditionResource conditionRes = new ConditionResource(player, null);
        if (checkConditions(conditionRes)) return;

        giveReward(null, player);
    }
}
