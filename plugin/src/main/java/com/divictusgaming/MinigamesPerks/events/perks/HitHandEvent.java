package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.List;

public class HitHandEvent extends CustomEvent {
    public HitHandEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    @EventHandler
    public void onHit(EntityDamageByEntityEvent event) {
        ConditionResource conditionRes = new ConditionResource(event.getDamager(), event.getEntity());
        if (checkConditions(conditionRes)) return;
        if (event.getEntity() instanceof LivingEntity)
            giveReward(event, (LivingEntity) event.getDamager(), (LivingEntity) event.getEntity());
        else
            giveReward(event, (LivingEntity) event.getDamager(), null);
    }
}
