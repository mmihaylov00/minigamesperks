package com.divictusgaming.MinigamesPerks.events;

import com.divictusgaming.MinigamesPerks.Main;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerEvents implements Listener {

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.getProvidingPlugin(Main.class),
                () -> Main.getPlugin(Main.class)
                        .getDb().loadData(event.getPlayer().getUniqueId()));
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.getProvidingPlugin(Main.class),
                () -> Main.getPlugin(Main.class)
                        .getDb().updateData(event.getPlayer().getUniqueId(), true));
    }

}
