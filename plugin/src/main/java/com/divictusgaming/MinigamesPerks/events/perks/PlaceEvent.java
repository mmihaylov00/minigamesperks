package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.List;

public class PlaceEvent extends CustomEvent {
    public PlaceEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlace(BlockPlaceEvent event) {
        ConditionResource conditionRes = new ConditionResource(event.getPlayer(), null, event.getBlock());
        if (checkConditions(conditionRes)) return;

        giveReward(event, event.getPlayer());
    }
}
