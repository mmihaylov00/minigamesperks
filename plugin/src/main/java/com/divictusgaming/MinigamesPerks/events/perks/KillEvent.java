package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.dto.LastDamagerStorage;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.List;

public class KillEvent extends CustomEvent {
    public KillEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onKill(EntityDeathEvent event) {
        ConditionResource conditionRes = new ConditionResource(event.getEntity().getKiller(), event.getEntity());
        if (checkConditions(conditionRes)) return;

        if (event.getEntity().getKiller() == null) {
            Main plugin = Main.getPlugin(Main.class);
            LastDamagerStorage storage = plugin.getStorage().getLastDamagerStorage(event.getEntity().getName());
            if (storage != null) {
                Player p = Bukkit.getPlayer(storage.getAttacker());
                if (p != null) {
                    Integer lastDamagerTimeSec = plugin.getConfigObject().getLastDamagerTimeSec();
                    if (System.currentTimeMillis() / 1000L < storage.getTime() + lastDamagerTimeSec) {
                        giveReward(event, p, event.getEntity());
                        return;
                    }
                }
            }
        }
        giveReward(event, event.getEntity().getKiller(), event.getEntity());
    }
}
