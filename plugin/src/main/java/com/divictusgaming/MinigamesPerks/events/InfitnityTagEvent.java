package com.divictusgaming.MinigamesPerks.events;

import com.divictusgaming.MinigamesPerks.Main;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class InfitnityTagEvent implements Listener {

    private final FixedMetadataValue metadata;

    public InfitnityTagEvent(Main pl) {
        metadata = new FixedMetadataValue(pl, 1);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onArrowShoot(EntityShootBowEvent event) {
        if (event.getEntity() instanceof Player && event.getBow() != null) {
            if (event.getBow().getEnchantmentLevel(Enchantment.ARROW_INFINITE) > 0){
                Arrow projectile = (Arrow) event.getProjectile();
                projectile.setMetadata("infinity", metadata);
            }
        }
    }
}
