package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class EnderPearlTeleportEvent extends CustomEvent {
    private static List<String> damaged = new ArrayList<>();

    public EnderPearlTeleportEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onTeleport(PlayerTeleportEvent event) {
        if (event.getCause() != PlayerTeleportEvent.TeleportCause.ENDER_PEARL){
            return;
        }
        ConditionResource conditionRes = new ConditionResource(event.getPlayer());
        if (checkConditions(conditionRes)) return;

        damaged.add(event.getPlayer().getName());

        Bukkit.getScheduler().runTaskAsynchronously(Main.getProvidingPlugin(Main.class), () -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ignored) {
            }finally {
                damaged.remove(event.getPlayer().getName());
            }
        });
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onTeleportDamage(EntityDamageEvent event) {
        if (EntityDamageEvent.DamageCause.FALL == event.getCause() &&
                event.getEntity() instanceof Player){
            if (damaged.remove(event.getEntity().getName())){
                giveReward(event, (LivingEntity) event.getEntity());
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        damaged.remove(event.getPlayer().getName());
    }
}
