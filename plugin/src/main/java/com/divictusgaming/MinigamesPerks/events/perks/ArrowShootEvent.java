package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityShootBowEvent;

import java.util.List;


public class ArrowShootEvent extends CustomEvent {
    private Projectile projectile;
    public ArrowShootEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onArrowShoot(EntityShootBowEvent event) {
        ConditionResource conditionRes = new ConditionResource(event.getProjectile(), event.getEntity());
        if (checkConditions(conditionRes)) return;
        giveReward(event, event.getEntity());
    }
}
