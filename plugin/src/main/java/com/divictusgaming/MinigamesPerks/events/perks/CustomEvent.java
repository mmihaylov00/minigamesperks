package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.dto.Perk;
import com.divictusgaming.MinigamesPerks.exceptions.EmptyPrizeException;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import com.divictusgaming.MinigamesPerks.utils.MessageSender;
import com.divictusgaming.MinigamesPerks.utils.RandomGenerator;
import com.divictusgaming.MinigamesPerks.utils.WorldChecker;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CustomEvent implements Listener {
    private final String perkName;
    private final List<IPrize> prizes;
    private final List<ICondition> conditions;
    private final Double percent;
    private final String message;
    private final Boolean messageEnabled;

    CustomEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        Perk perk = Main.getPlugin(Main.class).getPerks().getPerksObject().get(perkName);
        this.perkName = perkName;
        this.prizes = prizes == null ? new ArrayList<>() : prizes;
        this.conditions = conditions == null ? new ArrayList<>() : conditions;

        percent = perk.getPercentIncrease();
        message = perk.getMessage();
        messageEnabled = perk.getSendMessage();
    }

    boolean checkConditions(ConditionResource resource) {
        if (resource.getDamager() != null) {
            if (WorldChecker.checkWorld(resource.getDamager().getWorld())) return true;
        } else if (resource.getVictim() != null) {
            if (WorldChecker.checkWorld(resource.getVictim().getWorld())) return true;
        } else if (resource.getBlock() != null) {
            if (WorldChecker.checkWorld(resource.getBlock().getWorld())) return true;
        }

        for (ICondition condition : conditions)
            if (!condition.matchesConditions(resource))
                return true;

        return false;
    }

    void giveReward(Event event, LivingEntity... entity) {
        if (entity[0] == null) return;

        HashMap<String, Integer> levels = Main.getPlugin(Main.class)
                .getDb().getLevels((entity[0]).getUniqueId());

        if (levels == null) return;

        Integer level = levels.get(perkName);
        if (level > 0 && RandomGenerator.checkPercent(percent * level)) {
            HashMap<String, String> placeholders = new HashMap<>();
            for (IPrize prize : prizes) {
                try {
                    Map<String, String> ph = entity.length > 1 ?
                            prize.givePrize(entity[0], entity[1]) : prize.givePrize(entity[0]);

                    if (ph != null) placeholders.putAll(ph);
                } catch (EmptyPrizeException e) {
                    try {
                        prize.givePrize(event);
                    } catch (EmptyPrizeException ignored) {
                    }
                }
            }
            if (messageEnabled)
                MessageSender.sendMessage(entity[0], message, placeholders, false);
        }
    }
}
