package com.divictusgaming.MinigamesPerks.events;

import com.divictusgaming.MinigamesPerks.dto.LastDamagerStorage;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.HashMap;

public class LastDamagerEventStorage implements Listener {
    private HashMap<String, LastDamagerStorage> storage;

    public LastDamagerEventStorage() {
        this.storage = new HashMap<>();
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onHit(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player)
        storage.put(event.getEntity().getName(), new LastDamagerStorage(event.getDamager().getName()));
    }

    public LastDamagerStorage getLastDamagerStorage(String player){
        return storage.get(player);
    }

    public LastDamagerStorage getLastDamagerStorage(Player player){
        return storage.get(player.getName());
    }

}
