package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerItemConsumeEvent;

import java.util.List;

public class ConsumeEvent extends CustomEvent {
    public ConsumeEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onHit(PlayerItemConsumeEvent event) {
        ConditionResource conditionRes = new ConditionResource(event.getPlayer(), null, event.getItem());
        if (checkConditions(conditionRes)) return;

        giveReward(event, (LivingEntity) event.getPlayer());
    }
}
