package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.ProjectileHitEvent;

import java.util.List;

public class ProjectileHitBlockEvent extends CustomEvent {
    public ProjectileHitBlockEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onProjectileHit(ProjectileHitEvent event) {
        if (event.getHitEntity() != null){
            return;
        }
        ConditionResource conditionRes = new ConditionResource(event.getEntity(), (Entity) event.getEntity().getShooter());
        if (checkConditions(conditionRes)) return;

        giveReward(event, (LivingEntity) event.getEntity().getShooter());
    }
}
