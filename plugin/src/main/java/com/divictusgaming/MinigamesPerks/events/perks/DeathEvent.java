package com.divictusgaming.MinigamesPerks.events.perks;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.conditions.ICondition;
import com.divictusgaming.MinigamesPerks.prizes.IPrize;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.List;

public class DeathEvent extends CustomEvent {
    public DeathEvent(String perkName, List<IPrize> prizes, List<ICondition> conditions) {
        super(perkName, prizes, conditions);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onDeath(PlayerDeathEvent event) {
        ConditionResource conditionRes = new ConditionResource(event.getEntity().getKiller(), event.getEntity());
        if (checkConditions(conditionRes)) return;
        giveReward(event, event.getEntity(), event.getEntity().getKiller());
    }
}
