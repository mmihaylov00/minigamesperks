package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;

public class HitProjectileCondition implements ICondition {
    private final Class<? extends Projectile> projectile;

    public HitProjectileCondition(Class<? extends Projectile> projectile) {
        this.projectile = projectile;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        if (!(projectile.isInstance(resource.getDamager())))
            return false;

        return ((Projectile) resource.getDamager()).getShooter() instanceof Player;
    }
}
