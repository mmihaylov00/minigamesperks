package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.entity.LivingEntity;

public class IsAboveHpCondition implements ICondition {
    private final Double hp;
    private final int player;

    public IsAboveHpCondition(Double hp, int player) {
        this.hp = hp;
        this.player = player;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        if (player == 0)
            return ((LivingEntity) resource.getDamager()).getHealth() > hp;

        return ((LivingEntity) resource.getVictim()).getHealth() > hp;

    }
}
