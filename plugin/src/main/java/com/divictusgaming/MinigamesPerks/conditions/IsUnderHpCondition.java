package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.entity.LivingEntity;

public class IsUnderHpCondition implements ICondition {
    private final Double hp;
    private final int player;

    public IsUnderHpCondition(Double hp, int player) {
        this.hp = hp;
        this.player = player;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        if (player == 0 && resource.getDamager() instanceof LivingEntity)
            return ((LivingEntity) resource.getDamager()).getHealth() < hp;
        else if (resource.getVictim() instanceof LivingEntity)
            return ((LivingEntity) resource.getVictim()).getHealth() < hp;

        return false;
    }
}
