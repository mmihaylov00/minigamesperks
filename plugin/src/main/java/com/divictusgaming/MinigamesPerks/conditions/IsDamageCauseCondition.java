package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.List;

public class IsDamageCauseCondition implements ICondition {
    private final List<EntityDamageEvent.DamageCause> causes;

    public IsDamageCauseCondition(List<EntityDamageEvent.DamageCause> causes) {
        this.causes = causes;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        if (resource.getEvent() instanceof EntityDamageEvent)
            return checkCauses(((EntityDamageEvent) resource.getEvent()));

        return false;
    }

    private boolean checkCauses(EntityDamageEvent event) {
        if (event == null) return false;

        EntityDamageEvent.DamageCause currentCause = event.getCause();
        for (EntityDamageEvent.DamageCause cause : causes)
            if (cause == currentCause) return true;

        return false;
    }
}
