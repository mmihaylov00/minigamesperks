package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.metadata.MetadataValue;

import java.util.List;

public class BowNotInfinityCondition implements ICondition {

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        List<MetadataValue> infinity = resource.getDamager().getMetadata("infinity");
        if (infinity.isEmpty())
            return true;

        for (MetadataValue metadataValue : infinity)
            if (metadataValue.asInt() == 1)
                return false;

        return true;
    }
}
