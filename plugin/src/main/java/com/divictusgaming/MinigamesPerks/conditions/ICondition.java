package com.divictusgaming.MinigamesPerks.conditions;


import com.divictusgaming.MinigamesPerks.dto.ConditionResource;

public interface ICondition {
    public boolean matchesConditions(ConditionResource resource);
}
