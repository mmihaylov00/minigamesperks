package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.List;

public class HoldsItemCondition implements ICondition {
    private final List<Material> materials;
    private final int hand;
    private final Boolean isAny;

    public HoldsItemCondition(List<Material> materials, int hand, Boolean isAny) {
        this.materials = materials;
        this.hand = hand;
        this.isAny = isAny;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        if (resource.getDamager() instanceof Player)
            return checkItem(((Player) resource.getDamager()));

        return false;
    }

    private boolean checkItem(Player entity) {
        Material currentMaterial = Main.getPlugin(Main.class)
                .getINMSMain().getItemInHand(entity.getInventory(), hand).getType();
        if (isAny && currentMaterial != Material.AIR)
            return true;

        for (Material material : materials)
            if (material == currentMaterial)
                return true;

        return false;
    }
}
