package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;

import java.util.ArrayList;

public class IsInWorldCondition implements ICondition {
    private final ArrayList<String> worlds;

    public IsInWorldCondition(ArrayList<String> worlds) {
        this.worlds = worlds;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        for (String world : worlds) {
            if (resource.getDamager() != null)
                if (resource.getDamager().getWorld().getName().equalsIgnoreCase(world))
                    return true;

            if (resource.getVictim() != null)
                if (resource.getVictim().getWorld().getName().equalsIgnoreCase(world))
                    return true;
        }
        return false;
    }

}
