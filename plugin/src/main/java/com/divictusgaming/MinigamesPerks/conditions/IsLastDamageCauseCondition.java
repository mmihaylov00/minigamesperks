package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.List;

public class IsLastDamageCauseCondition implements ICondition {
    private final List<EntityDamageEvent.DamageCause> causes;
    private final int player;

    public IsLastDamageCauseCondition(int player, List<EntityDamageEvent.DamageCause> causes) {
        this.causes = causes;
        this.player = player;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        try {
            if (player == 0) {
                if (resource.getDamager() instanceof Player)
                    return checkCauses((Player) resource.getDamager());

            } else if (resource.getVictim() instanceof Player)
                return checkCauses((Player) resource.getVictim());

        } catch (ClassCastException ignored) {
        }
        return false;
    }

    private boolean checkCauses(Player entity) {
        if (entity.getLastDamageCause() == null)
            return false;

        EntityDamageEvent.DamageCause currentCause = entity.getLastDamageCause().getCause();
        for (EntityDamageEvent.DamageCause cause : causes)
            if (cause == currentCause) return true;

        return false;
    }
}
