package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class HasArmorCondition implements ICondition {
    private final List<Material> materials;
    private final int player;
    private final int slot;
    private final boolean isAny;

    public HasArmorCondition(int slot, List<Material> materials, int player, boolean isAny) {
        this.materials = materials;
        this.player = player;
        this.slot = slot;
        this.isAny = isAny;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        if (player == 0) {
            if (resource.getDamager() instanceof Player)
                return checkArmor(((Player) resource.getDamager()));

        } else if (resource.getVictim() instanceof Player)
            return checkArmor(((Player) resource.getVictim()));

        return false;
    }

    private boolean checkArmor(Player entity) {
        switch (slot) {
            case 1:
                return checkItem(entity.getInventory().getHelmet());
            case 2:
                return checkItem(entity.getInventory().getChestplate());
            case 3:
                return checkItem(entity.getInventory().getLeggings());
            case 4:
                return checkItem(entity.getInventory().getBoots());
        }

        return false;
    }

    private boolean checkItem(ItemStack item) {
        Material type;
        if (item == null)
            type = Material.AIR;
        else
            type = item.getType();

        if (isAny && type != Material.AIR)
            return true;

        for (Material material : materials)
            if (material == type)
                return true;

        return false;
    }
}
