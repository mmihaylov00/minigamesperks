package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.entity.Player;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IsJsCondition implements ICondition {
    private final String jsCondition;

    public IsJsCondition(String jsCondition) {
        this.jsCondition = jsCondition;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        String command = jsCondition;
        if (resource.getDamager() instanceof Player) {
            final String regex = "(\\{player})(%[A-Za-z_]+%)";
            final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
            final Matcher matcher = pattern.matcher(command);
            while (matcher.find()) {
                String val = PlaceholderAPI.setPlaceholders((Player) resource.getDamager(), matcher.group(2));
                command = command.replace(matcher.group(0), val);
            }
        }
        if (resource.getVictim() instanceof Player) {
            final String regex = "(\\{victim})(%[A-Za-z_]+%)";
            final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
            final Matcher matcher = pattern.matcher(command);
            while (matcher.find()) {
                String val = PlaceholderAPI.setPlaceholders((Player) resource.getVictim(), matcher.group(2));
                command = command.replace(matcher.group(0), val);
            }
        }
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        try {
            engine.eval("function run(){ return " + command + ";}");
            return Boolean.parseBoolean(String.valueOf(((Invocable) engine).invokeFunction("run")));
        } catch (ScriptException | NoSuchMethodException ignored) {
        }
        return false;
    }
}
