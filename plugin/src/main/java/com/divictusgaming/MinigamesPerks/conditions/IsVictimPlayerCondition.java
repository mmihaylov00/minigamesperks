package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.entity.Player;

public class IsVictimPlayerCondition implements ICondition {
    @Override
    public boolean matchesConditions(ConditionResource resource) {
        return resource.getVictim() instanceof Player;
    }
}
