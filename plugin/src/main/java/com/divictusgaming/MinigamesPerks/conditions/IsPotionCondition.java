package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.Material;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class IsPotionCondition implements ICondition {
    private final List<PotionEffectType> potions;
    private final boolean isAny;

    public IsPotionCondition(List<PotionEffectType> potions, boolean isAny) {
        this.potions = potions;
        this.isAny = isAny;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {

        if (!(resource.getItem().getItemMeta() instanceof PotionMeta))
            return false;

        PotionMeta itemMeta = (PotionMeta) resource.getItem().getItemMeta();

        if (isAny && resource.getItem().getType() == Material.POTION)
            return true;

        for (PotionEffectType potion : potions)
            for (PotionEffect effect : itemMeta.getCustomEffects())
                if (effect.getType() == potion) return true;

        return false;
    }

}
