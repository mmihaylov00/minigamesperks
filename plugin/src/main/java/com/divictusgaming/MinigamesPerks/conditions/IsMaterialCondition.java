package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.Material;

import java.util.List;

public class IsMaterialCondition implements ICondition {
    private final List<Material> blocks;

    public IsMaterialCondition(List<Material> blocks) {
        this.blocks = blocks;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        Material m = resource.getBlock().getType();

        for (Material block : blocks)
            if (block == m) return true;

        return false;
    }
}
