package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.entity.Player;

public class IsDamagerPlayerCondition implements ICondition {
    @Override
    public boolean matchesConditions(ConditionResource resource) {
        return resource.getDamager() instanceof Player;
    }
}
