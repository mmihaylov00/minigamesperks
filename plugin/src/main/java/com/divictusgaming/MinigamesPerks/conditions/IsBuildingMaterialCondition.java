package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import com.divictusgaming.MinigamesPerks.utils.BlockChecker;

public class IsBuildingMaterialCondition implements ICondition {
    @Override
    public boolean matchesConditions(ConditionResource resource) {
        return BlockChecker.isBuildingMaterial(resource.getBlock().getType());
    }
}
