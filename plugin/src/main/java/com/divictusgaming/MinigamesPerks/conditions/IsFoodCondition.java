package com.divictusgaming.MinigamesPerks.conditions;

import com.divictusgaming.MinigamesPerks.dto.ConditionResource;
import org.bukkit.Material;

import java.util.List;

public class IsFoodCondition implements ICondition {
    private final List<Material> food;
    private final boolean isAny;

    public IsFoodCondition(List<Material> food, boolean isAny) {
        this.food = food;
        this.isAny = isAny;
    }

    @Override
    public boolean matchesConditions(ConditionResource resource) {
        if (!resource.getItem().getType().isEdible())
            return false;

        if (isAny && resource.getItem().getType() != Material.POTION)
            return true;

        for (Material item : food)
            if (item == resource.getItem().getType())
                return true;

        return false;
    }

}
