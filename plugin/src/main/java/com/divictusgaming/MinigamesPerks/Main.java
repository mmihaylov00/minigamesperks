package com.divictusgaming.MinigamesPerks;

import com.divictusgaming.MinigamesPerks.commands.CommandTabCompleter;
import com.divictusgaming.MinigamesPerks.commands.MainCommand;
import com.divictusgaming.MinigamesPerks.database.FileDatabase;
import com.divictusgaming.MinigamesPerks.database.IDatabase;
import com.divictusgaming.MinigamesPerks.database.SQLDatabase;
import com.divictusgaming.MinigamesPerks.dto.Config;
import com.divictusgaming.MinigamesPerks.enums.DefaultConfig;
import com.divictusgaming.MinigamesPerks.events.InfitnityTagEvent;
import com.divictusgaming.MinigamesPerks.events.InventoryEvents;
import com.divictusgaming.MinigamesPerks.events.LastDamagerEventStorage;
import com.divictusgaming.MinigamesPerks.events.PlayerEvents;
import com.divictusgaming.MinigamesPerks.events.perks.CustomEvent;
import com.divictusgaming.MinigamesPerks.exceptions.InvalidValueException;
import com.divictusgaming.MinigamesPerks.factories.ConfigFactory;
import com.divictusgaming.MinigamesPerks.factories.MessagesFactory;
import com.divictusgaming.MinigamesPerks.factories.PerksFactory;
import com.divictusgaming.MinigamesPerks.papi.MinigamesPlaceholders;
import com.divictusgaming.MinigamesPerks.storages.PerksStorage;
import com.divictusgaming.MinigamesPerks.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main extends JavaPlugin {
    private MessagesFactory messages;
    private ConfigFactory config;
    private PerksStorage perks;
    private IDatabase database;
    private String perksList;
    private EventRegister eventRegister;
    private com.divictusgaming.MinigamesPerks.nms.INMSMain INMSMain;
    private List<Material> buildingBlocks;
    private final ChatColor mainColor = ChatColor.DARK_AQUA;
    private File dataFolder;
    private LastDamagerEventStorage storage;
    private HashMap<String, CustomEvent> commandEvents;
    private Metrics metrics;


    @Override
    public void onEnable() {
        dataFolder = getDataFolder().getAbsoluteFile();
        String nonce = "%%__NONCE__%%";
        String userId = "%%__USER__%%4";
        metrics = new Metrics(this, 6436);
        metrics.addCustomChart(new Metrics.SimplePie("user", () -> userId + " " + nonce));
        metrics.addCustomChart(new Metrics.SimplePie("ver", () -> VersionChecker.version));
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") == null) {
            throw new RuntimeException("Could not find PlaceholderAPI!");
        } else {
            new MinigamesPlaceholders().register();
        }
        eventRegister = new EventRegister(this);

        if (reload()) return;

        buildingBlocks = INMSMain.getBuildingBlocks();

        if (database instanceof SQLDatabase && !((SQLDatabase) database).getWorking()) {
            printError("Could not connect to the database!");
            return;
        }

        Bukkit.getConsoleSender().sendMessage(mainColor + "------------------------------------------");
        Bukkit.getConsoleSender().sendMessage(mainColor + "Minigames Perk (v" + VersionChecker.version + ")");
        try {
            if (VersionChecker.isLatestVersion()) {
                Bukkit.getConsoleSender().sendMessage(mainColor + "You have the latest version!");
            } else {
                Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "There is a newer version available!");
            }
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Could not check version, please connect to internet!");
        }
        Bukkit.getConsoleSender().sendMessage(mainColor + "Plugin purchased by: %%__USER__%%");
        Bukkit.getConsoleSender().sendMessage(mainColor + "Spigot page: https://www.spigotmc.org/resources/%%__RESOURCE__%%/");
        Bukkit.getConsoleSender().sendMessage(mainColor + "");
        Bukkit.getConsoleSender().sendMessage(mainColor + "Loaded " + perks.getPerksObject().keySet().size() + " perks");
        Bukkit.getConsoleSender().sendMessage(mainColor + "Loaded " + messages.getMessages().size() + " messages");
        Bukkit.getConsoleSender().sendMessage(mainColor + "Loaded " + config.getFileConfiguration().size() + " config options");
        Bukkit.getConsoleSender().sendMessage(mainColor + "Created by ZeaL_BGN");
        Bukkit.getConsoleSender().sendMessage(mainColor + "Spigot profile: https://www.spigotmc.org/members/51054/");
        Bukkit.getConsoleSender().sendMessage(mainColor + "------------------------------------------");
        this.getCommand("minigamesperks").setExecutor(new MainCommand());
        this.getCommand("minigamesperks").setTabCompleter(new CommandTabCompleter());
        Bukkit.getServer().getPluginManager().registerEvents(new InventoryEvents(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerEvents(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new InfitnityTagEvent(this), this);
    }

    private void printError(String error) {
        Bukkit.getConsoleSender().sendMessage(mainColor + "------------------------------------------");
        Bukkit.getConsoleSender().sendMessage(mainColor + "Minigames Perk Version " + VersionChecker.version);
        Bukkit.getConsoleSender().sendMessage(ChatColor.RED + error);
        Bukkit.getConsoleSender().sendMessage(mainColor + "Created by ZeaL_BGN");
        Bukkit.getConsoleSender().sendMessage(mainColor + "------------------------------------------");
        Bukkit.getPluginManager().disablePlugin(getProvidingPlugin(Main.class));
    }

    private boolean setupVersion() {
        String version;
        try {
            version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        } catch (ArrayIndexOutOfBoundsException whatVersionAreYouUsingException) {
            return false;
        }
        switch (version) {
            case "v1_8_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_8_R1.NMSMain();
                break;
            case "v1_8_R2":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_8_R2.NMSMain();
                break;
            case "v1_8_R3":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_8_R3.NMSMain();
                break;
            case "v1_9_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_9_R1.NMSMain();
                break;
            case "v1_9_R2":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_9_R2.NMSMain();
                break;
            case "v1_10_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_10_R1.NMSMain();
                break;
            case "v1_11_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_11_R1.NMSMain();
                break;
            case "v1_12_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_12_R1.NMSMain();
                break;
            case "v1_13_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_13_R1.NMSMain();
                break;
            case "v1_13_R2":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_13_R2.NMSMain();
                break;
            case "v1_14_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_14_R1.NMSMain();
                break;
            case "v1_15_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_15_R1.NMSMain();
                break;
            case "v1_16_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_16_R1.NMSMain();
                break;
            case "v1_16_R2":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_16_R2.NMSMain();
                break;
            case "v1_16_R3":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_16_R3.NMSMain();
                break;
            case "v1_17_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_17_R1.NMSMain();
                break;
            case "v1_18_R1":
                INMSMain = new com.divictusgaming.MinigamesPerks.nms.v1_18_R1.NMSMain();
                break;
        }
        return INMSMain != null;
    }

    @Override
    public void onDisable() {
//        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
//            MaxHealthChanger.resetMaxHealth(onlinePlayer);
//        }
        if (database != null) {
            database.saveAllPlayers();
            if (database instanceof SQLDatabase) {
                ((SQLDatabase) database).closeConnection();
            }
        }
        getLogger().info("MinigamesPerks Disabled");
    }

    public boolean reload() {
        if (!setupVersion()) {
            printError("Your server version is not compatible with this plugin!");
            return true;
        }

        commandEvents = new HashMap<>();
        config = new ConfigFactory();

        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            if (onlinePlayer.getOpenInventory().getTitle().equals(config.getConfig().getInvTitle())) {
                onlinePlayer.getOpenInventory().close();
            }
        }
        perksList = null;
        messages = new MessagesFactory();
        perks = new PerksFactory().getPerks();

        MessageSender.reloadPrefix();
        String dbType;
        try {
            dbType = ObjectExtractor.extractObject(config.getFileConfiguration().get(DefaultConfig.DATABASE_TYPE.getPath()), String.class);
        } catch (InvalidValueException e) {
            Bukkit.getLogger().warning(ChatColor.RED + "Your databasetype config option is invalid! Defaulting to file");
            dbType = "file";
        }
        if (database == null) {
            if (dbType.equalsIgnoreCase("file")) {
                database = new FileDatabase();
            } else {
                eventRegister.enableListeners();
                return !connectToDb();
            }
        } else {
            database.saveAllPlayers();
            if (dbType.equalsIgnoreCase("file")) {
                if (database instanceof SQLDatabase) {
                    ((SQLDatabase) database).closeConnection();
                    database = new FileDatabase();
                    database.loadAllPlayers();
                }
            } else {
                if (database instanceof SQLDatabase) {
                    ((SQLDatabase) database).closeConnection();
                }
                eventRegister.enableListeners();
                return !connectToDb();
            }
        }
        eventRegister.enableListeners();
        return false;
    }

    private boolean connectToDb() {
        try {
            database = new SQLDatabase(ObjectExtractor.extractObject(config.getFileConfiguration().get(DefaultConfig.MYSQL_URL.getPath()), String.class),
                    ObjectExtractor.extractObject(config.getFileConfiguration().get(DefaultConfig.MYSQL_USERNAME.getPath()), String.class),
                    ObjectExtractor.extractObject(config.getFileConfiguration().get(DefaultConfig.MYSQL_PASSWORD.getPath()), String.class));
            database.loadAllPlayers();
        } catch (InvalidValueException e) {
            Bukkit.getLogger().warning(ChatColor.RED + "Your database setup is invalid! Disabling the plugin!");
            Bukkit.getPluginManager().disablePlugin(getProvidingPlugin(Main.class));
            return false;
        }
        return true;
    }


    public String getPerksList() {
        return perksList;
    }

    public void setPerksList(String perksList) {
        this.perksList = perksList;
    }

    public Map<String, Object> getMessages() {
        return messages.getMessages();
    }

    public Map<String, Object> getConfiguration() {
        return config.getFileConfiguration();
    }

    public Config getConfigObject() {
        return config.getConfig();
    }

    public IDatabase getDb() {
        return database;
    }

    public com.divictusgaming.MinigamesPerks.nms.INMSMain getINMSMain() {
        return INMSMain;
    }

    public List<Material> getBuildingBlocks() {
        return buildingBlocks;
    }

    public PerksStorage getPerks() {
        return perks;
    }

    public File getPluginsFolder() {
        return dataFolder;
    }

    public void setLastDamagerEventStorage(LastDamagerEventStorage st) {
        storage = st;
    }

    public LastDamagerEventStorage getStorage() {
        return storage;
    }

    public HashMap<String, CustomEvent> getCommandEvents() {
        return commandEvents;
    }

    public Metrics getMetrics() {
        return metrics;
    }
}
