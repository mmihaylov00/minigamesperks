package com.divictusgaming.MinigamesPerks.exceptions;

public class PerksException extends Exception {

    public PerksException(String message) {
        super(message);
    }
}
