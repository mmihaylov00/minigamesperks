package com.divictusgaming.MinigamesPerks.utils;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.conditions.*;
import com.divictusgaming.MinigamesPerks.enums.Path;
import com.divictusgaming.MinigamesPerks.events.LastDamagerEventStorage;
import com.divictusgaming.MinigamesPerks.events.perks.*;
import com.divictusgaming.MinigamesPerks.exceptions.InvalidValueException;
import com.divictusgaming.MinigamesPerks.prizes.*;
import com.divictusgaming.MinigamesPerks.storages.PerksStorage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.MemorySection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Snowball;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EventRegister {
    private Main main;
    private List<Listener> events;

    public EventRegister(Main main) {
        this.main = main;
        events = new ArrayList<>();
    }

    public void enableListeners() {
        for (Listener event : events) {
            HandlerList.unregisterAll(event);
        }
        events.clear();


        LastDamagerEventStorage storage = new LastDamagerEventStorage();
        Bukkit.getServer().getPluginManager().registerEvents(storage, main);
        events.add(storage);
        Main plugin = Main.getPlugin(Main.class);
        plugin.setLastDamagerEventStorage(storage);

        PerksStorage perks = plugin.getPerks();
        Map<String, Object> settings = perks.getPerksSettings();

        for (String perk : perks.getPerksObject().keySet()) {
            Boolean isEnabled;
            try {
                isEnabled = ObjectExtractor.extractObject(settings.get(Path.getFullPath(perk, Path.ENABLED)), Boolean.class);
            } catch (InvalidValueException e) {
                isEnabled = false;
            }
            if (isEnabled) {
                ArrayList<ICondition> conditions = new ArrayList<>();
                CustomEvent event;
                boolean isFireArrow = false;
                boolean isTripleArrow = false;
                boolean isTNTSpawn = false;
                boolean isConsumable = false;
                boolean isDamageChange = false;
                boolean isBuilding = false;
                boolean isDamageCause = false;
                List<IPrize> prizes = new ArrayList<>();
                MemorySection prizesSec = (MemorySection) settings.get(Path.getFullPath(perk, Path.PRIZES));
                if (prizesSec == null) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has no prizes! Disabling!");
                    break;
                }
                Set<String> prizesList = prizesSec.getKeys(false);
                if (prizesList == null || prizesList.isEmpty()) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has no prizes! Disabling!");
                    break;
                }
                MemorySection sec = (MemorySection) settings.get(Path.getFullPath(perk, Path.CONDITIONS));
                if (sec != null) {
                    Set<String> conditionsList = sec.getKeys(false);
                    if (conditionsList != null) {
                        for (String condition : conditionsList) {
                            switch (condition) {
                                case "has-armor-helmet-items":
                                case "has-armor-chestplate-items":
                                case "has-armor-leggings-items":
                                case "has-armor-boots-items":
                                case "is-last-damage-cause-list":
                                case "has-under-hp-value":
                                case "has-above-hp-value":
                                    break;
                                case "is-under-hp": {
                                    Double hp;
                                    try {
                                        hp = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_UNDER_HP_VALUE)), Double.class);
                                    } catch (InvalidValueException e) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has is-under-hp, but has invalid is-under-hp-value! Ignoring condition!");
                                        break;
                                    }
                                    if (hp < 1) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has is-under-hp-value thats less than 1! Ignoring condition!");
                                        break;
                                    }
                                    String player;
                                    try {
                                        player = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_UNDER_HP)), String.class);
                                    } catch (InvalidValueException e) {
                                        player = "player";
                                    }
                                    conditions.add(new IsUnderHpCondition(hp, player.equalsIgnoreCase("player") ? 0 : 1));
                                    break;
                                }
                                case "javascript-condition": {
                                    String cond;
                                    try {
                                        cond = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.JAVASCRIPT_CONDITION)), String.class);
                                    } catch (InvalidValueException e) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid javascript-condition! Ignoring condition!");
                                        break;
                                    }
                                    conditions.add(new IsJsCondition(cond));
                                    break;
                                }
                                case "is-above-hp": {
                                    Double hp;
                                    try {
                                        hp = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_ABOVE_HP_VALUE)), Double.class);
                                    } catch (InvalidValueException e) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has is-under-hp, but has invalid is-under-hp-value! Ignoring condition!");
                                        break;
                                    }
                                    if (hp < 1) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has is-under-hp-value that's less than 1! Defaulting to 1!");
                                        hp = 1d;
                                    }
                                    String player;
                                    try {
                                        player = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_ABOVE_HP)), String.class);
                                    } catch (InvalidValueException e) {
                                        player = "player";
                                    }
                                    conditions.add(new IsAboveHpCondition(hp, player.equalsIgnoreCase("player") ? 0 : 1));
                                    break;
                                }
                                case "holds-item-main": {
                                    ArrayList<String> items;
                                    try {
                                        items = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.HOLDS_ITEM_MAIN)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        items = new ArrayList<>();
                                    }
                                    Boolean isAny = false;
                                    ArrayList<Material> materials = new ArrayList<>();
                                    for (String item : items) {
                                        if (item.equalsIgnoreCase("non-empty")) {
                                            isAny = true;
                                            break;
                                        }
                                        try {
                                            materials.add(Material.valueOf(item));
                                        } catch (IllegalArgumentException ignored) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid item in holds-item-main condition: " + item + "! Ignoring!");
                                        }
                                    }
                                    if (materials.isEmpty()) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty list in holds-item-main condition! Ignoring condition!");
                                        continue;
                                    }
                                    conditions.add(new HoldsItemCondition(materials, 0, isAny));
                                    break;
                                }
                                case "holds-item-offhand": {
                                    ArrayList<String> items;
                                    try {
                                        items = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.HOLDS_ITEM_OFFHAND)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        items = new ArrayList<>();
                                    }
                                    Boolean isAny = false;
                                    ArrayList<Material> materials = new ArrayList<>();
                                    for (String item : items) {
                                        if (item.equalsIgnoreCase("non-empty")) {
                                            isAny = true;
                                            break;
                                        }
                                        try {
                                            materials.add(Material.valueOf(item));
                                        } catch (IllegalArgumentException ignored) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid item in holds-item-offhand condition: " + item + "! Ignoring!");
                                        }
                                    }
                                    if (materials.isEmpty()) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty list in holds-item-offhand condition! Ignoring condition!");
                                        continue;
                                    }
                                    conditions.add(new HoldsItemCondition(materials, 1, isAny));
                                    break;
                                }
                                case "has-armor-helmet": {
                                    ArrayList<String> items = null;
                                    try {
                                        items = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.HAS_ARMOR_HELMET_ITEMS)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        items = new ArrayList<>();
                                    }
                                    String player;
                                    try {
                                        player = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.HAS_ARMOR_HELMET)), String.class);
                                    } catch (InvalidValueException e) {
                                        player = "player";
                                    }
                                    boolean isAny = false;
                                    ArrayList<Material> materials = new ArrayList<>();
                                    for (String item : items) {
                                        if (item.equalsIgnoreCase("non-empty")) {
                                            isAny = true;
                                            break;
                                        }
                                        try {
                                            materials.add(Material.valueOf(item));
                                        } catch (IllegalArgumentException ignored) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid item in has-armor-helmet condition: " + item + "! Ignoring!");
                                        }
                                    }
                                    if (materials.isEmpty() && !isAny) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty has-armor-helmet-items list in has-armor condition! Ignoring condition!");
                                        continue;
                                    }
                                    conditions.add(new HasArmorCondition(1, materials, player.equalsIgnoreCase("player") ? 0 : 1, isAny));
                                    break;
                                }
                                case "has-armor-chestplate": {
                                    ArrayList<String> items = null;
                                    try {
                                        items = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.HAS_ARMOR_CHESTPLATE_ITEMS)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        items = new ArrayList<>();
                                    }
                                    String player;
                                    try {
                                        player = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.HAS_ARMOR_CHESTPLATE)), String.class);
                                    } catch (InvalidValueException e) {
                                        player = "player";
                                    }
                                    boolean isAny = false;
                                    ArrayList<Material> materials = new ArrayList<>();
                                    for (String item : items) {
                                        if (item.equalsIgnoreCase("non-empty")) {
                                            isAny = true;
                                            break;
                                        }
                                        try {
                                            materials.add(Material.valueOf(item));
                                        } catch (IllegalArgumentException ignored) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid item in has-armor-chestplate condition: " + item + "! Ignoring!");
                                        }
                                    }
                                    if (materials.isEmpty() && !isAny) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty has-armor-chestplate-items list in has-armor condition! Ignoring condition!");
                                        continue;
                                    }
                                    conditions.add(new HasArmorCondition(2, materials, player.equalsIgnoreCase("player") ? 0 : 1, isAny));
                                    break;
                                }
                                case "has-armor-leggings": {
                                    ArrayList<String> items = null;
                                    try {
                                        items = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.HAS_ARMOR_LEGGINGS_ITEMS)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        items = new ArrayList<>();
                                    }
                                    String player;
                                    try {
                                        player = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.HAS_ARMOR_LEGGINGS)), String.class);
                                    } catch (InvalidValueException e) {
                                        player = "player";
                                    }
                                    boolean isAny = false;
                                    ArrayList<Material> materials = new ArrayList<>();
                                    for (String item : items) {
                                        if (item.equalsIgnoreCase("non-empty")) {
                                            isAny = true;
                                            break;
                                        }
                                        try {
                                            materials.add(Material.valueOf(item));
                                        } catch (IllegalArgumentException ignored) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid item in has-armor-leggings condition: " + item + "! Ignoring!");
                                        }
                                    }
                                    if (materials.isEmpty() && !isAny) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty has-armor-leggings-items list in has-armor condition! Ignoring condition!");
                                        continue;
                                    }
                                    conditions.add(new HasArmorCondition(3, materials, player.equalsIgnoreCase("player") ? 0 : 1, isAny));
                                    break;
                                }
                                case "has-armor-boots": {
                                    ArrayList<String> items = null;
                                    try {
                                        items = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.HAS_ARMOR_BOOTS_ITEMS)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        items = new ArrayList<>();
                                    }
                                    String player;
                                    try {
                                        player = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.HAS_ARMOR_BOOTS)), String.class);
                                    } catch (InvalidValueException e) {
                                        player = "player";
                                    }
                                    boolean isAny = false;
                                    ArrayList<Material> materials = new ArrayList<>();
                                    for (String item : items) {
                                        if (item.equalsIgnoreCase("non-empty")) {
                                            isAny = true;
                                            break;
                                        }
                                        try {
                                            materials.add(Material.valueOf(item));
                                        } catch (IllegalArgumentException ignored) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid item in has-armor-boots condition: " + item + "! Ignoring!");
                                        }
                                    }
                                    if (materials.isEmpty() && !isAny) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty has-armor-boots-items list in has-armor condition! Ignoring condition!");
                                        continue;
                                    }
                                    conditions.add(new HasArmorCondition(4, materials, player.equalsIgnoreCase("player") ? 0 : 1, isAny));
                                    break;
                                }
                                case "is-damage-cause": {
                                    isDamageCause = true;
                                    ArrayList<String> causes = null;
                                    try {
                                        causes = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_DAMAGE_CAUSE)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        causes = new ArrayList<>();
                                    }
                                    ArrayList<EntityDamageEvent.DamageCause> finalCauses = new ArrayList<>();
                                    for (String cause : causes) {
                                        try {
                                            finalCauses.add(EntityDamageEvent.DamageCause.valueOf(cause));
                                        } catch (IllegalArgumentException e) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid cause in is-damage-cause condition: " + cause + "! Ignoring!");
                                        }
                                    }
                                    if (finalCauses.isEmpty()) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has no valid causes in is-damage-cause! Ignoring condition!");
                                        break;
                                    }

                                    conditions.add(new IsDamageCauseCondition(finalCauses));
                                    break;
                                }
                                case "is-last-damage-cause": {
                                    String player;
                                    try {
                                        player = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_LAST_DAMAGE_CAUSE)), String.class);
                                    } catch (InvalidValueException e) {
                                        player = "player";
                                    }
                                    ArrayList<String> causes;
                                    try {
                                        causes = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_LAST_DAMAGE_CAUSE_LIST)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        causes = new ArrayList<>();
                                    }
                                    ArrayList<EntityDamageEvent.DamageCause> finalCauses = new ArrayList<>();
                                    for (String cause : causes) {
                                        try {
                                            finalCauses.add(EntityDamageEvent.DamageCause.valueOf(cause));
                                        } catch (IllegalArgumentException e) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid cause in is-last-damage-cause condition: " + cause + "! Ignoring!");
                                        }
                                    }
                                    if (finalCauses.isEmpty()) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has no valid causes in is-last-damage-cause! Ignoring condition!");
                                        break;
                                    }

                                    conditions.add(new IsLastDamageCauseCondition(player.equalsIgnoreCase("player") ? 0 : 1, finalCauses));
                                    break;
                                }
                                case "is-potion": {
                                    isConsumable = true;
                                    ArrayList<String> items;
                                    try {
                                        items = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_POTION)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        items = new ArrayList<>();
                                    }
                                    boolean isAny = false;
                                    ArrayList<PotionEffectType> materials = new ArrayList<>();
                                    for (String item : items) {
                                        if (item.equalsIgnoreCase("any")) {
                                            isAny = true;
                                            break;
                                        }
                                        try {
                                            materials.add(PotionEffectType.getByName(item));
                                        } catch (IllegalArgumentException ignored) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid potion in is-potion condition: " + item + "! Ignoring!");
                                        }
                                    }
                                    if (materials.isEmpty() && !isAny) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty is-potion condition! Ignoring condition!");
                                        continue;
                                    }
                                    conditions.add(new IsPotionCondition(materials, isAny));
                                    break;
                                }
                                case "is-food": {
                                    isConsumable = true;
                                    ArrayList<String> items;
                                    try {
                                        items = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_FOOD)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        items = new ArrayList<>();
                                    }
                                    boolean isAny = false;
                                    ArrayList<Material> materials = new ArrayList<>();
                                    for (String item : items) {
                                        if (item.equalsIgnoreCase("any")) {
                                            isAny = true;
                                            break;
                                        }
                                        try {
                                            Material mat = Material.valueOf(item);
                                            if (!mat.isEdible()) {
                                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has non food item in is-food condition: " + item + "! Ignoring!");
                                                continue;
                                            }
                                            materials.add(mat);
                                        } catch (IllegalArgumentException ignored) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid item in is-food condition: " + item + "! Ignoring!");
                                        }
                                    }
                                    if (materials.isEmpty() && !isAny) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty is-food condition! Ignoring condition!");
                                        continue;
                                    }
                                    conditions.add(new IsFoodCondition(materials, isAny));
                                    break;
                                }
                                case "is-building-material": {
                                    isBuilding = true;
                                    Boolean isBuildingMaterial;
                                    try {
                                        isBuildingMaterial = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_BUILDING_MATERIAL)), Boolean.class);
                                    } catch (InvalidValueException e) {
                                        isBuildingMaterial = false;
                                    }
                                    if (!isBuildingMaterial) {
                                        continue;
                                    }
                                    conditions.add(new IsBuildingMaterialCondition());
                                    break;
                                }
                                case "is-in-world": {
                                    ArrayList<String> worlds;
                                    try {
                                        worlds = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_IN_WORLD)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid is-in-world condition! Ignoring condition!");
                                        continue;
                                    }
                                    conditions.add(new IsInWorldCondition(worlds));
                                    break;
                                }
                                case "bow-not-infinity": {
                                    Boolean inf;
                                    try {
                                        inf = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.BOW_NOT_INFINITY)), Boolean.class);
                                    } catch (InvalidValueException e) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid bow-not-infinity condition! Ignoring condition!");
                                        continue;
                                    }
                                    if (!inf) {
                                        continue;
                                    }
                                    conditions.add(new BowNotInfinityCondition());
                                    break;
                                }
                                case "is-material": {
                                    ArrayList<String> items;
                                    try {
                                        items = ObjectExtractor.extractObject(settings.get(Path.getConditionPath(perk, Path.IS_MATERIAL)), ArrayList.class);
                                    } catch (InvalidValueException e) {
                                        items = new ArrayList<>();
                                    }
                                    ArrayList<Material> materials = new ArrayList<>();
                                    for (String item : items) {
                                        try {
                                            materials.add(Material.valueOf(item));
                                        } catch (IllegalArgumentException ignored) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid material in is-material condition: " + item + "! Ignoring!");
                                        }
                                    }
                                    if (materials.isEmpty()) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty list in is-material condition! Ignoring condition!");
                                        continue;
                                    }
                                    conditions.add(new IsMaterialCondition(materials));
                                    break;
                                }
                            }
                        }
                    }
                }
                for (String prize : prizesList) {
                    switch (prize.toLowerCase()) {
                        case "items-give-type":
                        case "effects-give-type":
                        case "effects-list":
                        case "enchants-items":
                        case "enchants-give-type":
                        case "spawn-mob-types": {
                            continue;
                        }
                        case "items": {
                            ArrayList<ItemStack> finalItems = new ArrayList<>();
                            ArrayList<String> items;
                            try {
                                items = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.ITEMS)), ArrayList.class);
                            } catch (InvalidValueException e) {
                                items = new ArrayList<>();
                            }
                            if (items.isEmpty()) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty items! Ignoring!");
                                continue;
                            }
                            boolean hasHandBlock = false;
                            boolean hasBrokenBlock = false;
                            if (items.isEmpty()) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has item prize, but no items included");
                                continue;
                            }
                            for (String i : items) {
                                String[] split = i.split(":");
                                try {
                                    Material material = Material.valueOf(split[0]);
                                    int size = 1;
                                    byte meta = 0;
                                    if (split.length > 2) {
                                        size = Integer.parseInt(split[1]);
                                        meta = Byte.parseByte(split[2]);
                                    } else if (split.length == 2) {
                                        size = Integer.parseInt(split[1]);
                                    }
                                    finalItems.add(new ItemStack(material, size, meta));
                                } catch (IllegalArgumentException e) {
                                    if (i.equalsIgnoreCase("hand-item")) {
                                        hasHandBlock = true;
                                    } else if (i.equalsIgnoreCase("broken-block")) {
                                        hasBrokenBlock = true;
                                    } else {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid item prize: " + split[0] + "! Ignoring!");
                                    }
                                }
                            }
                            int itemsGiveNum = 0;
                            try {
                                itemsGiveNum = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.ITEMS_GIVE_TYPE)), Integer.class);
                            } catch (InvalidValueException e) {

                            }
                            if (finalItems.isEmpty() && !hasBrokenBlock && !hasHandBlock) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty effects-list! Ignoring!");
                                continue;
                            }
                            prizes.add(new ItemGivePrize(finalItems, hasHandBlock, hasBrokenBlock, itemsGiveNum));
                            continue;
                        }
                        case "fire-arrow": {
                            Boolean isActivated;
                            try {
                                isActivated = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.FIRE_ARROW)), Boolean.class);
                            } catch (InvalidValueException e) {
                                isActivated = false;
                            }
                            if (isActivated) {
                                prizes.add(new FireArrowPrize());
                                isFireArrow = true;
                            }
                            continue;
                        }
                        case "feed": {
                            Integer bonus;
                            try {
                                bonus = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.FEED)), Integer.class);
                            } catch (InvalidValueException e) {
                                bonus = 1;
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid value for feed, please input a number! Defaulting to 1!");
                            }
                            prizes.add(new FeedPrize(bonus));
                            continue;
                        }
                        case "heal": {
                            Integer bonus;
                            try {
                                bonus = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.HEAL)), Integer.class);
                            } catch (InvalidValueException e) {
                                bonus = 1;
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid value for heal, please input a number! Defaulting to 1!");
                            }
                            prizes.add(new HealPrize(bonus));
                            continue;
                        }
                        case "increase-damage": {
                            isDamageChange = true;
                            Double bonus;
                            try {
                                bonus = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.INCREASE_DAMAGE)), Double.class);
                            } catch (InvalidValueException e) {
                                bonus = 1d;
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid value for increase-damage, please input a number! Defaulting to 1!");
                            }
                            prizes.add(new IncreaseDamagePrize(bonus));
                            continue;
                        }
                        case "reduce-damage": {
                            isDamageChange = true;
                            Double bonus;
                            try {
                                bonus = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.REDUCE_DAMAGE)), Double.class);
                            } catch (InvalidValueException e) {
                                bonus = 1d;
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid value for reduce-damage, please input a number! Defaulting to 1!");
                            }
                            prizes.add(new ReduceDamagePrize(bonus));
                            continue;
                        }
                        case "spawn-tnt": {
                            Integer spawnAtNum;
                            try {
                                spawnAtNum = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.SPAWN_TNT)), String.class)
                                        .equalsIgnoreCase("player") ? 0 : 1;
                            } catch (InvalidValueException e) {
                                spawnAtNum = 0;
                            }
                            prizes.add(new SpawnTNTPrize(spawnAtNum));
                            isTNTSpawn = true;
                            continue;
                        }
                        case "triple-arrow": {
                            Boolean isActivated;
                            try {
                                isActivated = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.TRIPLE_ARROW)), Boolean.class);
                            } catch (InvalidValueException e) {
                                isActivated = false;
                            }
                            if (isActivated) {
                                prizes.add(new TripleArrowPrize());
                                isTripleArrow = true;
                            }
                            continue;
                        }
                        case "exp-level": {
                            Integer bonus;
                            try {
                                bonus = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.EXP_LEVEL)), Integer.class);
                            } catch (InvalidValueException e) {
                                bonus = 1;
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid value for exp-level, please input a number! Defaulting to 1!");
                            }
                            prizes.add(new ExpLevelPrize(bonus));
                            continue;
                        }
                        case "enchants": {
                            ArrayList<String> enchants;
                            try {
                                enchants = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.ENCHANTS)), ArrayList.class);
                            } catch (InvalidValueException e) {
                                enchants = new ArrayList<>();
                            }
                            if (enchants.isEmpty()) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty enchants list! Ignoring!");
                                continue;
                            }

                            List<Enchantment> enchantments = new ArrayList<>();
                            for (String enchant : enchants) {
                                Enchantment enchantment = plugin.getINMSMain().stringToEnchant(enchant);
                                if (enchantment == null) {
                                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid enchant: " + enchant + "! Ignoring!");
                                    continue;
                                }
                                enchantments.add(enchantment);
                            }

                            if (enchantments.isEmpty()) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty enchant list! Ignoring!");
                                continue;
                            }

                            ArrayList<String> items;
                            try {
                                items = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.ENCHANTS_ITEMS)), ArrayList.class);
                            } catch (InvalidValueException e) {
                                items = new ArrayList<>();
                            }
                            if (items.isEmpty()) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty enchants items list! Ignoring!");
                                continue;
                            }

                            List<Material> materials = new ArrayList<>();

                            boolean isHand = false;
                            for (String item : items) {
                                if (item.equalsIgnoreCase("hand")) {
                                    isHand = true;
                                    break;
                                }
                                try {
                                    materials.add(Material.valueOf(item));
                                } catch (IllegalArgumentException e) {
                                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid item in the enchant items: " + item + "! Ignoring!");
                                }
                            }

                            int giveNum = 0;
                            try {
                                giveNum = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.ENCHANTS_GIVE_TYPE)), Integer.class);
                            } catch (InvalidValueException ignored) {
                            }

                            prizes.add(new EnchantPrize(enchantments, materials, giveNum, isHand));
                            continue;
                        }
                        case "console-command": {
                            ArrayList<String> commands;
                            try {
                                commands = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.CONSOLE_COMMAND)), ArrayList.class);
                            } catch (InvalidValueException e) {
                                commands = new ArrayList<>();
                            }
                            if (commands.isEmpty()) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty commands list! Ignoring!");
                                continue;
                            }
                            prizes.add(new ConsoleCommandPrize(commands));
                            continue;
                        }
                        case "player-command": {
                            ArrayList<String> commands;
                            try {
                                commands = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.PLAYER_COMMAND)), ArrayList.class);
                            } catch (InvalidValueException e) {
                                commands = new ArrayList<>();
                            }
                            if (commands.isEmpty()) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty commands list! Ignoring!");
                                continue;
                            }
                            prizes.add(new PlayerCommandPrize(commands));
                            continue;
                        }
                        case "smite-victim": {
                            Double damage;
                            try {
                                damage = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.SMITE_VICTIM)), Double.class);
                            } catch (InvalidValueException e) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has an invalid value smite-victim value! Ignoring!");
                                continue;
                            }
                            prizes.add(new SmitePrize(damage));
                            continue;
                        }
                        case "effects": {
                            ArrayList<String> effects;
                            try {
                                effects = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.EFFECTS_LIST)), ArrayList.class);
                            } catch (InvalidValueException e) {
                                effects = new ArrayList<>();
                            }
                            if (effects.isEmpty()) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty effects-list! Ignoring!");
                                continue;
                            }
                            ArrayList<PotionEffect> finalEffects = new ArrayList<>();
                            for (String ef : effects) {
                                String[] split = ef.split(":");
                                if (split.length < 2) {
                                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has incorrect effects-list prize: " + ef + "! Ignoring!");
                                    continue;
                                }
                                String name = split[0];
                                int time;
                                try {
                                    time = Integer.parseInt(split[1]);
                                } catch (NumberFormatException e) {
                                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has incorrect effect prize: " + ef + "! Ignoring!");
                                    continue;
                                }
                                int power = 1;
                                if (split.length == 3) {
                                    try {
                                        power = Integer.parseInt(split[2]);
                                    } catch (NumberFormatException e) {
                                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has incorrect effect prize: " + ef + "! Ignoring!");
                                        continue;
                                    }
                                }
                                PotionEffectType effect = PotionEffectType.getByName(name);
                                if (effect == null) {
                                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has incorrect effect prize: " + ef + "! Ignoring!");
                                    continue;
                                }
                                PotionEffect potionEffect = new PotionEffect(effect, time * 20, power);
                                finalEffects.add(potionEffect);
                            }

                            int effectGiveSize = 0;
                            try {
                                effectGiveSize = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.EFFECTS_GIVE_TYPE)), Integer.class);
                            } catch (InvalidValueException ignored) {
                            }
                            int giveToNum = 0;
                            try {
                                if (!ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.EFFECTS)), String.class).equalsIgnoreCase("player")) {
                                    giveToNum = 1;
                                }
                            } catch (InvalidValueException ignored) {
                            }
                            if (finalEffects.size() == 0) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has empty effects-list! Ignoring!");
                                continue;
                            }
                            prizes.add(new EffectPrize(giveToNum, finalEffects, effectGiveSize));
                            continue;
                        }
                        case "spawn-mob": {
                            ArrayList<String> spawnMobTypes;
                            try {
                                spawnMobTypes = ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.SPAWN_MOB_TYPES)), ArrayList.class);
                            } catch (InvalidValueException e) {
                                spawnMobTypes = new ArrayList<>();
                            }
                            if (spawnMobTypes.isEmpty()) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has spawn-mob prize, but no spawn-mob-types included");
                                continue;
                            }
                            ArrayList<Class<? extends Entity>> finalMobs = new ArrayList<>();
                            ArrayList<Integer> params = new ArrayList<>();
                            for (String spawnMobType : spawnMobTypes) {
                                try {
                                    String[] types = spawnMobType.split(":");
                                    if (types.length > 1) {
                                        try {
                                            params.add(Integer.parseInt(types[1]));
                                        } catch (NumberFormatException e) {
                                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid spawn-mob-types data for " + spawnMobType + "! Defaulting to 0!");
                                            params.add(0);
                                        }
                                    } else {
                                        params.add(0);
                                    }
                                    EntityType entityType = EntityType.valueOf(types[0].toUpperCase());
                                    finalMobs.add(entityType.getEntityClass());
                                } catch (IllegalArgumentException e) {
                                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid spawn-mob-type " + spawnMobType + "! Ignored!");
                                }
                            }
                            if (finalMobs.isEmpty()) {
                                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has spawn-mob prize, but no valid spawn-mob-types included");
                                continue;
                            }
                            int spawnAtNum = 0;
                            try {
                                if (!ObjectExtractor.extractObject(settings.get(Path.getPrizePath(perk, Path.SPAWN_MOB)), String.class).equalsIgnoreCase("player")) {
                                    spawnAtNum = 1;
                                }
                            } catch (InvalidValueException ignored) {
                            }
                            prizes.add(new SpawnMobPrize(spawnAtNum, finalMobs, params));
                            continue;
                        }
                        default: {
                            Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid prize " + prize + "! Disabling!");
                        }
                    }
                }
                String configEvent;
                try {
                    configEvent = ObjectExtractor.extractObject(settings.get(Path.getFullPath(perk, Path.EVENT)), String.class).toLowerCase();
                } catch (InvalidValueException e) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " doesn't have an event! Disabling!");
                    continue;
                }
                Metrics metrics = Main.getPlugin(Main.class).getMetrics();
                switch (configEvent) {
                    case "hit-arrow-entity": {
                        isDamageChange = false;
                        conditions.add(new HitProjectileCondition(Arrow.class));
                        event = new ProjectileHitEntityEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "HitArrowEntity"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "hit-arrow-player": {
                        isDamageChange = false;
                        conditions.add(new HitProjectileCondition(Arrow.class));
                        conditions.add(new IsVictimPlayerCondition());
                        event = new ProjectileHitEntityEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "HitArrowPlayer"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "hit-arrow-mob": {
                        isDamageChange = false;
                        conditions.add(new HitProjectileCondition(Arrow.class));
                        conditions.add(new IsVictimMobCondition());
                        event = new ProjectileHitEntityEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "HitArrowMob"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "kill": {
                        conditions.add(new IsDamagerPlayerCondition());
                        event = new KillEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "Kill"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "kill-player": {
                        conditions.add(new IsDamagerPlayerCondition());
                        conditions.add(new IsVictimPlayerCondition());
                        event = new KillEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "KillPlayer"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "kill-mob": {
                        conditions.add(new IsDamagerPlayerCondition());
                        conditions.add(new IsVictimMobCondition());
                        event = new KillEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "KillMob"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "death": {
                        conditions.add(new IsVictimPlayerCondition());
                        event = new DeathEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "Death"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "arrow-shoot": {
                        conditions.add(new IsShooterPlayerCondition());
                        event = new ArrowShootEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "ArrowShoot"));
                        isFireArrow = false;
                        isTripleArrow = false;
                        isTNTSpawn = false;
                        break;
                    }
                    case "hit-hand-entity": {
                        isDamageChange = false;
                        conditions.add(new IsDamagerPlayerCondition());
                        event = new HitHandEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "HitHandEntity"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "hit-hand-player": {
                        isDamageChange = false;
                        conditions.add(new IsDamagerPlayerCondition());
                        conditions.add(new IsVictimPlayerCondition());
                        event = new HitHandEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "HitHandPlayer"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "hit-hand-mob": {
                        isDamageChange = false;
                        conditions.add(new IsDamagerPlayerCondition());
                        conditions.add(new IsVictimMobCondition());
                        event = new HitHandEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "HitHandMob"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "damage-take": {
                        isDamageChange = false;
                        conditions.add(new IsVictimPlayerCondition());
                        isDamageCause = false;
                        event = new DamageTakeEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "DamageTake"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "consume": {
                        isConsumable = false;
                        event = new ConsumeEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "Consume"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "break": {
                        isBuilding = false;
                        event = new BreakEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "Break"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "place": {
                        isBuilding = false;
                        event = new PlaceEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "Place"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "snowball-hit": {
                        isDamageChange = false;
                        conditions.add(new HitProjectileCondition(Snowball.class));
                        event = new ProjectileHitEntityEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "SnowballHit"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "snowball-hit-player": {
                        isDamageChange = false;
                        conditions.add(new HitProjectileCondition(Snowball.class));
                        conditions.add(new IsVictimPlayerCondition());
                        event = new ProjectileHitEntityEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "SnowballHitPlayer"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "snowball-hit-mob": {
                        isDamageChange = false;
                        conditions.add(new HitProjectileCondition(Snowball.class));
                        conditions.add(new IsVictimMobCondition());
                        event = new ProjectileHitEntityEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "SnowballHitMob"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "enderpearl-teleport": {
                        isDamageChange = false;
                        event = new EnderPearlTeleportEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "EnderpearlTeleport"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "hit-arrow-block": {
                        conditions.add(new HitProjectileCondition(Arrow.class));
                        event = new ProjectileHitBlockEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "HitArrowBlock"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "hit-snowball-block": {
                        conditions.add(new HitProjectileCondition(Snowball.class));
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "HitSnowballBlock"));
                        event = new ProjectileHitBlockEvent(perk, prizes, conditions);
                        isTNTSpawn = false;
                        break;
                    }
                    case "world-change": {
                        event = new WorldChangeEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "WorldChange"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "respawn": {
                        event = new RespawnEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "Respawn"));
                        isTNTSpawn = false;
                        break;
                    }
                    case "command": {
                        event = new CommandEvent(perk, prizes, conditions);
                        metrics.addCustomChart(new Metrics.SimplePie("event", () -> "Command"));
                        isTNTSpawn = false;
                        break;
                    }
                    default: {
                        Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid event: " + configEvent + "! Disabling!");
                        continue;
                    }
                }
                if (isFireArrow) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has fire-arrow prize, but the event is not arrow-shoot! Disabling!");
                    break;
                }
                if (isTripleArrow) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has triple-arrow prize, but the event is not arrow-shoot! Disabling!");
                    break;
                }
                if (isBuilding) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has is-building-material, but the event is not place or break! Disabling!");
                    break;
                }
                if (isConsumable) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has is-potion or is-food condition, but the event is not consume! Disabling!");
                    break;
                }
                if (isDamageCause) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has fire-arrow prize, but the event is not arrow-shoot! Disabling!");
                    break;
                }
                if (isTNTSpawn) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has spawn-tnt prize, but the event does not support this prize! Disabling!");
                    break;
                }
                if (isDamageChange) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has damage changing prize, but the event does not have damaging! Disabling!");
                    break;
                }

                if (event instanceof CommandEvent) {
                    Main.getPlugin(Main.class).getCommandEvents().put(perk, event);
                } else {
                    Bukkit.getServer().getPluginManager().registerEvents(event, main);
                    events.add(event);
                }
            }
        }
    }
}
