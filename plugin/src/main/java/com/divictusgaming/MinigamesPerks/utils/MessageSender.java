package com.divictusgaming.MinigamesPerks.utils;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.enums.DefaultMessages;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

import java.util.HashMap;
import java.util.Map;

public class MessageSender {
    private static String prefix = "";

    public static void reloadPrefix() {
        prefix = (String) Main.getPlugin(Main.class).getMessages().get(DefaultMessages.PREFIX.getPath());
    }

    public static void sendMessage(CommandSender receiver, String messageId) {
        sendMessage(receiver, messageId, null, true);
    }

    public static void sendMessage(String message, CommandSender receiver) {
        receiver.sendMessage(prefix + message);
    }

    public static void sendMessage(CommandSender receiver, String messageId, HashMap<String, String> placeholders, boolean isId) {
        Map<String, Object> messages = Main.getPlugin(Main.class).getMessages();

        String message = messageId;
        if (isId) message = (String) messages.get(messageId);
        if (placeholders != null) {
            for (Map.Entry<String, String> ph : placeholders.entrySet()) {
                message = message.replaceAll(ph.getKey(), ph.getValue());
            }
        }

        receiver.sendMessage(prefix + message);
    }

    public static void sendMessage(CommandSender receiver, String messageId, boolean hasPrefix) {
        Map<String, Object> messages = Main.getPlugin(Main.class).getMessages();

        String message = (String) messages.get(messageId);
        if (hasPrefix) {
            receiver.sendMessage(prefix + message);
        } else {
            receiver.sendMessage(message);
        }
    }

    public static void sendMessage(CommandSender receiver, DefaultMessages message) {
        sendMessage(receiver, message.getPath(), true);
    }

    public static void sendMessage(CommandSender receiver, DefaultMessages message, Boolean hasPrefix) {
        sendMessage(receiver, message.getPath(), hasPrefix);
    }

    public static void sendMessage(CommandSender receiver, DefaultMessages messageId, HashMap<String, String> placeholders) {
        sendMessage(receiver, messageId.getPath(), placeholders, true);
    }

    public static void sendConsoleMessage(DefaultMessages messageId) {
        sendMessage(Bukkit.getConsoleSender(), messageId.getPath());
    }

    public static void sendBoth(CommandSender receiver, DefaultMessages messageId) {
        if (!(receiver instanceof ConsoleCommandSender)) {
            sendConsoleMessage(messageId);
        }
        sendMessage(receiver, messageId.getPath());
    }

}
