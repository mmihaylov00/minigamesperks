package com.divictusgaming.MinigamesPerks.utils;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.Perk;
import com.divictusgaming.MinigamesPerks.enums.DefaultMessages;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringFormatter {
    public static String format(String message) {
        StringBuilder sb = new StringBuilder();
        sb.append(message);
        final String regex = "(&([0-9a-fk-or]))";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(message);

        final ChatColor[] colors = ChatColor.values();
        while (matcher.find())
            for (ChatColor color : colors)
                if (matcher.group(2).charAt(0) == color.getChar())
                    sb.replace(matcher.start(), matcher.end(), color.toString());

        return sb.toString();
    }

    public static String replaceVars(String str, int currentLevel, Perk perk, Player player) {
        str = str.replaceAll("\\{level}", String.valueOf(currentLevel))
                .replaceAll("\\{nextLevel}", String.valueOf(currentLevel + 1))
                .replaceAll("\\{level_roman}", String.valueOf(RomanConventer.toRoman(currentLevel)))
                .replaceAll("\\{nextLevel_roman}", String.valueOf(RomanConventer.toRoman(currentLevel + 1)));
        if (player != null) {
            final Pattern pattern = Pattern.compile("%[A-Za-z_{}.0-9]+%", Pattern.MULTILINE);
            final Matcher matcher = pattern.matcher(str);
            while (matcher.find()) {
                for (int j = 0; j <= matcher.groupCount(); j++) {
                    String val = PlaceholderAPI.setPlaceholders(player, matcher.group(j));
                    str = str.replace(matcher.group(j), val);
                }
            }
        }
        if (perk != null) {
            str = str.replaceAll("\\{maxLevel}", String.valueOf(perk.getMaxLevel()));
            if (perk.getPermission() != null) {
                Map<String, Object> messages = Main.getPlugin(Main.class).getMessages();
                String yes = (String) messages.get(DefaultMessages.HAS_PERMISSION_PLACEHOLDER.getPath());
                String no = (String) messages.get(DefaultMessages.NO_PERMISSION_PLACEHOLDER.getPath());
                str = str.replaceAll("\\{has_permission}",
                        (player.hasPermission(perk.getPermission()) || player.isOp() || player.hasPermission("*")) ?
                                yes : no
                );
            }
            double chance = currentLevel * perk.getPercentIncrease();
            int chanceInt = (int) Math.floor(chance);
            double upgradeChance = (currentLevel + 1) * perk.getPercentIncrease();
            int upgradeChanceInt = (int) Math.floor(upgradeChance);
            double cost = (currentLevel * perk.getCostIncrease()) + perk.getCost();
            int costInt = (int) Math.floor(cost);
            str = str.replaceAll("\\{chance}", String.valueOf(chance))
                    .replaceAll("\\{chance_rounded}", String.valueOf(chanceInt))
                    .replaceAll("\\{upgradeChance}", String.valueOf(upgradeChance))
                    .replaceAll("\\{upgradeChance_rounded}", String.valueOf(upgradeChanceInt))
                    .replaceAll("\\{upgradeCost}", String.valueOf(cost))
                    .replaceAll("\\{upgradeCost_rounded}", String.valueOf(costInt));
        }

        return str;
    }
}
