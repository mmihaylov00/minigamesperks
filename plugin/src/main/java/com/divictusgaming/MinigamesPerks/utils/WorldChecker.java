package com.divictusgaming.MinigamesPerks.utils;

import com.divictusgaming.MinigamesPerks.Main;
import org.bukkit.World;

public class WorldChecker {
    public static boolean checkWorld(String worldName){
        for (String disabledWorld : Main.getPlugin(Main.class).getConfigObject().getDisabledWorlds())
            if (disabledWorld.equals(worldName)) return true;

        return false;
    }
    public static boolean checkWorld(World worldName){
        return checkWorld(worldName.getName());
    }
}
