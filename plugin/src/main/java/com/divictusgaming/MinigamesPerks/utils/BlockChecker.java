package com.divictusgaming.MinigamesPerks.utils;


import com.divictusgaming.MinigamesPerks.Main;
import org.bukkit.Material;

import java.util.List;

public class BlockChecker {
    public static boolean isBuildingMaterial(Material material){
        List<Material> blocks = Main.getPlugin(Main.class).getBuildingBlocks();
        for (Material block : blocks) {
            if (block == material){
                return true;
            }
        }
        return false;
    }
}
