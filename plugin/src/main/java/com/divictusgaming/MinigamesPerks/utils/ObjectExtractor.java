package com.divictusgaming.MinigamesPerks.utils;

import com.divictusgaming.MinigamesPerks.exceptions.InvalidValueException;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class ObjectExtractor {

    public static ItemStack stringToItem(String material) throws InvalidValueException {
        try {
            String[] split = material.split(":");
            Material mat = Material.valueOf(split[0]);
            byte meta = 0;
            if (split.length > 1) {
                meta = Byte.parseByte(split[1]);
            }
            return new ItemStack(mat, 1, meta);
        }catch (IllegalArgumentException e){
            throw new InvalidValueException();
        }
    }

    public static <T> T extractObject(Object value, Class<T> e) throws InvalidValueException {

        try {
            if (e.isInstance(value)) {
                return (T) value;
            }
            if (e == Integer.class) {
                if (value == null){
                    return (T) new Integer(0);
                }
                if (value instanceof String) {
                    return (T) new Integer(Integer.parseInt((String) value));
                }
                if (value instanceof Double) {
                    throw new InvalidValueException();
                }
                if (value instanceof Boolean) {
                    throw new InvalidValueException();
                }
            }
            if (e == Double.class) {
                if (value == null){
                    return (T) new Double(0);
                }
                if (value instanceof String) {
                    return (T) new Double(Double.parseDouble((String) value));
                }
                if (value instanceof Integer) {
                    return (T) new Double((Integer) value);
                }
                if (value instanceof Boolean) {
                    throw new InvalidValueException();
                }
            }
            if (e == String.class) {
                if (value == null){
                    return (T) "";
                }
                if (value instanceof Double) {
                    return (T) (((Double) value) + "");
                }
                if (value instanceof Integer) {
                    return (T) (((Integer) value) + "");
                }
                if (value instanceof Boolean) {
                    return (T) (((Boolean) value) + "");
                }
            }
            if (e == Boolean.class) {
                if (value == null){
                    return (T) Boolean.FALSE;
                }
                if (value instanceof Double) {
                    return (T) Boolean.FALSE;
                }
                if (value instanceof Integer) {
                    return (T) Boolean.FALSE;
                }
                if (value instanceof String) {
                    return (T) new Boolean(Boolean.parseBoolean((String) value));
                }
            }
            if (e == ArrayList.class) {
                if (value == null){
                    return (T) new ArrayList<>();
                }
                if (value instanceof Double) {
                    ArrayList<Double> list = new ArrayList<>();
                    list.add((Double) value);
                    return (T) list;
                }
                if (value instanceof Integer) {
                    ArrayList<Integer> list = new ArrayList<>();
                    list.add((Integer) value);
                    return (T) list;
                }
                if (value instanceof String) {
                    ArrayList<String> list = new ArrayList<>();
                    list.add((String) value);
                    return (T) list;
                }
            }
        }catch (NumberFormatException ignored){
        }
        throw new InvalidValueException();
    }

}
