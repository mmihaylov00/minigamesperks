package com.divictusgaming.MinigamesPerks.utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.Base64;
import java.util.UUID;

public class HeadWorker {
    public static void setHeadSkin(SkullMeta im, String url) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        if (url.startsWith("http")) {
            byte[] encodedData = Base64.getEncoder().encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
            profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
        } else {
            profile.getProperties().put("textures", new Property("textures", url));
        }

        try {
            Field profileField = im.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(im, profile);
            profileField.setAccessible(false);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
            e1.printStackTrace();
        }
    }
}
