package com.divictusgaming.MinigamesPerks.utils;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.Perk;
import com.divictusgaming.MinigamesPerks.enums.DefaultMessages;
import com.divictusgaming.MinigamesPerks.inventories.ConfirmMenu;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PerkUpgrader {
    public static boolean upgradePerk(Player player, String perk, boolean openConfirm) {
        Main plugin = Main.getPlugin(Main.class);
        Perk perkObj = plugin.getPerks().getPerksObject().get(perk);

        if (perkObj.getPermission() != null &&
                !perkObj.getPermission().isEmpty() &&
                !player.hasPermission(perkObj.getPermission()) &&
                !player.isOp() &&
                !player.hasPermission("*")) {
            MessageSender.sendMessage(perkObj.getPermissionMessage(), player);
            return false;
        }

        HashMap<String, Integer> levels = plugin.getDb().getLevels(player.getUniqueId());
        Integer level = levels.get(perk);
        if (level >= perkObj.getMaxLevel()) {
            MessageSender.sendMessage(player, DefaultMessages.PERK_MAXED);
            return false;
        }


        HashMap<String, String> placeholders = new HashMap<>();

        String economyPlaceholder = plugin.getConfigObject().getEcoPlaceholder();
        Double balance = parsePlaceholder(player, economyPlaceholder);

        if (balance == null) return false;

        double updateCost = (level * perkObj.getCostIncrease()) + perkObj.getCost();
        int updateCostInt = (int) Math.floor(updateCost);

        if (balance < updateCost) {
            MessageSender.sendMessage(player, DefaultMessages.NOT_ENOUGH_BALANCE);
            return false;
        }
        if (openConfirm) {
            ConfirmMenu.openConfirmMenu(player, perkObj);
            return false;
        }

        String command = plugin.getConfigObject().getEcoCommand()
                .replaceAll("\\{cost_rounded}", String.valueOf(updateCostInt))
                .replaceAll("\\{cost}", String.valueOf(updateCost))
                .replaceAll("\\{player}", player.getName());

        Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), command);

        Double endBalance = parsePlaceholder(player, economyPlaceholder);

        if (endBalance == null) return false;

        plugin.getDb().setLevel(player.getUniqueId(), perk, level + 1);
        placeholders.put("\\{perkName}", perkObj.getName());
        placeholders.put("\\{level}", String.valueOf(level + 1));
        MessageSender.sendMessage(player, DefaultMessages.PERK_UPGRADED, placeholders);

        if (!perkObj.getUpgradeCommands().isEmpty())
            for (String upgradeCommand : perkObj.getUpgradeCommands())
                Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(),
                        upgradeCommand.replaceAll("\\{player}", player.getName())
                                .replaceAll("\\{cost}", String.valueOf(updateCost))
                                .replaceAll("\\{perkName}", perkObj.getName())
                                .replaceAll("\\{level}", String.valueOf(level + 1))
                                .replaceAll("\\{cost_rounded}", String.valueOf(updateCostInt)));

        return true;
    }

    public static boolean setPerkLevel(CommandSender executor, Player player, String perk, Integer level) {
        Main plugin = Main.getPlugin(Main.class);
        Perk perkObj = plugin.getPerks().getPerksObject().get(perk);
        if (perkObj.getPermission() != null &&
                !perkObj.getPermission().isEmpty() &&
                !player.hasPermission(perkObj.getPermission()) &&
                !player.isOp() &&
                !player.hasPermission("*")) {
            MessageSender.sendMessage(perkObj.getPermissionMessage(), player);
            return false;
        }

        if (level > perkObj.getMaxLevel() || level < 0) {
            MessageSender.sendMessage(executor, DefaultMessages.INVALID_PERK_LEVEL);
            return false;
        }

        HashMap<String, String> placeholders = new HashMap<>();

        plugin.getDb().setLevel(player.getUniqueId(), perk, level);
        placeholders.put("\\{perkName}", perkObj.getName());
        placeholders.put("\\{level}", String.valueOf(level));
        MessageSender.sendMessage(player, DefaultMessages.PERK_UPGRADED, placeholders);

        return true;
    }

    private static Double parsePlaceholder(Player player, String economyPlaceholder) {
        String value = PlaceholderAPI.setPlaceholders(player, economyPlaceholder);
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            try {
                final Matcher matcher = Pattern.compile("[0-9.,]+", Pattern.MULTILINE).matcher(value);
                StringBuilder endValue = new StringBuilder();
                while (matcher.find())
                    endValue.append(matcher.group(0));

                return Double.parseDouble(endValue.toString());
            } catch (NumberFormatException ignored) {
            }

            MessageSender.sendMessage(player, DefaultMessages.SOMETHING_WENT_WRONG);
            Bukkit.getLogger().warning(ChatColor.RED + "The Minigames Perks economy placeholder is invalid, it's value was " +
                    value + ", please fix!");
            e.printStackTrace();
            return null;
        }
    }
}
