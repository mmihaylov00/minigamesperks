package com.divictusgaming.MinigamesPerks.enums;

public enum DefaultMessages {
    PREFIX("perks.prefix", "&c&l[&a&lPerks&c&l] &f"),
    RELOAD_STARTED("perks.reload.started", "&eReloading..."),
    RELOAD_FAILED("perks.reload.started", "&eFailed to reload the plugin!"),
    RELOAD_COMPLETED("perks.reload.completed", "&eReload completed!"),
    INVALID_PLAYER("perks.player-invalid", "&cInvalid player!"),
    PERMISSION_DENIED("perks.permission-denied", "&cPermission denied!"),
    PERMISSION_DENIED_UPGRADE("perks.default-upgrade-permission-denied", "&cYou don't have permission to upgrade this perk!"),
    HAS_PERMISSION_PLACEHOLDER("perks.placeholder.permission-true", "&aYes"),
    NO_PERMISSION_PLACEHOLDER("perks.placeholder.permission-false", "&cNo"),
    PERK_MAXED("perks.upgrade.maxed", "&cThis perk is already maxed!"),
    PERK_UPGRADED("perks.upgrade.upgraded", "&aPerk {perkName}&a has been upgraded to &9level {level}&a!"),
    NOT_ENOUGH_BALANCE("perks.upgrade.not-enough-balance", "&cYou don't have enough coins!"),
    SOMETHING_WENT_WRONG("perks.something-went-wrong", "&cSomething went wrong, please contact an administrator!"),
    INVALID_PERK("perks.invalid-perk", "&cInvalid perk name!"),
    INVALID_PERK_LEVEL("perks.invalid-perk-level", "&cInvalid perk level!"),
    INVALID_OR_NOT_COMMAND_PERK("perks.not-command", "&cThis perk isn't a command executable perk!"),
    INVALID_COMMAND("perks.invalid-command", "&cInvalid command, please use &e/mp help"),
    HELP_MENU_HEADER("perks.help.menu-header", "&c&m&l---*---*--&a &lMinigames Perk&c &m&l--*---*---"),
    HELP_LINE_1("perks.help.line-1", "&r  &a/mp open &7- &eOpen the menu"),
    HELP_LINE_2("perks.help.line-2", "&r  &a/mp perks &7 - &eList all perks"),
    HELP_LINE_3("perks.help.line-3", "&r  &a/mp upgrade &6[perk] &7- &eUpgrade a perk"),
    ADMIN_HELP_LINE_1("perks.admin-help.line-1", "&r  &a/mp open &6[player] &7- &eOpen the menu for a player"),
    ADMIN_HELP_LINE_2("perks.admin-help.line-2", "&r  &a/mp reload &7- &eReload the configs"),
    ADMIN_HELP_LINE_3("perks.admin-help.line-3", "&r  &a/mp perks &7 - &eList all perks"),
    ADMIN_HELP_LINE_4("perks.admin-help.line-4", "&r  &a/mp upgrade &6<perk> [player] &7- &eUpgrade a perk for a player"),
    ADMIN_HELP_LINE_5("perks.admin-help.line-5", "&r  &a/mp execute &6<perk> <player> &7- &eExecute a command perk"),
    ADMIN_HELP_LINE_6("perks.admin-help.line-6", "&r  &a/mp set-level &6<perk> <player> <level> &7- &eSet the perk's level"),
    PERK_EXECUTED("perks.executed", "&aPerk {perkName} executed for {player}!"),
    PERK_NOT_SPECIFIED("perks.perk-not-specified","&cPlease specify the perk's id. Use /mp perks to list them."),
    PLAYER_NOT_SPECIFIED("perks.player-not-specified", "&cPlease specify a player."),
    LEVEL_NOT_SPECIFIED("perks.level-not-specified", "&cPlease specify a perk level.");


    private String path;
    private String value;

    DefaultMessages(String path, String value) {
        this.path = path;
        this.value = value;
    }

    public String getPath() {
        return path;
    }

    public String getValue() {
        return value;
    }
}
