package com.divictusgaming.MinigamesPerks.enums;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.entities.PerkValue;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffectType;

public enum DefaultPerks {
    ARROW("perks.arrow", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.ARROW.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lArrow Recovery"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to get your||" +
                            "&7arrow back on hitting a player.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to get your||" +
                            "&7arrow back on hitting a player.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 2000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 1500.0),
            new PerkValue(Path.PERCENT.getPath(), 5.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 10),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou got your arrow back from your &eArrow Recovery &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "hit-arrow-player"),
            new PerkValue(Path.PRIZES.getPath() + Path.ITEMS.getPath(), Material.ARROW.name(), null),
            new PerkValue(Path.PRIZES.getPath() + Path.ITEMS_GIVE_TYPE.getPath(), "all"),
            new PerkValue(Path.CONDITIONS.getPath() + Path.BOW_NOT_INFINITY.getPath(), true)
    ),

    BLAZE("perks.blaze", new PerkValue(Path.DISPLAY_ITEM.getPath(),
            Main.getPlugin(Main.class).getINMSMain().getVersionMaterialName(1)),
            new PerkValue(Path.NAME.getPath(), "&a&lBlazing Arrow"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to||" +
                            "&7shoot a burning arrow.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to||" +
                            "&7shoot a burning arrow.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 4000),
            new PerkValue(Path.COST_INCREASE.getPath(), 2000.0),
            new PerkValue(Path.PERCENT.getPath(), 5.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 11),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou shot a fire arrow from your &eBlazing Arrow &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "arrow-shoot"),
            new PerkValue(Path.PRIZES.getPath() + Path.FIRE_ARROW.getPath(), true)
    ),
    EPEARL("perks.epearl", new PerkValue(Path.DISPLAY_ITEM.getPath(),
            Main.getPlugin(Main.class).getINMSMain().getVersionMaterialName(2)),
            new PerkValue(Path.NAME.getPath(), "&a&lEnder Mastery"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&7You will take 40% less damage||" +
                            "&7when you teleport with an ender pearl.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(),
                    "&7You take 40% less damage when||" +
                            "&7you teleport with an ender peark.||&r||" +
                            "&e&lPurchased!"),
            new PerkValue(Path.COST.getPath(), 5000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 1.0),
            new PerkValue(Path.PERCENT.getPath(), 100.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 1),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 12),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), false),
            new PerkValue(Path.MESSAGE.getPath(), ""),
            new PerkValue(Path.EVENT.getPath(), "enderpearl-teleport"),
            new PerkValue(Path.PRIZES.getPath() + Path.REDUCE_DAMAGE.getPath(), 40)
    ),

    RAGE("perks.rage", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.IRON_SWORD.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lRage"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to get Strength II||" +
                            "&7for 5 seconds after killing a player.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to get Strength II||" +
                            "&7for 5 seconds after killing a player.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 4000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 2000.0),
            new PerkValue(Path.PERCENT.getPath(), 5.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 13),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou got {effect} {amplifier} from your &eRage &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "kill-player"),
            new PerkValue(Path.PRIZES.getPath() + Path.EFFECTS.getPath(), "player"),
            new PerkValue(Path.PRIZES.getPath() + Path.EFFECTS_LIST.getPath(),
                    PotionEffectType.INCREASE_DAMAGE.getName() + ":5:2", null)
    ),

    BLOODTHIRST("perks.bloodthirst", new PerkValue(Path.DISPLAY_ITEM.getPath(),
            Main.getPlugin(Main.class).getINMSMain().getVersionMaterialName(3)),
            new PerkValue(Path.NAME.getPath(), "&a&lBloodthirst"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to get Regeneration II||" +
                            "&7for 5 seconds after killing a player.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to get Regeneration II||" +
                            "&7for 5 seconds after killing a player.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 4000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 2000.0),
            new PerkValue(Path.PERCENT.getPath(), 5.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 14),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou got {effect} {amplifier} from your &eBloodthirst &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "kill-player"),
            new PerkValue(Path.PRIZES.getPath() + Path.EFFECTS.getPath(), "player"),
            new PerkValue(Path.PRIZES.getPath() + Path.EFFECTS_LIST.getPath(),
                    PotionEffectType.REGENERATION.getName() + ":5:2", null)
    ),


    MARKSMANSHIP("perks.marksmanship", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.BOW.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lMarksmanship"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to get +1 random enchant level on||" +
                            "&7all bows in your inventory after killing a player.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to get +1 random enchant level on||" +
                            "&7all bows in your inventory after killing a player.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 4000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 2000.0),
            new PerkValue(Path.PERCENT.getPath(), 4.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 15),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou got +1 {enchant} to all bows from your &eMarksmanship &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "kill-player"),
            new PerkValue(Path.PRIZES.getPath() + Path.ENCHANTS_ITEMS.getPath(), Material.BOW.name(), null),
            new PerkValue(Path.PRIZES.getPath() + Path.ENCHANTS_GIVE_TYPE.getPath(), 1),
            new PerkValue(Path.PRIZES.getPath() + Path.ENCHANTS.getPath(),
                    Main.getPlugin(Main.class).getINMSMain().getEnchantName(Enchantment.ARROW_DAMAGE),
                    Main.getPlugin(Main.class).getINMSMain().getEnchantName(Enchantment.ARROW_FIRE),
                    Main.getPlugin(Main.class).getINMSMain().getEnchantName(Enchantment.DURABILITY),
                    Main.getPlugin(Main.class).getINMSMain().getEnchantName(Enchantment.ARROW_KNOCKBACK))
    ),

    MINEEXPERT("perks.mineexpert", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.IRON_PICKAXE.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lMining Expert"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to get||" +
                            "&7an extra block on mining.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to get||" +
                            "&7an extra block on mining.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 3000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 1500.0),
            new PerkValue(Path.PERCENT.getPath(), 7.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 16),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou got an extra block from your &eMining Expert &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "break"),
            new PerkValue(Path.CONDITIONS.getPath() + Path.IS_BUILDING_MATERIAL.getPath(), true),
            new PerkValue(Path.PRIZES.getPath() + Path.ITEMS.getPath(), "broken-block", null),
            new PerkValue(Path.PRIZES.getPath() + Path.ITEMS_GIVE_TYPE.getPath(), "all")
    ),


    ENCHANTER("perks.enchanter", new PerkValue(Path.DISPLAY_ITEM.getPath(),
            Main.getPlugin(Main.class).getINMSMain().getVersionMaterialName(6)),
            new PerkValue(Path.NAME.getPath(), "&a&lEnchanter"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to||" +
                            "&7get 1 EXP Level on kill.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to||" +
                            "&7get 1 EXP Level on kill.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 4000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 2000.0),
            new PerkValue(Path.PERCENT.getPath(), 10.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 19),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou leveled up from your &eEnchanter &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "kill-player"),
            new PerkValue(Path.PRIZES.getPath() + Path.EXP_LEVEL.getPath(), 1)
    ),

    ANNOYER("perks.annoyer", new PerkValue(Path.DISPLAY_ITEM.getPath(),
            Main.getPlugin(Main.class).getINMSMain().getVersionMaterialName(5)),
            new PerkValue(Path.NAME.getPath(), "&a&lAnnoyer"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to spawn an endermite||" +
                            "&7when you hit a player with an arrow.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to spawn an endermite||" +
                            "&7when you hit a player with an arrow.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 4000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 2000.0),
            new PerkValue(Path.PERCENT.getPath(), 3.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 20),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou spawned an endermite from your &eAnnoyer &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "hit-arrow-player"),
            new PerkValue(Path.PRIZES.getPath() + Path.SPAWN_MOB.getPath(), "victim"),
            new PerkValue(Path.PRIZES.getPath() + Path.SPAWN_MOB_TYPES.getPath(), EntityType.ENDERMITE.getName(), null)
    ),

    PEOPLEEATER("perks.peopleeater", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.BREAD.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lPeople Eater"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to fill your||" +
                            "&7hunger after killing a player.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to fill your||" +
                            "&7hunger after killing a player.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 6000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 0.0),
            new PerkValue(Path.PERCENT.getPath(), 100.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 1),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 21),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou fed yourself from your &ePeople Eater &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "kill-player"),
            new PerkValue(Path.PRIZES.getPath() + Path.FEED.getPath(), 20)
    ),

    REVENGE("perks.revenge", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.SPIDER_EYE.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lRevenge"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to spawn||" +
                            "&7a spider on death.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to spawn||" +
                            "&7a spider on death.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 8000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 0.0),
            new PerkValue(Path.PERCENT.getPath(), 5.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 1),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 22),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou spawned a spider from your &eRevenge &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "death"),
            new PerkValue(Path.PRIZES.getPath() + Path.SPAWN_MOB.getPath(), "player"),
            new PerkValue(Path.PRIZES.getPath() + Path.SPAWN_MOB_TYPES.getPath(), EntityType.SPIDER.getName(), null)
    ),

    BRIDGE("perks.bridge", new PerkValue(Path.DISPLAY_ITEM.getPath(),
            Main.getPlugin(Main.class).getINMSMain().getVersionMaterialName(4)),
            new PerkValue(Path.NAME.getPath(), "&a&lBridge Builder"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to keep||" +
                            "&7your block when you place it.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to keep||" +
                            "&7your block when you place it.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 3000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 1500.0),
            new PerkValue(Path.PERCENT.getPath(), 5.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 23),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou kept your block from your &eBridge Builder &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "place"),
            new PerkValue(Path.CONDITIONS.getPath() + Path.IS_BUILDING_MATERIAL.getPath(), true),
            new PerkValue(Path.PRIZES.getPath() + Path.ITEMS.getPath(), "hand-item", null),
            new PerkValue(Path.PRIZES.getPath() + Path.ITEMS_GIVE_TYPE.getPath(), "all")
    ),

    GAPPLE("perks.gapple", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.GOLDEN_APPLE.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lApple of Death"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to||" +
                            "&7get a golden apple on kill.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to get||" +
                            "&7a golden apple on kill.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 4000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 2000.0),
            new PerkValue(Path.PERCENT.getPath(), 1.5),
            new PerkValue(Path.MAX_LEVEL.getPath(), 20),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 24),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou got a golden apple from your &eApple of Death &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "kill-player"),
            new PerkValue(Path.PRIZES.getPath() + Path.ITEMS.getPath(), Material.GOLDEN_APPLE.name(), null),
            new PerkValue(Path.PRIZES.getPath() + Path.ITEMS_GIVE_TYPE.getPath(), "all")
    ),

    PEARLSTEALER("perks.pearlstealer", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.ENDER_PEARL.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lPearl Stealer"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to||" +
                            "&7get an ender pearl on void kill.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to||" +
                            "&7get an ender pearl on void kill.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 5000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 2000.0),
            new PerkValue(Path.PERCENT.getPath(), 3.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
                new PerkValue(Path.POSITION.getPath(), 25),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou got an ender pearl from your &ePearl Stealer &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "kill-player"),
            new PerkValue(Path.PRIZES.getPath() + Path.ITEMS.getPath(), Material.ENDER_PEARL.name(), null),
            new PerkValue(Path.CONDITIONS.getPath() + Path.IS_LAST_DAMAGE_CAUSE.getPath(), "victim"),
            new PerkValue(Path.CONDITIONS.getPath() + Path.IS_LAST_DAMAGE_CAUSE_LIST.getPath(), EntityDamageEvent.DamageCause.VOID.name(), null),
            new PerkValue(Path.PRIZES.getPath() + Path.ITEMS_GIVE_TYPE.getPath(), "all")
    ),

    BARBARIAN("perks.barbarian", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.DIAMOND_AXE.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lBarbarian"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to||" +
                            "&7deal more damage with axes.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to||" +
                            "&7deal more damage with axes.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 3000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 1500.0),
            new PerkValue(Path.PERCENT.getPath(), 1.5),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 28),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou dealt bonus damage from your &eBarbarian &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "hit-hand-entity"),
            new PerkValue(Path.CONDITIONS.getPath() + Path.HOLDS_ITEM_MAIN.getPath(),
                    Main.getPlugin(Main.class).getINMSMain().getVersionMaterial(4).name(),
                    Material.STONE_AXE.name(),
                    Main.getPlugin(Main.class).getINMSMain().getVersionMaterial(3).name(),
                    Material.IRON_AXE.name(),
                    Material.DIAMOND_AXE.name()),
            new PerkValue(Path.PRIZES.getPath() + Path.INCREASE_DAMAGE.getPath(), 50d)
    ),

    TANK("perks.tank", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.DIAMOND_CHESTPLATE.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lTanky"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to reduce damage||" +
                            "&7taken while wearing a chestplate.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to reduce damage||" +
                            "&7taken while wearing a chestplate.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 3000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 1500.0),
            new PerkValue(Path.PERCENT.getPath(), 1.5),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 29),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou reduced damage from your &eTanky &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "damage-take"),
            new PerkValue(Path.CONDITIONS.getPath() + Path.HAS_ARMOR_CHESTPLATE.getPath(), "player"),
            new PerkValue(Path.CONDITIONS.getPath() + Path.HAS_ARMOR_CHESTPLATE_ITEMS.getPath(),
                    Material.LEATHER_CHESTPLATE.name(),
                    Material.CHAINMAIL_CHESTPLATE.name(),
                    Material.IRON_CHESTPLATE.name(),
                    Main.getPlugin(Main.class).getINMSMain().getVersionMaterialName(8),
                    Material.DIAMOND_CHESTPLATE.name()),
            new PerkValue(Path.PRIZES.getPath() + Path.REDUCE_DAMAGE.getPath(), 50d)
    ),

    HUNGRY("perks.hungry", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.CAKE.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lHungry"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to restore||" +
                            "&7more food and health after eating.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to restore more||" +
                            "&7food and health after eating.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 3000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 1500.0),
            new PerkValue(Path.PERCENT.getPath(), 3.5),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 30),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou healed and restore more food from your &eHungry &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "consume"),
            new PerkValue(Path.CONDITIONS.getPath() + Path.IS_FOOD.getPath(), "any", null),
            new PerkValue(Path.PRIZES.getPath() + Path.FEED.getPath(), 2),
            new PerkValue(Path.PRIZES.getPath() + Path.HEAL.getPath(), 2)
    ),

    GLADIATOR("perks.gladiator", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.DIAMOND_SWORD.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lGladiator"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to get +1 random enchant level on||" +
                            "&7all swords in your inventory after killing a player.||&r"),
            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to get +1 random enchant level on||" +
                            "&7all swords in your inventory after killing a player.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 4000.0),
            new PerkValue(Path.COST_INCREASE.getPath(), 2000.0),
            new PerkValue(Path.PERCENT.getPath(), 2.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 31),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou got +1 {enchant} to all swords from your &eGladiator &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "kill-player"),
            new PerkValue(Path.PRIZES.getPath() + Path.ENCHANTS_ITEMS.getPath(),
                    Main.getPlugin(Main.class).getINMSMain().getVersionMaterial(2).name(),
                    Material.STONE_SWORD.name(),
                    Main.getPlugin(Main.class).getINMSMain().getVersionMaterial(1).name(),
                    Material.IRON_SWORD.name(),
                    Material.DIAMOND_SWORD.name()),
            new PerkValue(Path.PRIZES.getPath() + Path.ENCHANTS_GIVE_TYPE.getPath(), 1),
            new PerkValue(Path.PRIZES.getPath() + Path.ENCHANTS.getPath(),
                    Main.getPlugin(Main.class).getINMSMain().getEnchantName(Enchantment.DAMAGE_ALL),
                    Main.getPlugin(Main.class).getINMSMain().getEnchantName(Enchantment.DAMAGE_ARTHROPODS),
                    Main.getPlugin(Main.class).getINMSMain().getEnchantName(Enchantment.FIRE_ASPECT),
                    Main.getPlugin(Main.class).getINMSMain().getEnchantName(Enchantment.KNOCKBACK),
                    Main.getPlugin(Main.class).getINMSMain().getEnchantName(Enchantment.DURABILITY),
                    Main.getPlugin(Main.class).getINMSMain().getEnchantName(Enchantment.DAMAGE_UNDEAD))
    ),

    TRIPLE_SHOT("perks.triple-shot", new PerkValue(Path.DISPLAY_ITEM.getPath(), Material.FEATHER.name()),
            new PerkValue(Path.NAME.getPath(), "&a&lTriple Shot"),
            new PerkValue(Path.LORE.getPath(),
                    "&cLevel: &7{level}/{maxLevel}||" +
                            "||" +
                            "&7You have &3{chance}%&7 chance to||" +
                            "&7shoot a three arrows.||&r"),
                            new PerkValue(Path.LAST_LORE_UPGRADE.getPath(),
                    "&e&lUpgrade to:||" +
                            "&3{upgradeChance}% &7chance to||" +
                            "&7shoot three arrows.||&r||" +
                            "&eCost:&7 {upgradeCost_rounded} coins||&aClick to upgrade!"),
                            new PerkValue(Path.LAST_LORE_MAXED.getPath(), "&e&lFully Upgraded!"),
            new PerkValue(Path.COST.getPath(), 4000),
            new PerkValue(Path.COST_INCREASE.getPath(), 2000.0),
            new PerkValue(Path.PERCENT.getPath(), 3.0),
            new PerkValue(Path.MAX_LEVEL.getPath(), 5),
            new PerkValue(Path.STARTING_LEVEL.getPath(), 0),
            new PerkValue(Path.POSITION.getPath(), 32),
            new PerkValue(Path.ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE_ENABLED.getPath(), true),
            new PerkValue(Path.MESSAGE.getPath(), "&aYou shot three arrows from your &eTriple Shot &aperk!"),
            new PerkValue(Path.EVENT.getPath(), "arrow-shoot"),
            new PerkValue(Path.PRIZES.getPath() + Path.TRIPLE_ARROW.getPath(), true)
            ),
    ;

    private final String path;
    private final PerkValue[] custom;

    DefaultPerks(String path, PerkValue... custom) {
        this.path = path;
        this.custom = custom;
    }

    public String getPath() {
        return path;
    }

    public PerkValue[] getCustomValues() {
        return custom;
    }
}
