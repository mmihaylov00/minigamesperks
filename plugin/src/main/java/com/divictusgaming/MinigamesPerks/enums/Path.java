package com.divictusgaming.MinigamesPerks.enums;

public enum Path {
    MAIN("perks."),
    EVENT(".event"),
    PERMISSION(".permission"),
    PERMISSION_MESSAGE(".permission-message"),
    DISPLAY_ITEM(".display-item"),
    UPGRADE_COMMANDS(".upgrade-commands"),
    IS_UNDER_HP(".is-under-hp"),
    STARTING_LEVEL(".starting-level"),
    IS_UNDER_HP_VALUE(".is-under-hp-value"),
    JAVASCRIPT_CONDITION(".javascript-condition"),
    IS_ABOVE_HP(".is-above-hp"),
    IS_IN_WORLD(".is-in-world"),
    BOW_NOT_INFINITY(".bow-not-infinity"),
    IS_ABOVE_HP_VALUE(".is-above-hp-value"),
    IS_POTION(".is-potion"),
    IS_FOOD(".is-food"),
    IS_BUILDING_MATERIAL(".is-building-material"),
    HAS_ARMOR_HELMET(".has-armor-helmet"),
    HAS_ARMOR_HELMET_ITEMS(".has-armor-helmet-items"),
    HAS_ARMOR_CHESTPLATE(".has-armor-chestplate"),
    HAS_ARMOR_CHESTPLATE_ITEMS(".has-armor-chestplate-items"),
    HAS_ARMOR_LEGGINGS(".has-armor-leggings"),
    HAS_ARMOR_LEGGINGS_ITEMS(".has-armor-leggings-items"),
    HAS_ARMOR_BOOTS(".has-armor-boots"),
    HAS_ARMOR_BOOTS_ITEMS(".has-armor-boots-items"),
    IS_DAMAGE_CAUSE(".is-damage-cause"),
    IS_LAST_DAMAGE_CAUSE(".is-last-damage-cause"),
    IS_LAST_DAMAGE_CAUSE_LIST(".is-last-damage-list"),
    HOLDS_ITEM_MAIN(".holds-item-main"),
    IS_MATERIAL(".is-material"),
    HOLDS_ITEM_OFFHAND(".holds-item-offhand"),
    PRIZES(".prizes"),
    ITEMS(".items"),
    CONDITIONS(".conditions"),
    ITEMS_GIVE_TYPE(".items-give-type"),
    NAME(".name"),
    FEED(".feed"),
    EFFECTS(".effects"),
    EFFECTS_LIST(".effects-list"),
    EFFECTS_GIVE_TYPE(".effects-give-type"),
    ENCHANTS(".enchants"),
    ENCHANTS_ITEMS(".enchants-items"),
    ENCHANTS_GIVE_TYPE(".enchants-give-type"),
    FIRE_ARROW(".fire-arrow"),
    TRIPLE_ARROW(".triple-arrow"),
    SPAWN_TNT(".spawn-tnt"),
    CONSOLE_COMMAND(".console-command"),
    PLAYER_COMMAND(".player-command"),
    SMITE_VICTIM(".smite-victim"),
    EXP_LEVEL(".exp-level"),
    SPAWN_MOB(".spawn-mob"),
    SPAWN_MOB_TYPES(".spawn-mob-types"),
    HEAL(".heal"),
    INCREASE_DAMAGE(".increase-damage"),
    REDUCE_DAMAGE(".reduce-damage"),
    LORE(".lore"),
    LAST_LORE_UPGRADE(".last-lore-line-upgrade-available"),
    LAST_LORE_MAXED(".last-lore-line-maxed"),
    COST(".cost"),
    HEAD_URL(".head-url"),
    COST_INCREASE(".cost-increase"),
    PERCENT(".percent-chance-increase"),
    MAX_LEVEL(".max-level"),
    POSITION(".position"),
    ENABLED(".enabled"),
    MESSAGE_ENABLED(".message-enabled"),
    MESSAGE(".message"),
    ;

    private final String path;

    Path(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public static String getFullPath(String perk, Path path){
        return Path.MAIN.getPath() + perk + path.getPath();
    }
    public static String getPrizePath(String perk, Path path){
        return Path.MAIN.getPath() + perk + Path.PRIZES.getPath() + path.getPath();
    }
    public static String getConditionPath(String perk, Path path){
        return Path.MAIN.getPath() + perk + Path.CONDITIONS.getPath() + path.getPath();
    }
}
