package com.divictusgaming.MinigamesPerks.enums;

import org.bukkit.Material;

import java.util.ArrayList;
import java.util.Arrays;

public enum DefaultConfig {
    INVENTORY_TITLE("perks.inventory-title", "&0&lPerk Upgrades"),
    INVENTORY_SIZE("perks.inventory-size", 6),
    INVENTORY_FILL("perks.inventory-fill-material", Material.AIR.name()),
    DATABASE_TYPE("perks.databasetype", "file"),
    MYSQL_URL("perks.mysql.url", "jdbc:mysql://localhost:3306/databasename?createDatabaseIfNotExist=true"),
    MYSQL_USERNAME("perks.mysql.username", "root"),
    MYSQL_PASSWORD("perks.mysql.password", "pass"),
    ECONOMY_COMMAND("perks.economy.command", "eco take {player} {cost_rounded}"),
    ECONOMY_PLACEHOLDER("perks.economy.placeholder", "%vault_eco_balance_fixed%"),
    NEED_PERMISSION("perks.permission-needed", false),
    SHOW_LEVEL_NUMBER("perks.show-level-number", true),
    ZERO_WHEN_NO_UPGRADED("perks.zero-when-not-upgraded", true),
    ENCHANT_WHEN_UPGRADED("perks.enchant-when-upgraded", true),
    LAST_DAMAGER_TIME("perks.kill.last-damager-time", 10),
    PERK_DISABLED_IN_WORLD("perks.disabled-world", new ArrayList<>(Arrays.asList("some-world", "other-world"))),
    //CONFIRM CONFIGURATIONS
    //CONFIRM INVENTORY
    CONFIRM_INVENTORY_ENABLED("perks.confirm.enabled", true),
    CONFIRM_INVENTORY_TITLE("perks.confirm.inventory-title", "&0&lConfirm Upgrade"),
    CONFIRM_INVENTORY_SIZE("perks.confirm.inventory-size", 6),
    CONFIRM_INVENTORY_FILL("perks.confirm.inventory-fill-material", Material.AIR.name()),
    //CONFIRM ITEM
    CONFIRM_INVENTORY_YES_ITEM("perks.confirm.items.confirm-item.item", Material.EMERALD_BLOCK.name(), false, true),
    CONFIRM_INVENTORY_YES_SLOTS("perks.confirm.items.confirm-item.slot", new ArrayList<>(Arrays.asList(10, 11, 19, 20)), false, true),
    CONFIRM_INVENTORY_YES_AMOUNT("perks.confirm.items.confirm-item.amount", 1, false, true),
    CONFIRM_INVENTORY_YES_NAME("perks.confirm.items.confirm-item.name", "&2&lConfirm Upgrade", false, true),
    CONFIRM_INVENTORY_YES_LORE("perks.confirm.items.confirm-item.lore", "&7&lClick to upgrade", false, true),
    CONFIRM_INVENTORY_YES_COMMANDS("perks.confirm.items.confirm-item.commands", new ArrayList<>(Arrays.asList("[confirm]")), false, true),
    //CANCEL ITEM
    CONFIRM_INVENTORY_NO_ITEM("perks.confirm.items.cancel-item.item", Material.REDSTONE_BLOCK.name(), false, true),
    CONFIRM_INVENTORY_NO_SLOTS("perks.confirm.items.cancel-item.slot", new ArrayList<>(Arrays.asList(15, 16, 24, 25)), false, true),
    CONFIRM_INVENTORY_NO_AMOUNT("perks.confirm.items.cancel-item.amount", 1, false, true),
    CONFIRM_INVENTORY_NO_NAME("perks.confirm.items.cancel-item.name", "&4&lCancel Upgrade", false, true),
    CONFIRM_INVENTORY_NO_LORE("perks.confirm.items.cancel-item.lore", "&7&lClick to close", false, true),
    CONFIRM_INVENTORY_NO_COMMANDS("perks.confirm.items.cancel-item.commands", new ArrayList<>(Arrays.asList("[cancel]")), false, true),
    //DISPLAY ITEM
    CONFIRM_INVENTORY_DISPLAY_ITEM("perks.confirm.items.perk-item.item", "{perkMaterial}", false, true),
    CONFIRM_INVENTORY_DISPLAY_SLOTS("perks.confirm.items.perk-item.slot", 40, false, true),
    CONFIRM_INVENTORY_DISPLAY_AMOUNT("perks.confirm.items.perk-item.amount", 1, false, true),
    CONFIRM_INVENTORY_DISPLAY_NAME("perks.confirm.items.perk-item.name", "{perkName} &7&l{level_roman}", false, true),
    CONFIRM_INVENTORY_DISPLAY_LORE("perks.confirm.items.perk-item.lore", "&7&l{level_roman} &c&l>> &7&l{nextLevel_roman}", false, true),
    //CUSTOM ITEMS
    //CLOSE ITEM
    CUSTOM_ITEMS_EXIT_ITEM("perks.custom-items.close.item", Material.BARRIER.name(), true),
    CUSTOM_ITEMS_EXIT_SLOT("perks.custom-items.close.slot", 48, true),
    CUSTOM_ITEMS_EXIT_AMOUNT("perks.custom-items.close.amount", 1, true),
    CUSTOM_ITEMS_EXIT_NAME("perks.custom-items.close.name", "&c&lClose", true),
    CUSTOM_ITEMS_EXIT_LORE("perks.custom-items.close.lore", "&7Click to close.", true),
    CUSTOM_ITEMS_EXIT_COMMANDS("perks.custom-items.close.commands", new ArrayList<>(Arrays.asList("[close]")), true),
    //BALANCE ITEM
    CUSTOM_ITEMS_BALANCE_ITEM("perks.custom-items.balance.item", Material.EMERALD.name(), true),
    CUSTOM_ITEMS_BALANCE_SLOT("perks.custom-items.balance.slot", 50, true),
    CUSTOM_ITEMS_BALANCE_AMOUNT("perks.custom-items.balance.amount", 1, true),
    CUSTOM_ITEMS_BALANCE_NAME("perks.custom-items.balance.name", "&a&lBalance", true),
    CUSTOM_ITEMS_BALANCE_LORE("perks.custom-items.balance.lore", "&7You have %vault_eco_balance_fixed% coins!.", true),
    CUSTOM_ITEMS_BALANCE_COMMANDS("perks.custom-items.balance.commands", new ArrayList<>(Arrays.asList("[message] {prefix}&7You have %vault_eco_balance_fixed% coins!", "[close]")), true),
    ;

    private String path;
    private Object value;
    private Boolean isCustomItem;
    private Boolean isConfirmItem;

    DefaultConfig(String path, Object value, Boolean isCustomItem, Boolean isConfirmItem) {
        this.path = path;
        this.value = value;
        this.isCustomItem = isCustomItem;
        this.isConfirmItem = isConfirmItem;
    }

    DefaultConfig(String path, Object value, Boolean isCustomItem) {
        this.path = path;
        this.value = value;
        this.isCustomItem = isCustomItem;
    }

    DefaultConfig(String path, Object value) {
        this.path = path;
        this.value = value;
        this.isCustomItem = false;
        this.isConfirmItem = false;
    }

    public String getPath() {
        return path;
    }

    public Object getValue() {
        return value;
    }

    public Boolean isCustomItem() {
        return isCustomItem;
    }

    public Boolean isConfirmItem() {
        return isConfirmItem;
    }
}
