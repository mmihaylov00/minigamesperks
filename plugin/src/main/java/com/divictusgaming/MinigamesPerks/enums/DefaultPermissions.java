package com.divictusgaming.MinigamesPerks.enums;


public enum DefaultPermissions {
    PERKS_ADMIN("perks.admin"),
    PERKS_USER("perks.user"),
            ;

    DefaultPermissions(String permission) {
        this.permission = permission;
    }

    private String permission;

    public String getPermission() {
        return permission;
    }
}
