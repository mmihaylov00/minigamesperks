package com.divictusgaming.MinigamesPerks.papi;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.enums.DefaultMessages;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class MinigamesPlaceholders extends me.clip.placeholderapi.expansion.PlaceholderExpansion {

    @Override
    public boolean persist(){
        return true;
    }

    @Override
    public boolean canRegister(){
        return true;
    }

    @Override
    public String getIdentifier() {
        return "minigamesperks";
    }

    @Override
    public String getAuthor() {
        return "ZeaL_BGN";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier){
        return getValue(player, identifier);
    }

    private String getValue(OfflinePlayer player, String identifier){
        String[] pl = identifier.split("_");
        if (pl.length < 2){
            return null;
        }
        Main plugin = Main.getPlugin(Main.class);
        for (String perk : plugin.getPerks().getPerksObject().keySet()) {
            if (perk.equalsIgnoreCase(pl[0])){
                switch (pl[1]){
                    case "level":
                        try {
                            Integer level = plugin.getDb().getLevels(player.getUniqueId()).get(pl[0]);
                            return level + "";
                        }catch (Exception e){
                            return null;
                        }
                    case "maxlevel":
                        try {
                            Integer level = plugin.getPerks().getPerksObject().get(pl[0]).getMaxLevel();
                            return level + "";
                        }catch (Exception e){
                            return null;
                        }
                    case "haspermission":
                        try {
                            if (player.isOnline() && player.getPlayer() != null){
                                if (player.getPlayer().hasPermission(plugin.getPerks().getPerksObject().get(pl[0]).getPermission())){
                                    return (String) plugin.getMessages().get(DefaultMessages.HAS_PERMISSION_PLACEHOLDER.getPath());
                                } else {
                                    return (String) plugin.getMessages().get(DefaultMessages.NO_PERMISSION_PLACEHOLDER.getPath());
                                }
                            }
                            ;
                        }catch (Exception e){
                            return null;
                        }
                    case "cost":
                        try {
                            Double cost = plugin.getPerks().getPerksObject().get(pl[0]).getCost();
                            Double increase = plugin.getPerks().getPerksObject().get(pl[0]).getCostIncrease();
                            Integer level = plugin.getDb().getLevels(player.getUniqueId()).get(pl[0]);
                            return ((level * increase) + cost) + "";
                        }catch (Exception e){
                            return null;
                        }
                }
            }
        }

        return null;
    }

    @Override
    public String onRequest(OfflinePlayer player, String identifier){
        return getValue(player, identifier);
    }
}
