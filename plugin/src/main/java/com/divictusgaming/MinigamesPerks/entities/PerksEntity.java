package com.divictusgaming.MinigamesPerks.entities;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.Perk;
import org.bukkit.configuration.ConfigurationSection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class PerksEntity {
    private final HashMap<String, Integer> perkLevels;
    private Boolean isUpdated;

    public PerksEntity() {
        this.perkLevels = new HashMap<>();
        for (Map.Entry<String, Perk> entry : Main.getPlugin(Main.class).getPerks().getPerksObject().entrySet())
            perkLevels.put(entry.getKey(), entry.getValue().getStartingLevel());

        this.isUpdated = false;
    }

    public PerksEntity(ResultSet set) {
        this.perkLevels = new HashMap<>();
        for (String value : Main.getPlugin(Main.class).getPerks().getPerksObject().keySet()) {
            try {
                perkLevels.put(value, set.getInt(value.replaceAll("-", "_") + "_LEVEL"));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        this.isUpdated = false;
    }

    public PerksEntity(ConfigurationSection section) {
        this.perkLevels = new HashMap<>();
        for (String value : Main.getPlugin(Main.class).getPerks().getPerksObject().keySet()) {
            int level = section.getInt(value);
            perkLevels.put(value, level);
        }
        this.isUpdated = false;
    }

    public HashMap<String, Integer> getPerkLevels() {
        return perkLevels;
    }

    public void update() {
        isUpdated = true;
    }

    public Boolean getUpdated() {
        return isUpdated;
    }
}
