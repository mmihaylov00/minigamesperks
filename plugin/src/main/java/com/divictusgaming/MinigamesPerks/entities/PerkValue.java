package com.divictusgaming.MinigamesPerks.entities;


import java.util.ArrayList;

public class PerkValue {
    private String path;
    private String stringValue;
    private Double doubleValue = 0D;
    private Integer integerValue;
    private Boolean booleanValue;
    private ArrayList<String> list;

    public PerkValue(String path, String stringValue) {
        this.path = path;
        this.stringValue = stringValue;
    }

    public PerkValue(String path, double doubleValue) {
        this.path = path;
        this.doubleValue = doubleValue;
    }

    public PerkValue(String path, int integerValue) {
        this.path = path;
        this.integerValue = integerValue;
    }

    public PerkValue(String path, boolean booleanValue) {
        this.path = path;
        this.booleanValue = booleanValue;
    }

    public PerkValue(String path, String... strings) {
        this.path = path;
        if (strings.length == 1) {
            this.stringValue = strings[0];
        } else {
            this.list = new ArrayList<>();
            for (String s : strings) {
                if (s == null) continue;
                list.add(s);
            }
        }
    }

    public Object getValue() {
        if (list != null){
            return list;
        }
        if (stringValue != null) {
            return stringValue;
        }
        if (doubleValue != 0) {
            return doubleValue;
        }
        if (integerValue != null) {
            return integerValue;
        }
        if (booleanValue != null) {
            return booleanValue;
        }
        return null;
    }

    public String getPath() {
        return path;
    }

    public String getStringValue() {
        return stringValue;
    }

    public double getDoubleValue() {
        if (doubleValue == null || doubleValue == 0) {
            doubleValue = new Double(integerValue);
        }
        return doubleValue;
    }

    public int getIntegerValue() {
        if (integerValue == null || integerValue == 0) {
            integerValue = doubleValue.intValue();
        }
        return integerValue;
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public ArrayList<String> getList() {
        return list;
    }
}
