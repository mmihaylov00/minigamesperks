package com.divictusgaming.MinigamesPerks.commands;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.Perk;
import com.divictusgaming.MinigamesPerks.enums.DefaultPermissions;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CommandTabCompleter implements TabCompleter {
    private final ArgumentNode argument;

    private static final CommandList[] commands = CommandList.values();

    private enum CommandList {
        RELOAD("reload", DefaultPermissions.PERKS_ADMIN),
        OPEN("open", DefaultPermissions.PERKS_USER),
        OPEN_PLAYER("open <player>", DefaultPermissions.PERKS_ADMIN),
        UPGRADE("upgrade <perk>", DefaultPermissions.PERKS_USER),
        LIST_PERKS("perks", DefaultPermissions.PERKS_USER),
        UPGRADE_PLAYER("upgrade <perk> <player>", DefaultPermissions.PERKS_ADMIN),
        EXECUTE("execute <perk> <player>", DefaultPermissions.PERKS_ADMIN),
        SET_LEVEL("set-level <perk> <player> <level>", DefaultPermissions.PERKS_ADMIN),
        ;

        private final String command;
        private final DefaultPermissions permission;

        CommandList(String command, DefaultPermissions permission) {
            this.command = command;
            this.permission = permission;
        }
    }

    public CommandTabCompleter() {
        argument = new ArgumentNode(null, null);
        setupArguments();
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String alias, String[] args) {
        if (!commandSender.hasPermission(DefaultPermissions.PERKS_USER.getPermission())) return new ArrayList<>();
        Main plugin = Main.getPlugin(Main.class);
        if (args.length == 1) {
            ArrayList<String> suggestions = new ArrayList<>(argument.children.size());
            for (ArgumentNode child : argument.children)
                suggestions.add(child.arg);

            filterSuggestions(suggestions, args);
            suggestions.sort(String::compareTo);

            return suggestions;
        }

        List<String> suggestions = getSuggestions(argument, args, 0, commandSender);
        if (suggestions.remove("<perk>")) {
            if (commandSender.hasPermission(DefaultPermissions.PERKS_ADMIN.getPermission())){
                suggestions.addAll(plugin.getPerks().getPerksObject().keySet());
            } else {
                Set<Map.Entry<String, Perk>> entries = plugin.getPerks().getPerksObject().entrySet();
                for (Map.Entry<String, Perk> entry : entries) {
                    if (entry.getValue().getPermission() == null ||
                            commandSender.hasPermission(entry.getValue().getPermission()))
                        suggestions.add(entry.getKey());
                }
            }
        } else if (suggestions.remove("<player>"))
            for (Player onlinePlayer : Bukkit.getOnlinePlayers())
                suggestions.add(onlinePlayer.getName());
        else if (suggestions.remove("<level>")) {
            Perk perk = plugin.getPerks().getPerksObject().get(args[1]);

            suggestions.add("0");
            if (perk != null) suggestions.add(perk.getMaxLevel() + "");

            return suggestions;
        }

        filterSuggestions(suggestions, args);
        suggestions.sort(String::compareTo);
        return suggestions;
    }

    private void filterSuggestions(List<String> suggestions, String[] args) {
        String lastArg;
        if (!(lastArg = args[args.length - 1]).isEmpty()) {
            for (int i = 0; i < suggestions.size(); i++)
                if (!suggestions.get(i).startsWith(lastArg))
                    suggestions.remove(i--);
        }

    }

    private List<String> getSuggestions(ArgumentNode current, String[] args, int i, CommandSender commandSender) {
        if (args.length >= i) {
            for (ArgumentNode argument : current.children) {
                if ((argument.permission == null || commandSender.hasPermission(argument.permission.getPermission())) &&
                        (argument.arg.equals(args[i]) || argument.arg.charAt(0) == '<')) {
                    if (++i == args.length - 1) {
                        ArrayList<String> suggestions = new ArrayList<>(argument.children.size());
                        for (ArgumentNode child : argument.children)
                            suggestions.add(child.arg);

                        return suggestions;
                    }
                    return getSuggestions(argument, args, i, commandSender);
                }
            }
        }
        return new ArrayList<>();
    }

    private void setupArguments() {
        for (CommandList value : commands) {
            String[] args = value.command.split(" ");
            ArgumentNode currentNode = argument;

            for (String arg : args) {
                boolean addToChildren = true;
                for (ArgumentNode argument : currentNode.children) {
                    if (argument.arg.equals(arg)) {
                        currentNode = argument;
                        addToChildren = false;
                        break;
                    }
                }
                if (addToChildren) {
                    ArgumentNode argument = new ArgumentNode(arg, value.permission);
                    currentNode.children.add(argument);
                    currentNode = argument;
                }
            }
        }
    }

    private static class ArgumentNode {
        private final String arg;
        private final DefaultPermissions permission;
        private final List<ArgumentNode> children;

        public ArgumentNode(String arg, DefaultPermissions permission) {
            this.arg = arg;
            this.permission = permission;
            this.children = new ArrayList<>();
        }
    }
}

