package com.divictusgaming.MinigamesPerks.commands;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.Perk;
import com.divictusgaming.MinigamesPerks.enums.DefaultMessages;
import com.divictusgaming.MinigamesPerks.enums.DefaultPermissions;
import com.divictusgaming.MinigamesPerks.events.perks.CommandEvent;
import com.divictusgaming.MinigamesPerks.inventories.MainMenu;
import com.divictusgaming.MinigamesPerks.utils.MessageSender;
import com.divictusgaming.MinigamesPerks.utils.PerkUpgrader;
import com.divictusgaming.MinigamesPerks.utils.StringFormatter;
import com.divictusgaming.MinigamesPerks.utils.VersionChecker;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class MainCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        Main plugin = Main.getPlugin(Main.class);
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (args.length == 0) {
                sendHelpMessage(p);
            } else {
                switch (args[0]) {
                    case "reload": {
                        if (!p.hasPermission(DefaultPermissions.PERKS_ADMIN.getPermission())) {
                            MessageSender.sendMessage(p, DefaultMessages.PERMISSION_DENIED);
                            break;
                        }
                        reload(p);
                        break;
                    }
                    case "help": {
                        sendHelpMessage(p);
                        break;
                    }
                    case "perks": {
                        listPerks(p);
                        break;
                    }
                    case "version": {
                        version(sender);
                        break;
                    }
                    case "open": {
                        if (args.length > 1) {
                            if (!p.hasPermission(DefaultPermissions.PERKS_ADMIN.getPermission())) {
                                MessageSender.sendMessage(p, DefaultMessages.PERMISSION_DENIED);
                                break;
                            }
                            Player player = Bukkit.getServer().getPlayer(args[1]);
                            if (player != null && !player.isOnline()) {
                                MessageSender.sendMessage(p, DefaultMessages.INVALID_PLAYER);
                                break;
                            }
                            openMenu(player);
                            break;
                        }
                        if (plugin.getConfigObject().getNeedPermission() &&
                                !p.hasPermission(DefaultPermissions.PERKS_USER.getPermission())) {
                            MessageSender.sendMessage(p, DefaultMessages.PERMISSION_DENIED);
                            break;
                        }
                        openMenu(p);
                        break;
                    }
                    case "upgrade": {
                        if (args.length > 1) {
                            if (!plugin.getPerks().getPerksObject().containsKey(args[1])) {
                                MessageSender.sendMessage(p, DefaultMessages.INVALID_PERK);
                                break;
                            }
                            if (args.length > 2) {
                                if (!p.hasPermission(DefaultPermissions.PERKS_ADMIN.getPermission())) {
                                    MessageSender.sendMessage(p, DefaultMessages.PERMISSION_DENIED);
                                    break;
                                }
                                Player player = Bukkit.getServer().getPlayer(args[2]);
                                if (player != null && !player.isOnline()) {
                                    MessageSender.sendMessage(p, DefaultMessages.INVALID_PLAYER);
                                    break;
                                }
                                upgrade(player, args[1]);
                                break;
                            }
                            if (plugin.getConfigObject().getNeedPermission() &&
                                    !p.hasPermission(DefaultPermissions.PERKS_USER.getPermission())) {
                                MessageSender.sendMessage(p, DefaultMessages.PERMISSION_DENIED);
                                break;
                            }
                            upgrade(p, args[1]);
                            break;
                        }
                        MessageSender.sendMessage(p, DefaultMessages.PERK_NOT_SPECIFIED);
                        break;
                    }
                    case "set-level": {
                        if (!p.hasPermission(DefaultPermissions.PERKS_ADMIN.getPermission())) {
                            MessageSender.sendMessage(p, DefaultMessages.PERMISSION_DENIED);
                            break;
                        }
                        if (args.length > 1) {
                            if (!plugin.getPerks().getPerksObject().containsKey(args[1])) {
                                MessageSender.sendMessage(p, DefaultMessages.INVALID_PERK);
                                break;
                            }
                            if (args.length > 2) {
                                Player player = Bukkit.getServer().getPlayer(args[2]);
                                if (player != null && !player.isOnline()) {
                                    MessageSender.sendMessage(p, DefaultMessages.INVALID_PLAYER);
                                    break;
                                }

                                if (args.length > 3) {
                                    try {
                                        setPerkLevel(p, player, args[1], Integer.parseInt(args[3]));
                                    } catch (NumberFormatException e) {
                                        MessageSender.sendMessage(sender, DefaultMessages.INVALID_PERK_LEVEL);
                                    }
                                    break;
                                }

                                MessageSender.sendMessage(sender, DefaultMessages.LEVEL_NOT_SPECIFIED);
                                break;
                            }
                            if (plugin.getConfigObject().getNeedPermission() &&
                                    !p.hasPermission(DefaultPermissions.PERKS_USER.getPermission())) {
                                MessageSender.sendMessage(p, DefaultMessages.PERMISSION_DENIED);
                                break;
                            }
                            upgrade(p, args[1]);
                            break;
                        }
                        MessageSender.sendMessage(p, DefaultMessages.PERK_NOT_SPECIFIED);
                        break;
                    }
                    case "execute": {
                        if (args.length > 1) {
                            if (args.length > 2) {
                                if (!p.hasPermission(DefaultPermissions.PERKS_ADMIN.getPermission())) {
                                    MessageSender.sendMessage(p, DefaultMessages.PERMISSION_DENIED);
                                    break;
                                }
                                Player player = Bukkit.getServer().getPlayer(args[2]);
                                if (player != null && !player.isOnline()) {
                                    MessageSender.sendMessage(sender, DefaultMessages.INVALID_PLAYER);
                                    break;
                                }
                                if (!plugin.getPerks().getPerksObject().containsKey(args[1])) {
                                    MessageSender.sendMessage(p, DefaultMessages.INVALID_PERK);
                                    break;
                                }
                                CommandEvent customEvent = (CommandEvent) plugin.getCommandEvents().get(args[1]);
                                if (customEvent == null) {
                                    MessageSender.sendMessage(p, DefaultMessages.INVALID_OR_NOT_COMMAND_PERK);
                                    break;
                                }
                                HashMap<String, String> ph = new HashMap<>();
                                ph.put("\\{perkName}", args[1]);
                                ph.put("\\{player}", player.getName());
                                customEvent.executeCommand(player);
                                MessageSender.sendMessage(p, DefaultMessages.PERK_EXECUTED, ph);
                                break;
                            }
                            MessageSender.sendMessage(sender, DefaultMessages.PLAYER_NOT_SPECIFIED);
                            break;
                        }
                        MessageSender.sendMessage(sender, DefaultMessages.PERK_NOT_SPECIFIED);
                        break;
                    }
                    default: {
                        openMenu(p);
                    }
                }
            }
            return true;
        }

        if (args.length == 0) {
            sendHelpMessage(sender);
        } else {
            switch (args[0]) {
                case "reload": {
                    reload(sender);
                    break;
                }
                case "perks": {
                    listPerks(sender);
                    break;
                }
                case "help": {
                    sendHelpMessage(sender);
                    break;
                }
                case "version": {
                    version(sender);
                    break;
                }
                case "zealcreatedme": {
                    sender.sendMessage("Wyd, %%__USER__%%?");
                    sender.sendMessage("Dw, %%__NONCE__%%?");
                    break;
                }
                case "open": {
                    if (args.length < 2) {
                        MessageSender.sendMessage(sender, DefaultMessages.PLAYER_NOT_SPECIFIED);
                    }
                    Player player = Bukkit.getServer().getPlayer(args[1]);
                    if (player != null && !player.isOnline()) {
                        MessageSender.sendMessage(sender, DefaultMessages.INVALID_PLAYER);
                        break;
                    }
                    openMenu(player);
                    break;
                }

                case "upgrade": {
                    if (args.length > 1) {
                        if (args.length > 2) {
                            Player p = Bukkit.getServer().getPlayer(args[2]);
                            if (p != null && !p.isOnline()) {
                                MessageSender.sendMessage(sender, DefaultMessages.INVALID_PLAYER);
                                break;
                            }
                            if (!plugin.getPerks().getPerksObject().containsKey(args[1])) {
                                MessageSender.sendMessage(p, DefaultMessages.INVALID_PERK);
                                break;
                            }
                            upgrade(p, args[1]);
                            break;
                        }
                        MessageSender.sendMessage(sender, DefaultMessages.PLAYER_NOT_SPECIFIED);
                        break;
                    }
                    MessageSender.sendMessage(sender, DefaultMessages.PERK_NOT_SPECIFIED);
                    break;
                }

                case "set-level": {
                    if (args.length > 1) {
                        if (args.length > 2) {
                            if (args.length > 3) {
                                Player p = Bukkit.getServer().getPlayer(args[2]);
                                if (p != null && !p.isOnline()) {
                                    MessageSender.sendMessage(sender, DefaultMessages.INVALID_PLAYER);
                                    break;
                                }
                                if (!plugin.getPerks().getPerksObject().containsKey(args[1])) {
                                    MessageSender.sendMessage(p, DefaultMessages.INVALID_PERK);
                                    break;
                                }

                                try {
                                    setPerkLevel(sender, p, args[1], Integer.parseInt(args[1]));
                                } catch (NumberFormatException e) {
                                    MessageSender.sendMessage(sender, DefaultMessages.INVALID_PERK_LEVEL);
                                }
                                break;
                            }
                            MessageSender.sendMessage(sender, DefaultMessages.LEVEL_NOT_SPECIFIED);
                            break;
                        }
                        MessageSender.sendMessage(sender, DefaultMessages.PLAYER_NOT_SPECIFIED);
                        break;
                    }
                    MessageSender.sendMessage(sender, DefaultMessages.PERK_NOT_SPECIFIED);
                    break;
                }
                case "execute": {
                    if (args.length > 1) {
                        if (args.length > 2) {
                            Player p = Bukkit.getServer().getPlayer(args[2]);
                            if (p == null || !p.isOnline()) {
                                MessageSender.sendMessage(sender, DefaultMessages.INVALID_PLAYER);
                                break;
                            }
                            if (!plugin.getPerks().getPerksObject().containsKey(args[1])) {
                                MessageSender.sendMessage(p, DefaultMessages.INVALID_PERK);
                                break;
                            }
                            CommandEvent customEvent = (CommandEvent) plugin.getCommandEvents().get(args[1]);
                            if (customEvent == null) {
                                MessageSender.sendMessage(p, DefaultMessages.INVALID_OR_NOT_COMMAND_PERK);
                                break;
                            }
                            HashMap<String, String> ph = new HashMap<>();
                            ph.put("\\{perkName}", args[1]);
                            ph.put("\\{player}", p.getName());
                            customEvent.executeCommand(p);
                            MessageSender.sendMessage(p, DefaultMessages.PERK_EXECUTED, ph);
                            break;
                        }
                        MessageSender.sendMessage(sender, DefaultMessages.PLAYER_NOT_SPECIFIED);
                        break;
                    }
                    MessageSender.sendMessage(sender, DefaultMessages.PERK_NOT_SPECIFIED);
                    break;
                }
                default: {
                    MessageSender.sendMessage(sender, DefaultMessages.INVALID_COMMAND);
                }


            }
        }
        return true;
    }

    private void listPerks(CommandSender p) {
        Main plugin = Main.getPlugin(Main.class);
        String output = plugin.getPerksList();
        if (output == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("&e&lPerk ID &7- &a&lPerk Name\n");
            for (Map.Entry<String, Perk> perk : plugin.getPerks().getPerksObject().entrySet()) {
                sb.append("&e").append(perk.getKey().toLowerCase()).append(" &7- ")
                        .append(perk.getValue().getName()).append("\n");
            }
            output = StringFormatter.format(sb.toString());
            plugin.setPerksList(output);
        }
        p.sendMessage(output);
    }

    private void openMenu(CommandSender sender) {
        MainMenu.openMainMenu((Player) sender);
    }

    private void upgrade(Player sender, String perk) {
        PerkUpgrader.upgradePerk(sender, perk, false);
    }

    private void setPerkLevel(CommandSender executor, Player sender, String perk, int level) {
        PerkUpgrader.setPerkLevel(executor, sender, perk, level);
    }

    private void reload(CommandSender p) {
        MessageSender.sendBoth(p, DefaultMessages.RELOAD_STARTED);
        if (Main.getPlugin(Main.class).reload()) {
            MessageSender.sendBoth(p, DefaultMessages.RELOAD_FAILED);
            return;
        }
        MessageSender.sendBoth(p, DefaultMessages.RELOAD_COMPLETED);
    }


    private void version(CommandSender p) {
        if (p.hasPermission(DefaultPermissions.PERKS_ADMIN.getPermission()) || p.isOp() || p instanceof ConsoleCommandSender) {
            MessageSender.sendMessage(p, ChatColor.YELLOW + "Plugin Minigames Perks " + VersionChecker.version + ", by ZeaL_BGN", null, false);
        } else {
            sendHelpMessage(p);
        }
    }

    private void sendHelpMessage(CommandSender p) {
        MessageSender.sendMessage(p, DefaultMessages.HELP_MENU_HEADER, false);
        if (p.hasPermission(DefaultPermissions.PERKS_ADMIN.getPermission()) || p.isOp() || p instanceof ConsoleCommandSender) {
            MessageSender.sendMessage(p, DefaultMessages.ADMIN_HELP_LINE_1, false);
            MessageSender.sendMessage(p, DefaultMessages.ADMIN_HELP_LINE_2, false);
            MessageSender.sendMessage(p, DefaultMessages.ADMIN_HELP_LINE_3, false);
            MessageSender.sendMessage(p, DefaultMessages.ADMIN_HELP_LINE_4, false);
            MessageSender.sendMessage(p, DefaultMessages.ADMIN_HELP_LINE_5, false);
            MessageSender.sendMessage(p, DefaultMessages.ADMIN_HELP_LINE_6, false);
        } else {
            MessageSender.sendMessage(p, DefaultMessages.HELP_LINE_1, false);
            MessageSender.sendMessage(p, DefaultMessages.HELP_LINE_2, false);
            MessageSender.sendMessage(p, DefaultMessages.HELP_LINE_3, false);
        }
    }
}
