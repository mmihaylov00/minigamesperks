package com.divictusgaming.MinigamesPerks.factories;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.Config;
import com.divictusgaming.MinigamesPerks.dto.CustomItem;
import com.divictusgaming.MinigamesPerks.enums.DefaultConfig;
import com.divictusgaming.MinigamesPerks.exceptions.InvalidValueException;
import com.divictusgaming.MinigamesPerks.utils.ObjectExtractor;
import com.divictusgaming.MinigamesPerks.utils.HeadWorker;
import com.divictusgaming.MinigamesPerks.utils.StringFormatter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class ConfigFactory {
    private FileConfiguration fileConfiguration = null;
    private File configFile = null;
    private final File path = Main.getPlugin(Main.class).getPluginsFolder();
    private Map<String, Object> values;
    private Config config = null;

    public ConfigFactory() {
        reloadConfig();
    }

    public void reloadConfig() {
        if (configFile == null) {
            configFile = new File(path, "config.yml");
        }
        fileConfiguration = YamlConfiguration.loadConfiguration(configFile);

        boolean createItems = fileConfiguration.isSet("perks.custom-items");
        boolean createConfirmItems = fileConfiguration.isSet("perks.confirm");

        for (DefaultConfig value : DefaultConfig.values()) {
            if (createItems && value.isCustomItem()) {
                continue;
            }
            if (createConfirmItems && value.isConfirmItem()) {
                continue;
            }
            if (!fileConfiguration.isSet(value.getPath())) {
                fileConfiguration.set(value.getPath(), value.getValue());
            }
        }

        values = fileConfiguration.getValues(true);
        for (Map.Entry<String, Object> set : values.entrySet()) {
            if (set.getValue() instanceof String) {
                set.setValue(StringFormatter.format((String) set.getValue()));
            }
        }
        try {
            fileConfiguration.save(path + File.separator + "config.yml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        String invTitle;
        try {
            invTitle = ObjectExtractor.extractObject(values.get(DefaultConfig.INVENTORY_TITLE.getPath()), String.class);
        } catch (InvalidValueException e) {
            invTitle = "Invalid title configuration";
            Bukkit.getLogger().warning(ChatColor.RED + "Invalid inventory-title configuration! Defaulting!");
        }
        String ecoCommand;
        try {
            ecoCommand = ObjectExtractor.extractObject(values.get(DefaultConfig.ECONOMY_COMMAND.getPath()), String.class);
        } catch (InvalidValueException e) {
            Bukkit.getLogger().warning(ChatColor.RED + "Invalid economy-command configuration! Fix it or the upgrading won't work!");
            ecoCommand = "";
        }
        boolean needPermission = false;
        try {
            needPermission = ObjectExtractor.extractObject(values.get(DefaultConfig.NEED_PERMISSION.getPath()), Boolean.class);
        } catch (InvalidValueException ignored) {
        }
        boolean zeroWhenNotUpgraded = true;
        try {
            zeroWhenNotUpgraded = ObjectExtractor.extractObject(values.get(DefaultConfig.ZERO_WHEN_NO_UPGRADED.getPath()), Boolean.class);
        } catch (InvalidValueException ignored) {
        }

        boolean showLevelNumber = true;
        try {
            showLevelNumber = ObjectExtractor.extractObject(values.get(DefaultConfig.SHOW_LEVEL_NUMBER.getPath()), Boolean.class);
        } catch (InvalidValueException ignored) {
        }
        boolean enchantWhenUpgraded = true;
        try {
            enchantWhenUpgraded = ObjectExtractor.extractObject(values.get(DefaultConfig.ENCHANT_WHEN_UPGRADED.getPath()), Boolean.class);
        } catch (InvalidValueException ignored) {
        }

        List<String> disabledWorlds;
        try {
            disabledWorlds = ObjectExtractor.extractObject(values.get(DefaultConfig.PERK_DISABLED_IN_WORLD.getPath()), ArrayList.class);
        } catch (InvalidValueException e) {
            disabledWorlds = new ArrayList<>();
        }
        String ecoPlaceholder;
        try {
            ecoPlaceholder = ObjectExtractor.extractObject(values.get(DefaultConfig.ECONOMY_PLACEHOLDER.getPath()), String.class);
        } catch (InvalidValueException e) {
            Bukkit.getLogger().warning(ChatColor.RED + "Invalid economy-placeholder configuration! Fix it or the upgrading won't work!");
            ecoPlaceholder = "";
        }
        int invSize = 6;
        try {
            try {
                invSize = ObjectExtractor.extractObject(values.get(DefaultConfig.INVENTORY_SIZE.getPath()), Integer.class);
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "The inventory-size in the config.yml file is invalid! Defaulting to 6!");
            }
            if (invSize < 1) {
                Bukkit.getLogger().warning(ChatColor.RED + "The inventory-size in the config.yml file is invalid! Defaulting to 6!");
                invSize = 6;
            }
        } catch (NumberFormatException e) {
            Bukkit.getLogger().warning(ChatColor.RED + "The inventory-size in the config.yml file is invalid! Defaulting to 6!");
        }
        ItemStack fillItem;

        try {
            String material = ObjectExtractor.extractObject(values.get(DefaultConfig.INVENTORY_FILL.getPath()), String.class);
            fillItem = ObjectExtractor.stringToItem(material);
        } catch (InvalidValueException e) {
            Bukkit.getLogger().warning(ChatColor.RED + "The inventory-fill-material in the config.yml file is invalid! Defaulting to AIR!");
            fillItem = new ItemStack(Material.AIR);
        }

        Integer lastDamagerTimeSec = 10;
        try {
            lastDamagerTimeSec = ObjectExtractor.extractObject(values.get(DefaultConfig.LAST_DAMAGER_TIME.getPath()), Integer.class);
        } catch (InvalidValueException e) {
            Bukkit.getLogger().warning(ChatColor.RED + "Invalid last-damager-time configuration! Defaulting to 10!");
        }


        boolean confirmIsEnabled = false;
        try {
            confirmIsEnabled = ObjectExtractor.extractObject(values.get(DefaultConfig.CONFIRM_INVENTORY_ENABLED.getPath()), Boolean.class);
        } catch (InvalidValueException ignored) {
        }

        String confirmInventoryTitle = "Invalid confirm inv title!";
        Integer confirmInventorySize = 6;
        ItemStack confirmFillItem = new ItemStack(Material.AIR);
        Map<String, CustomItem> confirmItems = new HashMap<>();

        if (confirmIsEnabled) {
            try {
                try {
                    confirmInventoryTitle = ObjectExtractor.extractObject(values.get(DefaultConfig.CONFIRM_INVENTORY_TITLE.getPath()), String.class);
                } catch (InvalidValueException e) {
                    Bukkit.getLogger().warning(ChatColor.RED + "Invalid confirm inventory title! Please fix.");
                }
                try {
                    confirmInventorySize = ObjectExtractor.extractObject(values.get(DefaultConfig.CONFIRM_INVENTORY_SIZE.getPath()), Integer.class);
                } catch (InvalidValueException e) {
                    Bukkit.getLogger().warning(ChatColor.RED + "Invalid confirm inventory size! Defaulting to 6.");
                }
                try {
                    String material = ObjectExtractor.extractObject(values.get(DefaultConfig.CONFIRM_INVENTORY_FILL.getPath()), String.class);
                    confirmFillItem = ObjectExtractor.stringToItem(material);
                } catch (InvalidValueException e) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The confirm.inventory-fill-material is invalid! Defaulting to AIR!");
                }

                Set<String> confirmDataItems = fileConfiguration.getConfigurationSection("perks.confirm.items").getValues(false).keySet();
                confirmItems = this.generateItems(confirmDataItems, "perks.confirm.items.");
            } catch (NullPointerException ignored) {
            }
        }

        Map<String, CustomItem> finalItems = new HashMap<>();
        try {
            Set<String> customItems = fileConfiguration.getConfigurationSection("perks.custom-items").getValues(false).keySet();
            finalItems = generateItems(customItems, "perks.custom-items.");
        } catch (NullPointerException ignored) {
        }

        fillItem = Main.getPlugin(Main.class).getINMSMain().createPerkItem(".", fillItem);

        config = new Config(invTitle, invSize, fillItem, ecoCommand, ecoPlaceholder, lastDamagerTimeSec, finalItems, confirmIsEnabled,
                confirmInventoryTitle, confirmInventorySize, confirmFillItem, confirmItems, disabledWorlds, needPermission,
                zeroWhenNotUpgraded, showLevelNumber, enchantWhenUpgraded);

    }

    public Map<String, Object> getFileConfiguration() {
        if (fileConfiguration == null) {
            reloadConfig();
            return this.values;
        }
        return this.values;
    }

    public Config getConfig() {
        return config;
    }

    private Map<String, CustomItem> generateItems(Set<String> customItems, String startPath) throws NullPointerException {
        Map<String, CustomItem> finalItems = new HashMap<>();
        for (String item : customItems) {
            ItemStack i = new ItemStack(Material.STONE);
            CustomItem ci = new CustomItem();
            try {
                String m = ObjectExtractor.extractObject(values.get(startPath + item + ".item"), String.class);
                Material mat;
                if (m.equals("{perkMaterial}")) {
                    ci.setPerkItem(true);
                } else {
                    String[] itemSplit = m.split(":");
                    if (itemSplit.length < 1)
                        throw new InvalidValueException();

                    if (itemSplit[0].equals("SKULL")) itemSplit[0] = "SKULL_ITEM";
                    mat = Material.getMaterial(itemSplit[0]);
                    if (mat == null || mat == Material.AIR)
                        throw new InvalidValueException();

                    if (itemSplit.length == 2) {
                        try {
                            i = new ItemStack(mat, 1, Byte.parseByte(itemSplit[1]));
                        } catch (NumberFormatException e) {
                            throw new InvalidValueException();
                        }
                    } else {
                        i = new ItemStack(mat);
                    }
                }
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "Invalid " + startPath + item + ".item! Ignoring item!");
                continue;
            }

            i = Main.getPlugin(Main.class).getINMSMain().createPerkItem(":" + item, i);
            try {
                ci.setPositions(ObjectExtractor.extractObject(values.get(startPath + item + ".slot"), ArrayList.class));
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "Invalid " + startPath + item + ".slot! Ignoring item!");
                continue;
            }

            Object o = values.get(startPath + item + ".amount");
            if (o != null) {
                try {
                    Integer amount = ObjectExtractor.extractObject(o, Integer.class);
                    i.setAmount(amount);
                } catch (InvalidValueException e) {
                    Bukkit.getLogger().warning(ChatColor.RED + "Invalid " + startPath + item + ".amount! Defaulting to 1!");
                    i.setAmount(1);
                }
            } else {
                i.setAmount(1);
            }

            ItemMeta meta = i.getItemMeta();
            o = values.get(startPath + item + ".name");
            if (o != null) {
                try {
                    String name = ObjectExtractor.extractObject(o, String.class);
                    meta.setDisplayName(name);
                } catch (InvalidValueException e) {
                    Bukkit.getLogger().warning(ChatColor.RED + "Invalid " + startPath + item + ".name! Defaulting to " + meta.getDisplayName());
                }
            }

            o = values.get(startPath + item + ".lore");
            if (o != null) {
                try {
                    String lore = ObjectExtractor.extractObject(o, String.class);
                    meta.setLore(Arrays.asList(lore.split("\\|\\|")));
                } catch (Exception e) {
                    Bukkit.getLogger().warning(ChatColor.RED + "Invalid " + startPath + item + ".lore!");
                }
            }

            o = values.get(startPath + item + ".head-url");
            if (o != null) {
                try {
                    String skullUrl = ObjectExtractor.extractObject(o, String.class);
                    if (!skullUrl.isEmpty() && meta instanceof SkullMeta) {
                        HeadWorker.setHeadSkin((SkullMeta) meta, skullUrl);
                    }
                } catch (Exception ignored) {
                }
            }

            i.setItemMeta(meta);
            ci.setItem(i);
            o = values.get(startPath + item + ".commands");
            if (o != null) {
                try {
                    ArrayList<String> commands = ObjectExtractor.extractObject(o, ArrayList.class);
                    for (String command : commands) {
                        switch (command) {
                            case "[close]":
                                ci.setClose(true);
                                break;
                            case "[cancel]":
                                ci.setCancel(true);
                                break;
                            case "[confirm]":
                                ci.setConfirm(true);
                                break;
                            default:
                                ci.addCommands(command);
                                break;
                        }
                    }
                } catch (InvalidValueException e) {
                    Bukkit.getLogger().warning(ChatColor.RED + "Invalid " + startPath + item + ".commands!");
                }
            }

            finalItems.put(item, ci);
        }
        return finalItems;
    }
}
