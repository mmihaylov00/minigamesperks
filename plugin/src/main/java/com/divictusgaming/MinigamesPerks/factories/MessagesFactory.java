package com.divictusgaming.MinigamesPerks.factories;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.enums.DefaultMessages;
import com.divictusgaming.MinigamesPerks.exceptions.InvalidValueException;
import com.divictusgaming.MinigamesPerks.utils.ObjectExtractor;
import com.divictusgaming.MinigamesPerks.utils.StringFormatter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class MessagesFactory {
    private File messagesFile = null;
    private final File path = Main.getPlugin(Main.class).getPluginsFolder();
    private Map<String, Object> messages;

    public MessagesFactory() {
        this.reloadMessages();
    }

    public void reloadMessages() {
        if (messagesFile == null) {
            messagesFile = new File(path, "messages.yml");
        }
        FileConfiguration messagesConfiguration = YamlConfiguration.loadConfiguration(messagesFile);
        for (DefaultMessages value : DefaultMessages.values()) {
            if (!messagesConfiguration.isSet(value.getPath())) {
                messagesConfiguration.set(value.getPath(), value.getValue());
            }
        }
        try {
            messagesConfiguration.save(path + File.separator + "messages.yml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        messages = messagesConfiguration.getValues(true);
        for (Map.Entry<String, Object> set : messages.entrySet()) {
            if (set.getValue() instanceof String) {
                set.setValue(StringFormatter.format((String) set.getValue()));
            } else {
                try {
                    set.setValue(StringFormatter.format(ObjectExtractor.extractObject(set.getValue(), String.class)));
                } catch (InvalidValueException e) {
                }
            }
        }
    }

    public Map<String, Object> getMessages() {
        if (messages == null || messages.isEmpty()) {
            reloadMessages();
        }
        return messages;
    }

}
