package com.divictusgaming.MinigamesPerks.factories;

import com.divictusgaming.MinigamesPerks.Main;
import com.divictusgaming.MinigamesPerks.dto.Perk;
import com.divictusgaming.MinigamesPerks.entities.PerkValue;
import com.divictusgaming.MinigamesPerks.enums.DefaultMessages;
import com.divictusgaming.MinigamesPerks.enums.DefaultPerks;
import com.divictusgaming.MinigamesPerks.enums.Path;
import com.divictusgaming.MinigamesPerks.exceptions.InvalidValueException;
import com.divictusgaming.MinigamesPerks.storages.PerksStorage;
import com.divictusgaming.MinigamesPerks.utils.ObjectExtractor;
import com.divictusgaming.MinigamesPerks.utils.HeadWorker;
import com.divictusgaming.MinigamesPerks.utils.StringFormatter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class PerksFactory {
    private FileConfiguration config = null;
    private File configFile = null;
    private final File PATH = Main.getPlugin(Main.class).getPluginsFolder();
    private PerksStorage storage;

    public PerksFactory() {
        reloadConfig();
    }

    public void reloadConfig() {
        if (configFile == null) {
            configFile = new File(PATH, "perks.yml");
            if (!configFile.exists()) {
                try {
                    configFile.createNewFile();
                    config = YamlConfiguration.loadConfiguration(configFile);
                    DefaultPerks[] perks = DefaultPerks.values();
                    for (DefaultPerks value : perks) {
                        for (PerkValue customValue : value.getCustomValues()) {
                            genConfigValue(customValue.getValue(), value.getPath() + customValue.getPath());
                        }
                    }
                    config.save(PATH + File.separator + "perks.yml");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else config = YamlConfiguration.loadConfiguration(configFile);
        } else config = YamlConfiguration.loadConfiguration(configFile);

        Map<String, Object> perks = config.getValues(true);
        Set<String> allPerks;
        try {
            allPerks = config.getConfigurationSection("perks").getValues(false).keySet();
        } catch (NullPointerException e) {
            configFile.renameTo(new File(PATH + File.separator + "perks" + new Random().nextInt(90000) + 10000 + ".yml"));
            Bukkit.getLogger().warning(ChatColor.RED + "The perks config is broken. It has been renamed to " + configFile.getName() + " and new one has been generated!");
            configFile = null;
            reloadConfig();
            return;
        }

        HashMap<String, Perk> finalPerks = new HashMap<>();
        for (String perk : allPerks) {
            boolean enabled = false;
            try {
                enabled = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.ENABLED)), Boolean.class);
            } catch (InvalidValueException e) {
            }
            if (!enabled) {
                continue;
            }
            String name;
            try {
                name = StringFormatter.format(ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.NAME)), String.class));
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "Invalid perk name: " + perk + "! Defaulting!");
                name = "Invalid perk " + perk + " name";
            }

            String lore;
            try {
                lore = StringFormatter.format(ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.LORE)), String.class));
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "Invalid perk lore: " + perk + "! Defaulting!");
                lore = "Invalid perk " + perk + " lore";
            }

            List<String> commands;
            try {
                commands = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.UPGRADE_COMMANDS)), ArrayList.class);
            } catch (InvalidValueException e) {
                commands = new ArrayList<>();
            }

            String lastLoreUpgrade;
            try {
                lastLoreUpgrade = StringFormatter.format(ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.LAST_LORE_UPGRADE)), String.class));
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "Invalid perk last-lore-upgrade: " + perk + "! Defaulting!");
                lastLoreUpgrade = "Invalid perk " + perk + " last-lore-upgrade";
            }

            String lastLoreMaxed;
            try {
                lastLoreMaxed = StringFormatter.format(ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.LAST_LORE_MAXED)), String.class));
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "Invalid perk last-lore-maxed: " + perk + "! Defaulting!");
                lastLoreMaxed = "Invalid perk " + perk + " last-lore-maxed";
            }

            String message;
            try {
                message = StringFormatter.format(ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.MESSAGE)), String.class));
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "Invalid perk message: " + perk + "! Defaulting!");
                message = "Invalid perk " + perk + " message";
            }

            Integer startingLevel;
            try {
                startingLevel = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.STARTING_LEVEL)), Integer.class);
            } catch (InvalidValueException e) {
                startingLevel = 0;
            }

            Boolean messageEnabled;
            try {
                messageEnabled = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.MESSAGE_ENABLED)), Boolean.class);
            } catch (InvalidValueException e) {
                messageEnabled = false;
            }

            String itemName;
            try {
                itemName = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.DISPLAY_ITEM)), String.class);
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "Invalid perk display-item: " + perk + "! Disabling!");
                continue;
            }

            String permission = null;
            try {
                permission = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.PERMISSION)), String.class);
            } catch (InvalidValueException ignored) {
            }

            String permissionMessage = null;
            try {
                permissionMessage = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.PERMISSION_MESSAGE)), String.class);
            } catch (InvalidValueException e) {

            }
            if (permissionMessage == null || permissionMessage.isEmpty()) {
                permissionMessage = (String) Main.getPlugin(Main.class).getMessages()
                        .get(DefaultMessages.PERMISSION_DENIED_UPGRADE.getPath());
            }

            String[] itemSplit = itemName.split(":");
            if (itemSplit.length < 1) {
                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid display item! Disabling!");
                continue;
            }
            ItemStack item;
            if (itemSplit[0].equals("SKULL")) itemSplit[0] = "SKULL_ITEM";
            Material m = Material.getMaterial(itemSplit[0]);
            if (m == null || m == Material.AIR) {
                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid display item! Disabling!");
                continue;
            }
            if (itemSplit.length == 2) {
                try {
                    item = new ItemStack(m, 1, Byte.parseByte(itemSplit[1]));
                } catch (NumberFormatException e) {
                    Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has invalid display item! Disabling!");
                    continue;
                }
            } else {
                item = new ItemStack(m);
            }
            Double cost;
            try {
                cost = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.COST)), Double.class);
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has incorrect cost value! Disabling!");
                continue;
            }
            String skullUrl;
            try {
                skullUrl = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.HEAD_URL)), String.class);
                ItemMeta meta = item.getItemMeta();
                if (!skullUrl.isEmpty() && meta instanceof SkullMeta) {
                    HeadWorker.setHeadSkin((SkullMeta) meta, skullUrl);
                    item.setItemMeta(meta);
                }
            } catch (Exception ignored) {
            }
            Double costIncrease;
            try {
                costIncrease = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.COST_INCREASE)), Double.class);
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has incorrect cost-increase value! Disabling!");
                continue;
            }
            Double percentIncrease;
            try {
                percentIncrease = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.PERCENT)), Double.class);
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has incorrect percent-increase value! Disabling!");
                continue;
            }
            Integer maxLevel;
            try {
                maxLevel = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.MAX_LEVEL)), Integer.class);
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has incorrect max-level value! Disabling!");
                continue;
            }
            Integer pos;
            try {
                pos = ObjectExtractor.extractObject(perks.get(Path.getFullPath(perk, Path.POSITION)), Integer.class);
            } catch (InvalidValueException e) {
                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has incorrect position value! Disabling!");
                continue;
            }
            if (cost < 0 || costIncrease < 0 || percentIncrease < 0 || maxLevel < 0 || pos < 0) {
                Bukkit.getLogger().warning(ChatColor.RED + "The " + perk + " perk has a negative value somewhere! Disabling!");
                continue;
            }
            finalPerks.put(perk, new Perk(perk, name, permission, permissionMessage, lore, lastLoreUpgrade, lastLoreMaxed, cost, costIncrease,
                    percentIncrease, maxLevel, pos, messageEnabled, message, item, startingLevel, commands));

        }
        this.storage = new PerksStorage(finalPerks, perks);
    }

    private void genConfigValue(Object value, String path) {
        if (!config.isSet(path)) {
            config.set(path, value);
        }
    }

    public PerksStorage getPerks() {
        if (config == null) {
            reloadConfig();
            return this.getPerks();
        }
        return storage;
    }

}
