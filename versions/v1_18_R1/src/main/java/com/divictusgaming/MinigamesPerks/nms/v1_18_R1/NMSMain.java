package com.divictusgaming.MinigamesPerks.nms.v1_18_R1;

import com.divictusgaming.MinigamesPerks.nms.INMSMain;
import net.minecraft.nbt.NBTTagCompound;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_18_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NMSMain implements INMSMain {
    @Override
    public ItemStack getItemInHand(PlayerInventory inv, int hand) {
        return hand == 0 ? inv.getItemInMainHand() : inv.getItemInOffHand();
    }

    private static final HashMap<String, String> effects;

    static {
        effects = new HashMap<>();
        effects.put(PotionEffectType.BLINDNESS.getName(), "Blindness");
        effects.put(PotionEffectType.JUMP.getName(), "Jump");
        effects.put(PotionEffectType.INCREASE_DAMAGE.getName(), "Strength");
        effects.put(PotionEffectType.SPEED.getName(), "Speed");
        effects.put(PotionEffectType.ABSORPTION.getName(), "Absorption");
        effects.put(PotionEffectType.CONFUSION.getName(), "Confusion");
        effects.put(PotionEffectType.DAMAGE_RESISTANCE.getName(), "Resistance");
        effects.put(PotionEffectType.FAST_DIGGING.getName(), "Haste");
        effects.put(PotionEffectType.FIRE_RESISTANCE.getName(), "Fire Resistance");
        effects.put(PotionEffectType.NIGHT_VISION.getName(), "Night Vision");
        effects.put(PotionEffectType.HARM.getName(), "Harm");
        effects.put(PotionEffectType.HEAL.getName(), "Heal");
        effects.put(PotionEffectType.HEALTH_BOOST.getName(), "Health Boost");
        effects.put(PotionEffectType.HUNGER.getName(), "Hunger");
        effects.put(PotionEffectType.INVISIBILITY.getName(), "Invisibility");
        effects.put(PotionEffectType.POISON.getName(), "Poison");
        effects.put(PotionEffectType.REGENERATION.getName(), "Regeneration");
        effects.put(PotionEffectType.SATURATION.getName(), "Saturation");
        effects.put(PotionEffectType.SLOW.getName(), "Slowness");
        effects.put(PotionEffectType.SLOW_DIGGING.getName(), "Mining Fatigue");
        effects.put(PotionEffectType.WATER_BREATHING.getName(), "Respiration");
        effects.put(PotionEffectType.WEAKNESS.getName(), "Weakness");
        effects.put(PotionEffectType.WITHER.getName(), "Wither");
        effects.put(PotionEffectType.GLOWING.getName(), "Glowing");
        effects.put(PotionEffectType.LEVITATION.getName(), "Levitation");
        effects.put(PotionEffectType.LUCK.getName(), "Luck");
        effects.put(PotionEffectType.UNLUCK.getName(), "Bad Luck");
        effects.put(PotionEffectType.CONDUIT_POWER.getName(), "Conduit");
        effects.put(PotionEffectType.DOLPHINS_GRACE.getName(), "Dolphin's Grace");
        effects.put(PotionEffectType.SLOW_FALLING.getName(), "Slow Falling");
        effects.put(PotionEffectType.BAD_OMEN.getName(), "Bad Omen");
        effects.put(PotionEffectType.HERO_OF_THE_VILLAGE.getName(), "Hero Of The Village");
    }

    @Override
    public String getEffectName(String effect) {
        return effects.get(effect);
    }

    public String getPerkName(String tagName, ItemStack item) {
        net.minecraft.world.item.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
        if (nmsItem == null ||
                // hasTag() = r()
                !nmsItem.r()) {
            return null;
        }
        // getTag() = s()
        // getString() = l()
        return nmsItem.s().l(tagName);
    }

    public Boolean getItemBoolean(String tagName, ItemStack item) {
        net.minecraft.world.item.ItemStack itemStack = CraftItemStack.asNMSCopy(item);
        return itemStack != null &&
                // hasTag() = r()
                itemStack.r() &&
                // getTag() = s()
                // getBoolean() = q()
                itemStack.s().q(tagName) == Boolean.TRUE;
    }

    @Override
    public ItemStack createPerkItem(String perkName, ItemStack item) {
        if (item != null) {
            net.minecraft.world.item.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
            NBTTagCompound compound = (nmsItem.r()) ? nmsItem.s() : new NBTTagCompound();
            //setBoolean() = a()
            compound.a("is.from.menu", true);
            //setString() = a()
            compound.a("perk.type", perkName);
            //setTag() = c()
            nmsItem.c(compound);
            return CraftItemStack.asBukkitCopy(nmsItem);
        }
        return new ItemStack(Material.AIR);
    }

    @Override
    public List<Material> getBuildingBlocks() {
        ArrayList<Material> materials = new ArrayList<>();
        materials.add(Material.STONE);
        materials.add(Material.GRASS);
        materials.add(Material.DIRT);
        materials.add(Material.COBBLESTONE);
        materials.add(Material.ANDESITE);
        materials.add(Material.POLISHED_ANDESITE);
        materials.add(Material.DIORITE);
        materials.add(Material.POLISHED_DIORITE);
        materials.add(Material.SAND);
        materials.add(Material.GRAVEL);
        materials.add(Material.GOLD_ORE);
        materials.add(Material.IRON_ORE);
        materials.add(Material.ACACIA_LOG);
        materials.add(Material.BIRCH_LOG);
        materials.add(Material.DARK_OAK_LOG);
        materials.add(Material.JUNGLE_LOG);
        materials.add(Material.OAK_LOG);
        materials.add(Material.SPRUCE_LOG);
        materials.add(Material.SPONGE);
        materials.add(Material.LAPIS_BLOCK);
        materials.add(Material.SANDSTONE);
        materials.add(Material.NOTE_BLOCK);
        materials.add(Material.BLACK_WOOL);
        materials.add(Material.BLUE_WOOL);
        materials.add(Material.CYAN_WOOL);
        materials.add(Material.BROWN_WOOL);
        materials.add(Material.GRAY_WOOL);
        materials.add(Material.GREEN_WOOL);
        materials.add(Material.LIGHT_BLUE_WOOL);
        materials.add(Material.LIME_WOOL);
        materials.add(Material.MAGENTA_WOOL);
        materials.add(Material.ORANGE_WOOL);
        materials.add(Material.PINK_WOOL);
        materials.add(Material.PURPLE_WOOL);
        materials.add(Material.RED_WOOL);
        materials.add(Material.LIGHT_GRAY_WOOL);
        materials.add(Material.WHITE_WOOL);
        materials.add(Material.YELLOW_WOOL);
        materials.add(Material.GOLD_BLOCK);
        materials.add(Material.IRON_BLOCK);
        materials.add(Material.ACACIA_WOOD);
        materials.add(Material.BIRCH_WOOD);
        materials.add(Material.DARK_OAK_WOOD);
        materials.add(Material.JUNGLE_WOOD);
        materials.add(Material.OAK_WOOD);
        materials.add(Material.SPRUCE_WOOD);
        materials.add(Material.BRICK);
        materials.add(Material.BOOKSHELF);
        materials.add(Material.MOSSY_COBBLESTONE);
        materials.add(Material.OBSIDIAN);
        materials.add(Material.DIAMOND_BLOCK);
        materials.add(Material.DIAMOND_ORE);
        materials.add(Material.SNOW_BLOCK);
        materials.add(Material.ICE);
        materials.add(Material.CLAY);
        materials.add(Material.JUKEBOX);
        materials.add(Material.PUMPKIN);
        materials.add(Material.GLOWSTONE);
        materials.add(Material.SMOOTH_STONE);
        materials.add(Material.MELON);
        materials.add(Material.MYCELIUM);
        materials.add(Material.NETHER_BRICK);
        materials.add(Material.REDSTONE_LAMP);
        materials.add(Material.EMERALD_ORE);
        materials.add(Material.EMERALD_BLOCK);
        materials.add(Material.COBBLESTONE_WALL);
        materials.add(Material.REDSTONE_BLOCK);
        materials.add(Material.QUARTZ_BLOCK);
        materials.add(Material.SLIME_BLOCK);
        materials.add(Material.HAY_BLOCK);
        materials.add(Material.COAL_BLOCK);
        materials.add(Material.PACKED_ICE);
        materials.add(Material.RED_SANDSTONE);
        materials.add(Material.END_STONE_BRICKS);
        materials.add(Material.FROSTED_ICE);
        materials.add(Material.DIRT_PATH);
        materials.add(Material.PURPUR_BLOCK);
        materials.add(Material.PURPUR_PILLAR);
        materials.add(Material.MAGMA_BLOCK);
        materials.add(Material.NETHER_WART_BLOCK);
        materials.add(Material.RED_NETHER_BRICKS);
        materials.add(Material.BONE_BLOCK);
        materials.add(Material.TERRACOTTA);
        materials.add(Material.BLACK_CONCRETE);
        materials.add(Material.BLUE_CONCRETE);
        materials.add(Material.CYAN_CONCRETE);
        materials.add(Material.BROWN_CONCRETE);
        materials.add(Material.GRAY_CONCRETE);
        materials.add(Material.GREEN_CONCRETE);
        materials.add(Material.LIGHT_BLUE_CONCRETE);
        materials.add(Material.LIME_CONCRETE);
        materials.add(Material.MAGENTA_CONCRETE);
        materials.add(Material.ORANGE_CONCRETE);
        materials.add(Material.PINK_CONCRETE);
        materials.add(Material.PURPLE_CONCRETE);
        materials.add(Material.RED_CONCRETE);
        materials.add(Material.LIGHT_GRAY_CONCRETE);
        materials.add(Material.WHITE_CONCRETE);
        materials.add(Material.YELLOW_CONCRETE);
        materials.add(Material.BLACK_TERRACOTTA);
        materials.add(Material.BLUE_TERRACOTTA);
        materials.add(Material.CYAN_TERRACOTTA);
        materials.add(Material.BROWN_TERRACOTTA);
        materials.add(Material.GRAY_TERRACOTTA);
        materials.add(Material.GREEN_TERRACOTTA);
        materials.add(Material.LIGHT_BLUE_TERRACOTTA);
        materials.add(Material.LIME_TERRACOTTA);
        materials.add(Material.MAGENTA_TERRACOTTA);
        materials.add(Material.ORANGE_TERRACOTTA);
        materials.add(Material.PINK_TERRACOTTA);
        materials.add(Material.PURPLE_TERRACOTTA);
        materials.add(Material.RED_TERRACOTTA);
        materials.add(Material.LIGHT_GRAY_TERRACOTTA);
        materials.add(Material.WHITE_TERRACOTTA);
        materials.add(Material.YELLOW_TERRACOTTA);
        materials.add(Material.BLUE_ICE);
        materials.add(Material.BRAIN_CORAL_BLOCK);
        materials.add(Material.DEAD_BRAIN_CORAL_BLOCK);
        materials.add(Material.BUBBLE_CORAL_BLOCK);
        materials.add(Material.DEAD_BUBBLE_CORAL_BLOCK);
        materials.add(Material.DEAD_FIRE_CORAL_BLOCK);
        materials.add(Material.FIRE_CORAL_BLOCK);
        materials.add(Material.HORN_CORAL_BLOCK);
        materials.add(Material.DEAD_HORN_CORAL_BLOCK);
        materials.add(Material.TUBE_CORAL_BLOCK);
        materials.add(Material.DEAD_TUBE_CORAL_BLOCK);
        materials.add(Material.DRIED_KELP_BLOCK);
        materials.add(Material.STRIPPED_ACACIA_LOG);
        materials.add(Material.STRIPPED_BIRCH_LOG);
        materials.add(Material.STRIPPED_DARK_OAK_LOG);
        materials.add(Material.STRIPPED_OAK_LOG);
        materials.add(Material.STRIPPED_JUNGLE_LOG);
        materials.add(Material.STRIPPED_SPRUCE_LOG);
        materials.add(Material.STRIPPED_ACACIA_WOOD);
        materials.add(Material.STRIPPED_BIRCH_WOOD);
        materials.add(Material.STRIPPED_DARK_OAK_WOOD);
        materials.add(Material.STRIPPED_OAK_WOOD);
        materials.add(Material.STRIPPED_JUNGLE_WOOD);
        materials.add(Material.SCAFFOLDING);
        materials.add(Material.PURPUR_SLAB);
        materials.add(Material.PRISMARINE_SLAB);
        materials.add(Material.SANDSTONE_SLAB);
        materials.add(Material.SMOOTH_QUARTZ_SLAB);
        materials.add(Material.SMOOTH_RED_SANDSTONE_SLAB);
        materials.add(Material.SMOOTH_SANDSTONE_SLAB);
        materials.add(Material.SMOOTH_STONE_SLAB);
        materials.add(Material.SPRUCE_SLAB);
        materials.add(Material.STONE_BRICK_SLAB);
        materials.add(Material.STONE_SLAB);
        materials.add(Material.ACACIA_SLAB);
        materials.add(Material.ANDESITE_SLAB);
        materials.add(Material.BIRCH_SLAB);
        materials.add(Material.BRICK_SLAB);
        materials.add(Material.COBBLESTONE_SLAB);
        materials.add(Material.CUT_SANDSTONE_SLAB);
        materials.add(Material.DARK_OAK_SLAB);
        materials.add(Material.DARK_PRISMARINE_SLAB);
        materials.add(Material.DIORITE_SLAB);
        materials.add(Material.END_STONE_BRICK_SLAB);
        materials.add(Material.GRANITE_SLAB);
        materials.add(Material.JUNGLE_SLAB);
        materials.add(Material.MOSSY_COBBLESTONE_SLAB);
        materials.add(Material.MOSSY_STONE_BRICK_SLAB);
        materials.add(Material.NETHER_BRICK_SLAB);
        materials.add(Material.OAK_SLAB);
        materials.add(Material.PETRIFIED_OAK_SLAB);
        materials.add(Material.POLISHED_ANDESITE_SLAB);
        materials.add(Material.POLISHED_DIORITE_SLAB);
        materials.add(Material.POLISHED_GRANITE_SLAB);
        materials.add(Material.PRISMARINE_BRICK_SLAB);
        materials.add(Material.QUARTZ_SLAB);
        materials.add(Material.RED_NETHER_BRICK_SLAB);
        materials.add(Material.RED_SANDSTONE_SLAB);
        materials.add(Material.PURPUR_STAIRS);
        materials.add(Material.PRISMARINE_STAIRS);
        materials.add(Material.SANDSTONE_STAIRS);
        materials.add(Material.SMOOTH_QUARTZ_STAIRS);
        materials.add(Material.SMOOTH_RED_SANDSTONE_STAIRS);
        materials.add(Material.SMOOTH_SANDSTONE_STAIRS);
        materials.add(Material.SPRUCE_STAIRS);
        materials.add(Material.STONE_BRICK_STAIRS);
        materials.add(Material.STONE_STAIRS);
        materials.add(Material.ACACIA_STAIRS);
        materials.add(Material.ANDESITE_STAIRS);
        materials.add(Material.BIRCH_STAIRS);
        materials.add(Material.BRICK_STAIRS);
        materials.add(Material.COBBLESTONE_STAIRS);
        materials.add(Material.DARK_OAK_STAIRS);
        materials.add(Material.DARK_PRISMARINE_STAIRS);
        materials.add(Material.DIORITE_STAIRS);
        materials.add(Material.END_STONE_BRICK_STAIRS);
        materials.add(Material.GRANITE_STAIRS);
        materials.add(Material.JUNGLE_STAIRS);
        materials.add(Material.MOSSY_COBBLESTONE_STAIRS);
        materials.add(Material.MOSSY_STONE_BRICK_STAIRS);
        materials.add(Material.NETHER_BRICK_STAIRS);
        materials.add(Material.OAK_STAIRS);
        materials.add(Material.POLISHED_ANDESITE_STAIRS);
        materials.add(Material.POLISHED_DIORITE_STAIRS);
        materials.add(Material.POLISHED_GRANITE_STAIRS);
        materials.add(Material.PRISMARINE_BRICK_STAIRS);
        materials.add(Material.QUARTZ_STAIRS);
        materials.add(Material.RED_NETHER_BRICK_STAIRS);
        materials.add(Material.RED_SANDSTONE_STAIRS);
        materials.add(Material.HONEY_BLOCK);
        materials.add(Material.HONEYCOMB_BLOCK);
        materials.add(Material.AMETHYST_BLOCK);
        materials.add(Material.COPPER_BLOCK);
        materials.add(Material.COPPER_ORE);
        materials.add(Material.RAW_COPPER_BLOCK);
        materials.add(Material.RAW_GOLD_BLOCK);
        materials.add(Material.RAW_IRON_BLOCK);
        materials.add(Material.BUDDING_AMETHYST);
        materials.add(Material.CALCITE);
        materials.add(Material.COBBLED_DEEPSLATE);
        materials.add(Material.INFESTED_DEEPSLATE);
        materials.add(Material.POLISHED_DEEPSLATE);
        materials.add(Material.CHISELED_DEEPSLATE);
        materials.add(Material.DEEPSLATE_BRICKS);
        materials.add(Material.DEEPSLATE_TILES);
        materials.add(Material.DEEPSLATE_REDSTONE_ORE);
        materials.add(Material.DEEPSLATE_LAPIS_ORE);
        materials.add(Material.DEEPSLATE_IRON_ORE);
        materials.add(Material.DEEPSLATE_GOLD_ORE);
        materials.add(Material.DEEPSLATE_EMERALD_ORE);
        materials.add(Material.DEEPSLATE_DIAMOND_ORE);
        materials.add(Material.DEEPSLATE_COPPER_ORE);
        materials.add(Material.DEEPSLATE_COAL_ORE);
        materials.add(Material.WAXED_COPPER_BLOCK);
        materials.add(Material.WAXED_CUT_COPPER);
        materials.add(Material.WAXED_EXPOSED_COPPER);
        materials.add(Material.MOSS_BLOCK);
        materials.add(Material.ROOTED_DIRT);
        materials.add(Material.BASALT);
        materials.add(Material.POLISHED_BASALT);
        materials.add(Material.SMOOTH_BASALT);
        materials.add(Material.TINTED_GLASS);
        materials.add(Material.TUFF);
        return materials;
    }

    @Override
    public String getVersionMaterialName(Integer id) {
        switch (id) {
            case 1:
                return Material.FIRE_CHARGE.name();
            case 2:
                return Material.ENDER_EYE.name();
            case 3:
                return Material.RED_DYE.name();
            case 4:
                return Material.OAK_WOOD.name();
            case 5:
                return Material.ENDERMITE_SPAWN_EGG.name();
            case 6:
                return Material.EXPERIENCE_BOTTLE.name();
            case 7:
                return Material.ORANGE_STAINED_GLASS_PANE.name();
            case 8:
                return Material.GOLDEN_CHESTPLATE.name();
        }
        return Material.AIR.name();
    }

    @Override
    public Material getVersionMaterial(Integer id) {
        switch (id) {
            case 1:
                return Material.GOLDEN_SWORD;
            case 2:
                return Material.WOODEN_SWORD;
            case 3:
                return Material.GOLDEN_AXE;
            case 4:
                return Material.WOODEN_AXE;
        }
        return Material.AIR;
    }

    @Override
    public ItemStack blockToItemStack(Block block) {
        return new ItemStack(block.getType());
    }

    @Override
    public String getEnchantName(Enchantment ench) {
        return ench.getKey().getKey();
    }

    @Override
    public Enchantment stringToEnchant(String name) {
        return Enchantment.getByKey(NamespacedKey.minecraft(name));
    }

    private static final HashMap<Enchantment, String> enchants;

    static {
        enchants = new HashMap<>();
        enchants.put(Enchantment.ARROW_DAMAGE, "Power");
        enchants.put(Enchantment.ARROW_FIRE, "Flame");
        enchants.put(Enchantment.ARROW_INFINITE, "Infinity");
        enchants.put(Enchantment.ARROW_KNOCKBACK, "Punch");
        enchants.put(Enchantment.BINDING_CURSE, "Curse of Binding");
        enchants.put(Enchantment.CHANNELING, "Channeling");
        enchants.put(Enchantment.DAMAGE_ALL, "Sharpness");
        enchants.put(Enchantment.DAMAGE_ARTHROPODS, "Bane of Arthropods");
        enchants.put(Enchantment.DAMAGE_UNDEAD, "Smite");
        enchants.put(Enchantment.DEPTH_STRIDER, "Depth Strider");
        enchants.put(Enchantment.DIG_SPEED, "Efficiency");
        enchants.put(Enchantment.DURABILITY, "Unbreaking");
        enchants.put(Enchantment.FIRE_ASPECT, "Fire Aspect");
        enchants.put(Enchantment.FROST_WALKER, "Frost Walker");
        enchants.put(Enchantment.IMPALING, "Impaling");
        enchants.put(Enchantment.KNOCKBACK, "Knockback");
        enchants.put(Enchantment.LOOT_BONUS_BLOCKS, "Fortune");
        enchants.put(Enchantment.LOOT_BONUS_MOBS, "Looting");
        enchants.put(Enchantment.LOYALTY, "Loyalty");
        enchants.put(Enchantment.LUCK, "Luck");
        enchants.put(Enchantment.LURE, "Lure");
        enchants.put(Enchantment.MENDING, "Mending");
        enchants.put(Enchantment.MULTISHOT, "Multishot");
        enchants.put(Enchantment.OXYGEN, "Oxygen");
        enchants.put(Enchantment.PIERCING, "Piercing");
        enchants.put(Enchantment.PROTECTION_ENVIRONMENTAL, "Protection");
        enchants.put(Enchantment.PROTECTION_EXPLOSIONS, "Blast Protection");
        enchants.put(Enchantment.PROTECTION_FALL, "Feather Falling");
        enchants.put(Enchantment.PROTECTION_FIRE, "Fire Protection");
        enchants.put(Enchantment.PROTECTION_PROJECTILE, "Projectile Protection");
        enchants.put(Enchantment.QUICK_CHARGE, "Quick Charge");
        enchants.put(Enchantment.RIPTIDE, "Riptide");
        enchants.put(Enchantment.SILK_TOUCH, "Silk Touch");
        enchants.put(Enchantment.SWEEPING_EDGE, "Sweeping Edge");
        enchants.put(Enchantment.THORNS, "Thorns");
        enchants.put(Enchantment.VANISHING_CURSE, "Vanishing Curse");
        enchants.put(Enchantment.WATER_WORKER, "Aqua Affinity");
    }

    @Override
    public String getEnchantRealName(Enchantment ench) {
        return enchants.get(ench);
    }

}
