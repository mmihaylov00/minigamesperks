package com.divictusgaming.MinigamesPerks.nms.v1_10_R1;

import com.divictusgaming.MinigamesPerks.nms.INMSMain;
import net.minecraft.server.v1_10_R1.NBTTagCompound;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NMSMain implements INMSMain {
    @Override
    public ItemStack getItemInHand(PlayerInventory inv, int hand) {
        return hand == 0 ? inv.getItemInMainHand() : inv.getItemInOffHand();
    }

    private static final HashMap<Enchantment, String> enchants;

    static {
        enchants = new HashMap<>();
        enchants.put(Enchantment.ARROW_DAMAGE, "Power");
        enchants.put(Enchantment.ARROW_FIRE, "Flame");
        enchants.put(Enchantment.ARROW_INFINITE, "Infinity");
        enchants.put(Enchantment.ARROW_KNOCKBACK, "Punch");
        enchants.put(Enchantment.DAMAGE_ALL, "Sharpness");
        enchants.put(Enchantment.DAMAGE_ARTHROPODS, "Bane of Arthropods");
        enchants.put(Enchantment.DAMAGE_UNDEAD, "Smite");
        enchants.put(Enchantment.DEPTH_STRIDER, "Depth Strider");
        enchants.put(Enchantment.DIG_SPEED, "Efficiency");
        enchants.put(Enchantment.DURABILITY, "Unbreaking");
        enchants.put(Enchantment.FIRE_ASPECT, "Fire Aspect");
        enchants.put(Enchantment.FROST_WALKER, "Frost Walker");
        enchants.put(Enchantment.KNOCKBACK, "Knockback");
        enchants.put(Enchantment.LOOT_BONUS_BLOCKS, "Fortune");
        enchants.put(Enchantment.LOOT_BONUS_MOBS, "Looting");
        enchants.put(Enchantment.LUCK, "Luck");
        enchants.put(Enchantment.LURE, "Lure");
        enchants.put(Enchantment.MENDING, "Mending");
        enchants.put(Enchantment.OXYGEN, "Oxygen");
        enchants.put(Enchantment.PROTECTION_ENVIRONMENTAL, "Protection");
        enchants.put(Enchantment.PROTECTION_EXPLOSIONS, "Blast Protection");
        enchants.put(Enchantment.PROTECTION_FALL, "Feather Falling");
        enchants.put(Enchantment.PROTECTION_FIRE, "Fire Protection");
        enchants.put(Enchantment.PROTECTION_PROJECTILE, "Projectile Protection");
        enchants.put(Enchantment.SILK_TOUCH, "Silk Touch");
        enchants.put(Enchantment.THORNS, "Thorns");
        enchants.put(Enchantment.WATER_WORKER, "Aqua Affinity");
    }

    @Override
    public String getEnchantRealName(Enchantment ench) {
        return enchants.get(ench);
    }

    private static final HashMap<String, String> effects;
    static {
        effects = new HashMap<>();
        effects.put(PotionEffectType.BLINDNESS.getName(), "Blindness");
        effects.put(PotionEffectType.JUMP.getName(), "Jump");
        effects.put(PotionEffectType.INCREASE_DAMAGE.getName(), "Strength");
        effects.put(PotionEffectType.SPEED.getName(), "Speed");
        effects.put(PotionEffectType.ABSORPTION.getName(), "Absorption");
        effects.put(PotionEffectType.CONFUSION.getName(), "Confusion");
        effects.put(PotionEffectType.DAMAGE_RESISTANCE.getName(), "Resistance");
        effects.put(PotionEffectType.FAST_DIGGING.getName(), "Haste");
        effects.put(PotionEffectType.FIRE_RESISTANCE.getName(), "Fire Resistance");
        effects.put(PotionEffectType.NIGHT_VISION.getName(), "Night Vision");
        effects.put(PotionEffectType.HARM.getName(), "Harm");
        effects.put(PotionEffectType.HEAL.getName(), "Heal");
        effects.put(PotionEffectType.HEALTH_BOOST.getName(), "Health Boost");
        effects.put(PotionEffectType.HUNGER.getName(), "Hunger");
        effects.put(PotionEffectType.INVISIBILITY.getName(), "Invisibility");
        effects.put(PotionEffectType.POISON.getName(), "Poison");
        effects.put(PotionEffectType.REGENERATION.getName(), "Regeneration");
        effects.put(PotionEffectType.SATURATION.getName(), "Saturation");
        effects.put(PotionEffectType.SLOW.getName(), "Slowness");
        effects.put(PotionEffectType.SLOW_DIGGING.getName(), "Mining Fatigue");
        effects.put(PotionEffectType.WATER_BREATHING.getName(), "Respiration");
        effects.put(PotionEffectType.WEAKNESS.getName(), "Weakness");
        effects.put(PotionEffectType.WITHER.getName(), "Wither");
        effects.put(PotionEffectType.GLOWING.getName(), "Glowing");
        effects.put(PotionEffectType.LEVITATION.getName(), "Levitation");
        effects.put(PotionEffectType.LUCK.getName(), "Luck");
        effects.put(PotionEffectType.UNLUCK.getName(), "Bad Luck");
    }

    @Override
    public String getEffectName(String effect) {
        return effects.get(effect);
    }

    public String getPerkName(String tagName, ItemStack item) {
        net.minecraft.server.v1_10_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
        if (nmsItem == null ||
                !nmsItem.hasTag()) {
            return null;
        }
        return nmsItem.getTag().getString(tagName);
    }

    public Boolean getItemBoolean(String tagName, ItemStack item) {
        net.minecraft.server.v1_10_R1.ItemStack itemStack = CraftItemStack.asNMSCopy(item);
        if (itemStack != null &&
                itemStack.hasTag() &&
                itemStack.getTag().getBoolean(tagName) == Boolean.TRUE) {
            return true;
        }
        return false;
    }

    public ItemStack createPerkItem(String perkName, ItemStack item) {
        if (item != null) {
            net.minecraft.server.v1_10_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
            NBTTagCompound compound = (nmsItem.hasTag()) ? nmsItem.getTag() : new NBTTagCompound();
            compound.setBoolean("is.from.menu", true);
            compound.setString("perk.type", perkName);
            nmsItem.setTag(compound);
            return CraftItemStack.asBukkitCopy(nmsItem);
        }
        return new ItemStack(Material.AIR);
    }

    @Override
    public List<Material> getBuildingBlocks() {
        ArrayList<Material> materials = new ArrayList<>();
        materials.add(Material.STONE);
        materials.add(Material.GRASS);
        materials.add(Material.DIRT);
        materials.add(Material.COBBLESTONE);
        materials.add(Material.WOOD);
        materials.add(Material.SAND);
        materials.add(Material.GRAVEL);
        materials.add(Material.GOLD_ORE);
        materials.add(Material.IRON_ORE);
        materials.add(Material.LOG);
        materials.add(Material.SPONGE);
        materials.add(Material.LAPIS_BLOCK);
        materials.add(Material.SANDSTONE);
        materials.add(Material.NOTE_BLOCK);
        materials.add(Material.WOOL);
        materials.add(Material.GOLD_BLOCK);
        materials.add(Material.IRON_BLOCK);
        materials.add(Material.DOUBLE_STEP);
        materials.add(Material.WOOD);
        materials.add(Material.BRICK);
        materials.add(Material.BOOKSHELF);
        materials.add(Material.MOSSY_COBBLESTONE);
        materials.add(Material.OBSIDIAN);
        materials.add(Material.WOOD_STAIRS);
        materials.add(Material.DIAMOND_BLOCK);
        materials.add(Material.DIAMOND_ORE);
        materials.add(Material.COBBLESTONE_STAIRS);
        materials.add(Material.SNOW_BLOCK);
        materials.add(Material.ICE);
        materials.add(Material.CLAY);
        materials.add(Material.JUKEBOX);
        materials.add(Material.PUMPKIN);
        materials.add(Material.GLOWSTONE);
        materials.add(Material.SMOOTH_BRICK);
        materials.add(Material.MELON_BLOCK);
        materials.add(Material.BRICK_STAIRS);
        materials.add(Material.SMOOTH_STAIRS);
        materials.add(Material.MYCEL);
        materials.add(Material.NETHER_BRICK);
        materials.add(Material.NETHER_BRICK_STAIRS);
        materials.add(Material.ENDER_STONE);
        materials.add(Material.REDSTONE_LAMP_OFF);
        materials.add(Material.REDSTONE_LAMP_ON);
        materials.add(Material.WOOD_DOUBLE_STEP);
        materials.add(Material.WOOD_STEP);
        materials.add(Material.SANDSTONE_STAIRS);
        materials.add(Material.EMERALD_ORE);
        materials.add(Material.EMERALD_BLOCK);
        materials.add(Material.SPRUCE_WOOD_STAIRS);
        materials.add(Material.BIRCH_WOOD_STAIRS);
        materials.add(Material.JUNGLE_WOOD_STAIRS);
        materials.add(Material.COBBLE_WALL);
        materials.add(Material.REDSTONE_BLOCK);
        materials.add(Material.QUARTZ_BLOCK);
        materials.add(Material.QUARTZ_STAIRS);
        materials.add(Material.STAINED_CLAY);
        materials.add(Material.LOG_2);
        materials.add(Material.ACACIA_STAIRS);
        materials.add(Material.DARK_OAK_STAIRS);
        materials.add(Material.SLIME_BLOCK);
        materials.add(Material.HAY_BLOCK);
        materials.add(Material.HARD_CLAY);
        materials.add(Material.COAL_BLOCK);
        materials.add(Material.PACKED_ICE);
        materials.add(Material.RED_SANDSTONE);
        materials.add(Material.RED_SANDSTONE_STAIRS);
        materials.add(Material.DOUBLE_STONE_SLAB2);
        materials.add(Material.STONE_SLAB2);
        materials.add(Material.END_BRICKS);
        materials.add(Material.FROSTED_ICE);
        materials.add(Material.GRASS_PATH);
        materials.add(Material.PURPUR_BLOCK);
        materials.add(Material.PURPUR_PILLAR);
        materials.add(Material.PURPUR_STAIRS);
        materials.add(Material.PURPUR_SLAB);
        materials.add(Material.PURPUR_DOUBLE_SLAB);
        materials.add(Material.MAGMA);
        materials.add(Material.NETHER_WART_BLOCK);
        materials.add(Material.RED_NETHER_BRICK);
        materials.add(Material.BONE_BLOCK);
        return materials;
    }

    @Override
    public String getVersionMaterialName(Integer id) {
        switch (id) {
            case 1:
                return Material.FIREBALL.name();
            case 2:
                return Material.EYE_OF_ENDER.name();
            case 3:
                return Material.INK_SACK.name() + ":1";
            case 4:
                return Material.WOOD.name();
            case 5:
                return Material.MONSTER_EGG.name() + ":67";
            case 6:
                return Material.EXP_BOTTLE.name();
            case 7:
                return Material.STAINED_GLASS_PANE.name() + ":1";
            case 8:
                return Material.GOLD_CHESTPLATE.name();
        }
        return Material.AIR.name();
    }
    @Override
    public Material getVersionMaterial(Integer id) {
        switch (id) {
            case 1:
                return Material.GOLD_SWORD;
            case 2:
                return Material.WOOD_SWORD;
            case 3:
                return Material.GOLD_AXE;
            case 4:
                return Material.WOOD_AXE;
        }
        return Material.AIR;
    }

    @Override
    public ItemStack blockToItemStack(Block block) {
        return new ItemStack(block.getType(), block.getData());
    }

    @Override
    public String getEnchantName(Enchantment ench) {
        return ench.getName();
    }

    @Override
    public Enchantment stringToEnchant(String name) {
        return Enchantment.getByName(name);
    }
}
