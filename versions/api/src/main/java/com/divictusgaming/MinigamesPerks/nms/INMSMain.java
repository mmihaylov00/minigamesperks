package com.divictusgaming.MinigamesPerks.nms;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

public interface INMSMain {
    public String getPerkName(String tagName, ItemStack item);
    public Boolean getItemBoolean(String tagName, ItemStack item);
    public ItemStack createPerkItem(String perkName, ItemStack item);
    public List<Material> getBuildingBlocks();
    public String getVersionMaterialName(Integer id);
    public Material getVersionMaterial(Integer id);
    public ItemStack blockToItemStack(Block block);
    public String getEnchantName(Enchantment ench);
    public Enchantment stringToEnchant(String name);
    public String getEnchantRealName(Enchantment ench);
    public ItemStack getItemInHand(PlayerInventory inv, int hand);

    String getEffectName(String effect);
}
